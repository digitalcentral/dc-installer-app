//Global variables
var remoteUrl = "http://realestate.digitalcentral.com.au/dcinstaller_beta/index.php/";
var chatUrl = "https://chat.digitalcentral.com.au/api/v1/";
var chatImageUrl = "https://chat.digitalcentral.com.au";
var uploads = "http://realestate.digitalcentral.com.au/";
var inappUrl = "http://realestate.digitalcentral.com.au/dc/index.php/home/view/";
var inappUrlProductOrder = "https://realestate.digitalcentral.com.au/dc/index.php/shopping_agency/order_details_shoping_product/";
var imgURI;

var ChatList = [];

var ChannalList = [];

var myChatTimer = "";
var myHomeTimer = "";
var chatAutoLoginTimer = "";

var loadMoreCount = 0;

var loadMoreClicked = 0;

var homeSyncDuration = 30000;

var audioElement = "";
var audio = "";
var gOrdertype = "";

var checkConnectionCalls = 0;

var monthNames = new Array("January", "February", "March",
                           "April", "May", "June", "July", "August", "September",
                           "October", "November", "December");


var version = 'V-2.0.62'; //Build date = 18/12/2017...
var versionCheck = '2.0.62';
var monthNames = [
                  "January", "February", "March",
                  "April", "May", "June", "July",
                  "August", "September", "October",
                  "November", "December"
                  ];

//===================================== login =======================================//

$(document).on('click', '#login-btn', function (event) {
               event.preventDefault();
               
               
               if ($('#loginForm').validate({
                                            rules: {
                                            uid_r: {
                                            required: true
                                            },
                                            pwd_r: {
                                            required: true
                                            },
                                            }
                                            }).form()) {
               appLogin();
               //  $.mobile.pageContainer.pagecontainer({ defaults: true });
               
               
               } else {
               return false;
               }
               
               });

//This function handled login

function appLogin() {
    
    $.ajax({
           type: 'post',
           url: remoteUrl + 'login',
           data: $('#loginForm').serialize(),
           cache: false,
           beforeSend: function () {
           $.mobile.loading('show');
           },
           complete: function () {
           $.mobile.loading('hide');
           },
           success: function (result) {
           
           result = JSON.parse(result);
           
           if (result.username && result.login_userid) {
           window.localStorage.setItem("userid", result.login_userid);
           window.localStorage.setItem("username", result.username);
           if (result.useonlyapp != null) {
           window.localStorage.setItem("useonlyapp", result.useonlyapp);
           }
           
           if (result.removalprompt != null) {
           window.localStorage.setItem("removalprompt", result.removalprompt);
           }
           
           if (result.rocketchatusername && result.rocketchatpassword) {
           window.localStorage.setItem("rocketchatusername", result.rocketchatusername);
           window.localStorage.setItem("rocketchatpassword", result.rocketchatpassword);
           
           chatLogin();
           
           } else {
           
           $(':mobile-pagecontainer').pagecontainer('change', "index.html");
           homepageAjax(1);
           }
           
           } else {
           alert("Unable To Authenticate. Check Your Credintials.");
           return false;
           }
           
           },
           error: function (jqXHR, textStatus, errorThrown) {
           //alert('Network error has occurred while fetching login details,please try again!');//1-
           }
           });
}


// Roket chat login and get accesstoken and userid //

function chatLogin() {
    
    $.ajax({
           type: 'post',
           url: chatUrl + 'login',
           data: 'username=' + window.localStorage.getItem("rocketchatusername") + '&password=' + window.localStorage.getItem("rocketchatpassword"),
           cache: false,
           beforeSend: function () {
           $.mobile.loading('show');
           },
           complete: function () {
           $.mobile.loading('hide');
           },
           success: function (result) {
           
           // result = JSON.parse(result);
           if (result.data) {
           
           window.localStorage.setItem("chat_authToken", result.data.authToken);
           window.localStorage.setItem("chat_userId", result.data.userId);
           
           homepageAjax();
           getChannelListAjax();
           
           if (myHomeTimer != null || myHomeTimer != "") {
           clearInterval(myHomeTimer);
           }
           
           myHomeTimer = setInterval(function () { getUnreadMessageCount() }, homeSyncDuration);
           
           $(':mobile-pagecontainer').pagecontainer('change', "index.html");
           
           if (chatAutoLoginTimer != null || chatAutoLoginTimer != "") {
           clearInterval(chatAutoLoginTimer);
           }
           
           } else {
           //alert("Unable To Authenticate. Check Your chat Credintials.");
           homepageAjax();
           $(':mobile-pagecontainer').pagecontainer('change', "index.html");
           
           return false;
           }
           
           },
           error: function (jqXHR, textStatus, errorThrown) {
           //alert('Network error has occurred please try again!');
           
           homepageAjax();
           $(':mobile-pagecontainer').pagecontainer('change', "index.html");
           
           if (chatAutoLoginTimer != null || chatAutoLoginTimer != "") {
           clearInterval(chatAutoLoginTimer);
           }
           
           chatAutoLoginTimer = setInterval(function () { chatAutoRetryLogin() }, 30 * 1000);
           
           }
           });
}

function chatAutoRetryLogin() {
    
    console.log("chatAutoRetryLogin >>>>> ");
    
    $.ajax({
           type: 'post',
           url: chatUrl + 'login',
           data: 'username=' + window.localStorage.getItem("rocketchatusername") + '&password=' + window.localStorage.getItem("rocketchatpassword"),
           cache: false,
           beforeSend: function () {
           $.mobile.loading('show');
           },
           complete: function () {
           $.mobile.loading('hide');
           },
           success: function (result) {
           
           // result = JSON.parse(result);
           if (result.data) {
           
           window.localStorage.setItem("chat_authToken", result.data.authToken);
           window.localStorage.setItem("chat_userId", result.data.userId);
           
           getChannelListAjax();
           
           if (myHomeTimer != null || myHomeTimer != "") {
           clearInterval(myHomeTimer);
           }
           
           myHomeTimer = setInterval(function () { getUnreadMessageCount() }, homeSyncDuration);
           
           if (chatAutoLoginTimer != null || chatAutoLoginTimer != "") {
           clearInterval(chatAutoLoginTimer);
           }
           
           } else {
           
           if (chatAutoLoginTimer != null || chatAutoLoginTimer != "") {
           clearInterval(chatAutoLoginTimer);
           }
           chatAutoLoginTimer = setInterval(function () { chatAutoRetryLogin() }, 30 * 1000);
           
           return false;
           }
           
           },
           error: function (jqXHR, textStatus, errorThrown) {
           //alert('Network error has occurred please try again!');
           
           if (chatAutoLoginTimer != null || chatAutoLoginTimer != "") {
           clearInterval(chatAutoLoginTimer);
           }
           
           chatAutoLoginTimer = setInterval(function () { chatAutoRetryLogin() }, 30 * 1000);
           
           }
           });
}

// End Rocket chat Login

$(window).on("navigate", function (event, data) {
             console.log(data.state);
             
             var pageUrl = null;
             
             if ('pageUrl' in data.state) {
             pageUrl = data.state.pageUrl;
             } else if ('url' in data.state) {
             pageUrl = data.state.url;
             }
             
             if (pageUrl != null) {
             
             if (pageUrl.indexOf("channel-page") >= 0) {
             
             //$('#channel-list-all').empty();
             
             //RefreshChannelList();
             if (myChatTimer != null || myChatTimer != "") {
             clearInterval(myChatTimer);
             }
             
             //if (myHomeTimer != null || myHomeTimer != "") {
             //    clearInterval(myHomeTimer);
             //}
             }
             else if (pageUrl.indexOf("chat-page") >= 0) {
             if (myHomeTimer != null || myHomeTimer != "") {
             clearInterval(myHomeTimer);
             }
             } else {
             if (myHomeTimer != null || myHomeTimer != "") {
             clearInterval(myHomeTimer);
             }
             
             if (myChatTimer != null || myChatTimer != "") {
             clearInterval(myChatTimer);
             }
             
             var dd = new Date();
             var ddNew = new Date(dd - 5000);
             window.localStorage.setItem("oldest_date", ddNew.toISOString());
             
             getChannelListAjax();
             
             myHomeTimer = setInterval(function () { getUnreadMessageCount() }, homeSyncDuration);
             }
             
             
             
             } else {
             if (myHomeTimer != null || myHomeTimer != "") {
             clearInterval(myHomeTimer);
             }
             
             var dd = new Date();
             var ddNew = new Date(dd - 5000);
             window.localStorage.setItem("oldest_date", ddNew.toISOString());
             
             getChannelListAjax();
             
             myHomeTimer = setInterval(function () { getUnreadMessageCount() }, homeSyncDuration);
             }
             
             });

$(document).ready(function () {
                  var _originalSize = $(window).width() + $(window).height();
                  $(window).resize(function () {
                                   if ($(window).width() + $(window).height() != _originalSize) {
                                   console.log("keyboard show up");
                                   //$(".copyright_link").css("position", "relative");
                                   } else {
                                   console.log("keyboard closed");
                                   //$(".copyright_link").css("position", "fixed");
                                   }
                                   });
                  
                  audioElement = document.createElement('audio');
                  audioElement.setAttribute('src', 'http://www.soundjay.com/button/sounds/beep-07.mp3');
                  //audioElement.setAttribute('src', "url('../sound/message_sound.mp3')");
                  
                  audio = new Audio('./sound/message_sound.mp3');
                  
                  setTimeout(function () {
                             cordova.plugins.backgroundMode.enable();
                             
                             cordova.plugins.backgroundMode.onactivate = function () {
                             var counter = 0;
                             
                             // Update badge number every second
                             // and write update to log
                             //timer = setInterval(function () {
                             //    counter++;
                             //    console.log('Running since ' + counter + ' sec');
                             //    cordova.plugins.notification.badge.set(counter);
                             //}, 1000);
                             
                             cordova.plugins.backgroundMode.setDefaults({ silent: false });
                             
                             cordova.plugins.backgroundMode.setDefaults({ title: 'myTitle', ticker: 'myTicker', text: 'myText' });
                             
                             if (myHomeTimer != null || myHomeTimer != "") {
                             clearInterval(myHomeTimer);
                             }
                             
                             myHomeTimer = setInterval(function () { getUnreadMessageCount() }, homeSyncDuration);
                             
                             };
                             
                             }, 5000);
                  
                  });

// Get channel list and show the count near the name

function getChannelListAjax() {
    var unreadMsgList = 0;
    var bChannelExist = false;
    $.ajax({
           type: 'GET',
           url: chatUrl + 'im.list',
           data: "",
           headers: {
           "Content-Type": "application/x-www-form-urlencoded",
           "X-Auth-Token": window.localStorage.getItem("chat_authToken"),
           "X-User-Id": window.localStorage.getItem("chat_userId")
           },
           
           beforeSend: function () {
           //$.mobile.loading('show');
           },
           complete: function () {
           //$.mobile.loading('hide');
           },
           success: function (result) {
           
           if (result.ims) {
           
           if (window.localStorage.getItem("channels_list") != null && window.localStorage.getItem("channels_list") != undefined)
           ChannalList = JSON.parse(window.localStorage.getItem("channels_list"));
           
           if (ChannalList.length > 0) {
           $.each(result.ims, function (i, item) {
                  
                  bChannelExist = false;
                  $.map(ChannalList, function (elementOfArray, indexInArray) {
                        
                        if (elementOfArray._id == item._id) {
                        bChannelExist = true;
                        }
                        });
                  
                  if (bChannelExist == false) {
                  ChannalList.push({
                                   '_id': item._id,
                                   'name': '',
                                   'msgs': item.msgs,
                                   'ts': item.ts,
                                   'username': '',
                                   'oldestdate': '',
                                   'LastMsg': '',
                                   'MsgDate': ''
                                   });
                  }
                  
                  });
           } else {
           $.each(result.ims, function (i, item) {
                  ChannalList.push({
                                   '_id': item._id,
                                   'name': '',
                                   'msgs': item.msgs,
                                   'ts': item.ts,
                                   'username': '',
                                   'oldestdate': '',
                                   'LastMsg': '',
                                   'MsgDate': ''
                                   });
                  
                  })
           }
           
           
           
           $.each(ChannalList, function (i, item) {
                  
                  console.log(" item >>>  " + 'im.history?roomId=' + item._id + '&unreads=true&count=1000&oldest=' + item.oldestdate);
                  
                  $.ajax({
                         type: 'GET',
                         url: chatUrl + 'im.history?roomId=' + item._id + '&unreads=true&count=1000&oldest=' + item.oldestdate,
                         data: "",
                         headers: {
                         "Content-Type": "application/x-www-form-urlencoded",
                         "X-Auth-Token": window.localStorage.getItem("chat_authToken"),
                         "X-User-Id": window.localStorage.getItem("chat_userId")
                         },
                         
                         beforeSend: function () {
                         //$.mobile.loading('show');
                         },
                         complete: function () {
                         //$.mobile.loading('hide');
                         },
                         success: function (result) {
                         
                         if (result.messages) {
                         console.log(" UnreadCount >>>> " + result.messages.length);
                         unreadMsgList = unreadMsgList + result.messages.length;
                         
                         console.log(" TotalCount >>>> " + unreadMsgList);
                         
                         $.each(result.messages, function (i, item) {
                                
                                $.map(ChannalList, function (elementOfArray, indexInArray) {
                                      
                                      if (elementOfArray._id == item.rid && item.u.username != window.localStorage.getItem("rocketchatusername")) {
                                      username = item.u.username;
                                      name = item.u.name;
                                      }
                                      });
                                
                                });
                         
                         
                         window.localStorage.setItem("unreadMsgList", unreadMsgList);
                         if (window.localStorage.getItem("unreadMsgList") == undefined || window.localStorage.getItem("unreadMsgList") == null) {
                         $('.username').html('Logged in as: ' + window.localStorage.getItem("username"));
                         $('#nameId').removeClass('usernameicon');
                         } else {
                         $('.username').html('Rocket Chat Logged in as: ' + window.localStorage.getItem("username") + " - " + window.localStorage.getItem("unreadMsgList"));
                         $('#nameId').addClass('usernameicon');
                         }
                         
                         } else {
                         alert("Unable to get channel list.");
                         return false;
                         }
                         
                         },
                         error: function (jqXHR, textStatus, errorThrown) {
                         // debugger;
                         //alert('Network error has occurred while fetching login details,please try again!');//1-
                         }
                         });
                  /// do stuff
                  });
           
           console.log(JSON.stringify(ChannalList));
           
           window.localStorage.setItem("channels_list", JSON.stringify(ChannalList));
           
           
           } else {
           //alert("Unable to get channel list.");
           return false;
           }
           
           },
           error: function (jqXHR, textStatus, errorThrown) {
           // debugger;
           //alert('Network error has occurred while fetching login details,please try again!');//1-
           }
           });
    
}

// End get count


// Unready message count for syncing process

function syncLatestUnreadMessageAjax() {
    
    var unreadMsgList = 0;
    
    var d = new Date();
    var ISODate = d.toISOString();
    
    var oldestISOdate = "";
    if (window.localStorage.getItem("oldest_date") == undefined || window.localStorage.getItem("oldest_date") == null) {
        //oldestISOdate = d.toISOString();
        var olddate = new Date(d - 60000);
        oldestISOdate = olddate.toISOString();
    } else {
        //var oldestISOdate = new Date(d - 30000);
        oldestISOdate = window.localStorage.getItem("oldest_date");
    }
    var myToast = '';
    $.ajax({
           type: 'GET',
           url: chatUrl + 'im.list',
           data: "",
           headers: {
           "Content-Type": "application/x-www-form-urlencoded",
           "X-Auth-Token": window.localStorage.getItem("chat_authToken"),
           "X-User-Id": window.localStorage.getItem("chat_userId")
           },
           
           beforeSend: function () {
           
           },
           complete: function () {
           
           },
           success: function (result) {
           
           if (result.ims) {
           
           $.each(result.ims, function (i, item) {
                  
                  console.log(chatUrl + 'im.history?roomId=' + item._id + '&unreads=true&count=1000&inclusive=true&oldest=' + oldestISOdate + '&latest=' + ISODate);
                  
                  $.ajax({
                         type: 'GET',
                         url: chatUrl + 'im.history?roomId=' + item._id + '&unreads=true&count=1000&inclusive=true&oldest=' + oldestISOdate + '&latest=' + ISODate,
                         data: "",
                         headers: {
                         "Content-Type": "application/x-www-form-urlencoded",
                         "X-Auth-Token": window.localStorage.getItem("chat_authToken"),
                         "X-User-Id": window.localStorage.getItem("chat_userId")
                         },
                         
                         beforeSend: function () {
                         
                         },
                         complete: function () {
                         
                         },
                         success: function (result) {
                         
                         var dd = new Date(ISODate);
                         var ddNew = new Date(dd - 2000);
                         window.localStorage.setItem("oldest_date", ddNew.toISOString());
                         
                         if (result.messages && result.messages.length > 0) {
                         
                         if (window.localStorage.getItem("rocketchatusername") != result.messages[0].u.username) {
                         
                         var recentMsg = JSON.stringify(result.messages[0]);
                         
                         $.each(result.messages, function (i, item) {
                                
                                var displayMsg = "";
                                
                                if (item.msg != null && item.msg != "" && (item.attachments == null || item.attachments == '' || item.attachments == undefined)) {
                                displayMsg = '<div class = "toastmessage"  id = "' + item.rid + '" onclick="LoadChatListPage(this.id)" >' + item.msg.replace(new RegExp('\n', 'g'), '<br>') + '</div>';
                                } else {
                                
                                displayMsg = '<div class = "toastmessage"  id = "' + item.rid + '" onclick="LoadChatListPage(this.id)" > Image File has been sent to you</div>';
                                
                                }
                                
                                myToast = $.toast({
                                                  text: displayMsg,
                                                  heading: '<div class = "toastmessage"  id = "' + item.rid + '" onclick="LoadChatListPage(this.id)" >' + item.u.username + '</div>',
                                                  icon: 'info',
                                                  showHideTransition: 'plain',
                                                  position: 'mid-center',
                                                  hideAfter: 30000,
                                                  stack: 100,
                                                  allowToastClose: true
                                                  })
                                
                                });
                         
                         //audioElement.play();
                         audio.play();
                         }
                         } else {
                         
                         return false;
                         }
                         
                         },
                         error: function (jqXHR, textStatus, errorThrown) {
                         //alert('Network error has occurred while fetching login details,please try again!');//1-
                         }
                         });
                  
                  });
           
           
           } else {
           
           return false;
           }
           
           },
           error: function (jqXHR, textStatus, errorThrown) {
           // debugger;
           //alert('Network error has occurred while fetching login details,please try again!');//1-
           }
           });
    
}

//$('.jq-toast-single').click(function () {
//    alert(1);
//});

function getUnreadMessageCount() {
    console.log(" UnreadMessageCount >>>> Called ");
    
    getChannelListAjax();
    syncLatestUnreadMessageAjax();
}

function myStopFunction() {
    clearInterval(myVar);
}

// End the get unread message


// Show the channel name with last message in the each list item of the list

function LoadChannelListPage() {
    
    if (window.localStorage.getItem("chat_userId") != undefined && window.localStorage.getItem("chat_userId") != null) {
        
        $(':mobile-pagecontainer').pagecontainer('change', "#channel-page");
        
        if (myHomeTimer != null || myHomeTimer != "") {
            clearInterval(myHomeTimer);
        }
        
        RefreshChannelList();
        
        //downloadFile('https://chat.digitalcentral.com.au/file-upload/gJLJg6X8tvSXZDeL3/image.jpg');
        
    }
    
}

var folderName = 'dc';
var fileName;

function downloadFile(URL) {
    //step to request a file system
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, fileSystemSuccess, fileSystemFail);
    
    function fileSystemSuccess(fileSystem) {
        var download_link = encodeURI(URL);
        fileName = download_link.substr(download_link.lastIndexOf('/') + 1); //Get filename of URL
        var directoryEntry = fileSystem.root; // to get root path of directory
        directoryEntry.getDirectory(folderName, {
                                    create: true,
                                    exclusive: false
                                    }, onDirectorySuccess, onDirectoryFail); // creating folder in sdcard
        var rootdir = fileSystem.root;
        var fp = fileSystem.root.toNativeURL(); // Returns Fullpath of local directory
        
        fp = fp + "/" + folderName + "/" + fileName; // fullpath and name of the file which we want to give
        // download function call
        filetransfer(download_link, fp);
    }
    
    function onDirectorySuccess(parent) {
        // Directory created successfuly
    }
    
    function onDirectoryFail(error) {
        //Error while creating directory
        alert("Unable to create new directory: " + error.code);
        
    }
    
    function fileSystemFail(evt) {
        //Unable to access file system
        alert(evt.target.error.code);
    }
}

function filetransfer(download_link, fp) {
    var fileTransfer = new FileTransfer();
    // File download function with URL and local path
    fileTransfer.download(download_link, fp,
                          function (entry) {
                          alert("download complete: " + entry.fullPath);
                          },
                          function (error) {
                          //Download abort errors or download failed errors
                          alert("download error source " + error.source);
                          }
                          );
}

function RefreshChannelList() {
    
    var ali = '';
    
    $('#channel-list-all').empty();
    
    $.ajax({
           type: 'GET',
           url: chatUrl + 'im.list',
           data: "",
           headers: {
           "Content-Type": "application/x-www-form-urlencoded",
           "X-Auth-Token": window.localStorage.getItem("chat_authToken"),
           "X-User-Id": window.localStorage.getItem("chat_userId")
           },
           
           beforeSend: function () {
           $.mobile.loading('show');
           },
           complete: function () {
           $.mobile.loading('hide');
           },
           success: function (result) {
           
           if (result.ims) {
           
           var removeDeletedChannel = JSON.parse(window.localStorage.getItem("channels_list"));
           var newChannalList = [];
           
           $.each(result.ims, function (i, data) {
                  
                  $.each(removeDeletedChannel, function (i1, data1) {
                         
                         if (data1._id == data._id) {
                         newChannalList.push(data1);
                         }
                         });
                  
                  });
           
           
           if (newChannalList.length > 0) {
           $.each(result.ims, function (i, item3) {
                  bChannelExist = false;
                  $.map(newChannalList, function (elementOfArray, indexInArray) {
                        if (elementOfArray._id == item3._id) {
                        bChannelExist = true;
                        }
                        });
                  
                  if (bChannelExist == false) {
                  newChannalList.push({
                                      '_id': item3._id,
                                      'name': '',
                                      'msgs': item3.msgs,
                                      'ts': item3.ts,
                                      'username': '',
                                      'oldestdate': '',
                                      'LastMsg': '',
                                      'MsgDate': ''
                                      });
                  }
                  
                  });
           }
           
           window.localStorage.setItem("channels_list", JSON.stringify(newChannalList));
           
           
           //window.localStorage.setItem("channels_list", JSON.stringify(result.channels));
           
           $.each(result.ims, function (i, item) {
                  
                  console.log(" item >>>  " + item._id);
                  
                  
                  
                  $.ajax({
                         type: 'GET',
                         url: chatUrl + 'im.history?roomId=' + item._id + '&unreads=true&count=1000',
                         data: "",
                         headers: {
                         "Content-Type": "application/x-www-form-urlencoded",
                         "X-Auth-Token": window.localStorage.getItem("chat_authToken"),
                         "X-User-Id": window.localStorage.getItem("chat_userId")
                         },
                         
                         beforeSend: function () {
                         $.mobile.loading('show');
                         },
                         complete: function () {
                         $.mobile.loading('hide');
                         },
                         success: function (result) {
                         
                         
                         if (result.messages && result.messages.length > 0) {
                         var channelListData = JSON.parse(window.localStorage.getItem("channels_list"));
                         
                         $.each(channelListData, function (i1, item1) {
                                
                                if (item1._id == result.messages[0].rid) {
                                if (result.messages[0].msg == "")
                                item1.LastMsg = "File";
                                else
                                item1.LastMsg = result.messages[0].msg;
                                
                                item1.MsgDate = result.messages[0].ts;
                                
                                $.each(result.messages, function (i, item) {
                                       
                                       if (item1._id == item.rid && item.u.username != window.localStorage.getItem("rocketchatusername")) {
                                       item1.username = item.u.username;
                                       item1.name = item.u.name;
                                       }
                                       
                                       });
                                
                                
                                
                                if (result.messages[0].msg != null && result.messages[0].msg != "" && result.messages[0].LastMsg != "File") {
                                ali = '<li id = "' + item1._id + '" onclick="LoadChatListPage(this.id)"><p>Name : ' + item1.username + '</p> <p>Message : ' + result.messages[0].msg.replace(new RegExp('\n', 'g'), '<br>') + '</p></li>';
                                } else if (item1.LastMsg == "File") {
                                ali = '<li id = "' + item1._id + '" onclick="LoadChatListPage(this.id)"><p>Name : ' + item1.username + '</p> <p>Message : <span class="attachicon"> </span> File </p></li>';
                                } else {
                                ali = '<li id = "' + item1._id + '" onclick="LoadChatListPage(this.id)"><p>Name : ' + item1.username + '</p> <p>Message : </p></li>';
                                }
                                
                                $('#channel-list-all').append(ali);
                                
                                $('#channel-list-all').listview().listview('refresh');
                                
                                }
                                
                                });
                         
                         console.log(channelListData);
                         
                         window.localStorage.setItem("channels_list", JSON.stringify(channelListData));
                         
                         channelListData = JSON.parse(window.localStorage.getItem("channels_list"));
                         
                         channelListData.sort(function (a, b) {
                                              return new Date(b.MsgDate) - new Date(a.MsgDate);
                                              });
                         
                         $('#channel-list-all').empty();
                         
                         $.each(channelListData, function (i1, item1) {
                                
                                var username = "";
                                if (item1.username == "") {
                                username = "Name show after user response your message (Version issue)";
                                } else {
                                username = item1.username
                                }
                                
                                if (item1.LastMsg != null && item1.LastMsg != "File") {
                                ali = '<li id = "' + item1._id + '" onclick="LoadChatListPage(this.id)"><p>Name : ' + username + '</p> <p>Message : ' + item1.LastMsg.replace(new RegExp('\n', 'g'), '<br>') + '</p></li>';
                                } else if (item1.LastMsg == "File") {
                                ali = '<li id = "' + item1._id + '" onclick="LoadChatListPage(this.id)"><p>Name : ' + username + '</p> <p>Message : <span class="attachicon"> </span> File </p></li>';
                                } else {
                                ali = '<li id = "' + item1._id + '" onclick="LoadChatListPage(this.id)"><p>Name : ' + username + '</p> <p>Message : </p></li>';
                                }
                                
                                $('#channel-list-all').append(ali);
                                $('#channel-list-all').listview().listview('refresh');
                                });
                         
                         
                         } else {
                         //alert('Channel list Failed!');
                         //return false;
                         }
                         
                         },
                         error: function (jqXHR, textStatus, errorThrown) {
                         // debugger;
                         
                         }
                         });
                  
                  
                  });
           
           } else {
           //alert("Unable to get channel list.");
           return false;
           }
           
           },
           error: function (jqXHR, textStatus, errorThrown) {
           //alert('Network error has occurred while fetching login details,please try again!');//1-
           }
           });
}

function GetUser() {
    
    $.ajax({
           type: 'GET',
           url: chatUrl + 'users.list',
           data: "",
           headers: {
           "Content-Type": "application/x-www-form-urlencoded",
           "X-Auth-Token": window.localStorage.getItem("chat_authToken"),
           "X-User-Id": window.localStorage.getItem("chat_userId")
           },
           
           beforeSend: function () {
           $.mobile.loading('show');
           },
           complete: function () {
           $.mobile.loading('hide');
           },
           success: function (result) {
           
           if (result.users) {
           
           $('#userListhPopupId').popup('open');
           
           $('#user-list-all').empty();
           
           $.each(result.users, function (i, item) {
                  ali = '<li id = "' + item._id + '" onclick="AddUserToRoom(this.id)"><p>Name : ' + item.username + '</p></li>';
                  
                  $('#user-list-all').append(ali);
                  $('#user-list-all').listview().listview('refresh');
                  });
           
           
           } else {
           return false;
           }
           
           },
           error: function (jqXHR, textStatus, errorThrown) {
           //alert('Network error has occurred while fetching login details,please try again!');//1-
           }
           });
    
}

function closeUserListPopup() {
    $('#userListhPopupId').popup('close');
}

function AddUserToRoom(id) {
    
    $.ajax({
           type: 'POST',
           url: chatUrl + 'im.create',
           data: {
           "username": id
           },
           headers: {
           "Content-Type": "application/x-www-form-urlencoded",
           "X-Auth-Token": window.localStorage.getItem("chat_authToken"),
           "X-User-Id": window.localStorage.getItem("chat_userId")
           },
           
           beforeSend: function () {
           $.mobile.loading('show');
           },
           complete: function () {
           $.mobile.loading('hide');
           },
           success: function (result) {
           
           if (result.room) {
           LoadChatListPage(result.room._id);
           } else {
           return false;
           }
           
           },
           error: function (jqXHR, textStatus, errorThrown) {
           //alert('Network error has occurred while fetching login details,please try again!');//1-
           }
           });
    
}

function LoadChatListPage(id) {
    
    $(':mobile-pagecontainer').pagecontainer('change', "#chat-page");
    
    window.localStorage.setItem("selectedChannelId", id);
    
    RefreshChatList();
    
    var d = new Date();
    window.localStorage.setItem("chatSyncDate", d.toISOString());
    
    myChatTimer = setInterval(function () { SyncChatList() }, 10000);
    
    $.toast().reset('all');
    
}


function openChatURL(urlString) {
    OpenUrlExt.open(urlString, onSuccessURL, onFailureURL);
}


function SyncChatList() {
    
    var bMessageExist = false;
    
    var messagecount = 0;
    
    var oldheight = 0;
    
    var d = new Date();
    var ISODate = d.toISOString();
    
    var oldestISOdate = "";
    if (window.localStorage.getItem("chatSyncDate") == undefined || window.localStorage.getItem("chatSyncDate") == null) {
        oldestISOdate = d.toISOString();
    } else {
        oldestISOdate = window.localStorage.getItem("chatSyncDate");
    }
    
    console.log('im.history?roomId=' + window.localStorage.getItem("selectedChannelId") + '&unreads=true&count=1000&inclusive=true&oldest=' + oldestISOdate + '&latest=' + ISODate);
    
    $.ajax({
           type: 'GET',
           url: chatUrl + 'im.history?roomId=' + window.localStorage.getItem("selectedChannelId") + '&count=1000&unreads=true&count=1000&inclusive=true&oldest=' + oldestISOdate + '&latest=' + ISODate,
           data: "",
           headers: {
           "Content-Type": "application/x-www-form-urlencoded",
           "X-Auth-Token": window.localStorage.getItem("chat_authToken"),
           "X-User-Id": window.localStorage.getItem("chat_userId")
           },
           
           beforeSend: function () {
           
           },
           complete: function () {
           
           },
           success: function (result) {
           
           var dd = new Date(ISODate);
           var ddNew = new Date(dd - 60000);
           window.localStorage.setItem("chatSyncDate", ddNew.toISOString());
           
           if (result.messages) {
           
           console.log("MessageCount >>>> " + result.messages.length);
           
           messagecount = result.messages.length;
           
           result.messages.sort(function (a, b) {
                                return new Date(a.ts) - new Date(b.ts);
                                });
           
           $.each(result.messages, function (i, item) {
                  
                  bMessageExist = false;
                  $.map(ChatList, function (elementOfArray, indexInArray) {
                        
                        if (elementOfArray.msgId == item._id) {
                        bMessageExist = true;
                        }
                        });
                  
                  
                  if (bMessageExist == false) {
                  
                  //audioElement.play();
                  
                  
                  var attachmentTitle = '';
                  var attachmentDescription = '';
                  var attachmentTitle_link = '';
                  var fileType = '';
                  
                  if (item.attachments != null && item.attachments != undefined && item.attachments.length > 0) {
                  attachmentTitle = item.attachments[0].title;
                  attachmentDescription = item.attachments[0].description;
                  attachmentTitle_link = item.attachments[0].title_link;
                  fileType = item.file.type;
                  }
                  
                  ChatList.push({
                                'msgId': item._id,
                                'channelId': item.rid,
                                'msg': item.msg,
                                'msgDate': item.ts,
                                'userId': item.u._id,
                                'username': item.u.username,
                                'name': item.u.name,
                                'attachmentTitle': attachmentTitle,
                                'attachmentDescription': attachmentDescription,
                                'attachmentTitle_link': attachmentTitle_link,
                                'fileType': fileType
                                
                                });
                  
                  var today = new Date(item.ts);
                  var cDate = today.getDate();
                  var cMonth = today.getMonth();
                  var cYear = today.getFullYear();
                  
                  var cHour = today.getHours() < 10 ? "0" + today.getHours() : today.getHours();
                  var cMin = today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes();
                  var cSec = today.getSeconds() < 10 ? "0" + today.getSeconds() : today.getSeconds();
                  
                  var convertedDate = monthNames[cMonth] + " " + cDate + ", " + cYear + " " + tConvert(cHour + ":" + cMin + ":" + cSec);
                  
                  oldheight = oldheight + $('#chat')[0].scrollHeight;
                  
                  
                  var urlArr = item.msg.match(/(?:https?|ftp|http):\/\/\S+/gi);
                  var msg = item.msg.replace(/(?:https?|ftp):\/\/[\n\S]+/g, '').replace(/(?:http?|ftp):\/\/[\n\S]+/g, '').replace(new RegExp('\n', 'g'), '<br>');
                  
                  if (urlArr != null) {
                  for (var i = 0; i < urlArr.length; i++) {
                  msg = msg + urlArr[i].replace(urlArr[i], '<a onclick="openChatURL(\'' + urlArr[i] + '\')">' + urlArr[i] + '</a><br>')
                  }
                  }
                  
                  if (window.localStorage.getItem("rocketchatusername") == item.u.username) {
                  if (item.msg != null && item.msg != "" && (item.attachments == null || item.attachments == '' || item.attachments == undefined)) {
                  $('#messages-box').append('<div class="you"> <div class="user"><div class="userimg"><div class="chatusername">' + item.u.username + '</div></div></div> <div class="messagechatbox">' + msg + '</div> <div class="date">' + convertedDate + '</div> </div>');
                  } else if (item.file != null) {
                  if (item.file.type == 'image/jpeg' || item.file.type == 'image/gif' || item.file.type == 'application/pdf' || item.file.type == 'image/ png') {
                  $('#messages-box').append('<div class="you"> <div class="user"><div class="userimg"><div class="chatusername">' + item.u.username + '</div></div></div> <div class="messagechatbox"> <span class="attachicon"> </span> ' + item.attachments[0].title + '<div> ' + msg + '<div class="inline-image" style="background-image: url("' + chatImageUrl + item.attachments[0].title_link + '");"> <img src= "' + chatImageUrl + item.attachments[0].title_link + '" height= "200" class="gallery-item" onclick="imageViewPopup(\'' + chatImageUrl + item.attachments[0].title_link + '\')" ></div > </div> <div class="date">' + convertedDate + '</div>  </div>');
                  }
                  } else {
                  $('#messages-box').append('<div class="you"> <div class="user"><div class="userimg"><div class="chatusername">' + item.u.username + '</div></div></div> <div class="messagechatbox"> <span class="attachicon"> </span> ' + item.attachments[0].title + '<div class="date">' + convertedDate + '</div>  </div>');
                  }
                  } else {
                  
                  audio.play();
                  
                  if (item.msg != null && item.msg != "" && (item.attachments == null || item.attachments == '' || item.attachments == undefined)) {
                  $('#messages-box').append('<div class="other"><div class="user"><div class="userimg"><div class="chatusername">' + item.u.username + '</div></div></div>  <div class="messagechatbox">' + msg + '</div> <div class="date">' + convertedDate + '</div></div>');
                  } else if (item.file != null) {
                  if (item.file.type == 'image/jpeg' || item.file.type == 'image/gif' || item.file.type == 'application/pdf' || item.file.type == 'image/ png') {
                  $('#messages-box').append('<div class="other"> <div class="user"><div class="userimg"><div class="chatusername">' + item.u.username + '</div></div></div>  <div class="messagechatbox"> <span class="attachicon"> </span> ' + item.attachments[0].title + '<div> ' + msg + ' </div><div class="inline-image" style="background-image: url("' + chatImageUrl + item.attachments[0].title_link + '");"> <img src="' + chatImageUrl + item.attachments[0].title_link + '" height= "200" class="gallery-item" onclick="imageViewPopup(\'' + chatImageUrl + item.attachments[0].title_link + '\')" ></div ></div> <div class="date">' + convertedDate + '</div> </div>');
                  }
                  } else {
                  $('#messages-box').append('<div class="other"> <div class="user"><div class="userimg"><div class="chatusername">' + item.u.username + '</div></div></div> <div class="messagechatbox"> <span class="attachicon"> </span> ' + item.attachments[0].title + '</div> <div class="date">' + convertedDate + '</div>  </div>');
                  }
                  }
                  
                  
                  }
                  
                  });
           
           ChatList.sort(function (a, b) {
                         // Turn your strings into dates, and then subtract them
                         // to get a value that is either negative, positive, or zero.
                         return new Date(a.msgDate) - new Date(b.msgDate);
                         });
           
           } else {
           //alert("Unable to get channel list.");
           return false;
           }
           
           console.log($('#chat').scrollTop());
           console.log($('#chat').innerHeight());
           console.log($('#chat')[0].scrollHeight);
           
           var oldNewHeightdifferent = 0;
           
           if (oldheight > 0) {
           oldNewHeightdifferent = $('#chat')[0].scrollHeight - oldheight;
           }
           
           console.log(oldNewHeightdifferent);
           
           if ($('#chat').scrollTop() + $('#chat').innerHeight() + oldNewHeightdifferent + 700 >= $('#chat')[0].scrollHeight) {
           ScrollBottom();
           }
           
           //ScrollBottom();
           
           var dDate = new Date();
           var channelListData = JSON.parse(window.localStorage.getItem("channels_list"));
           
           $.each(channelListData, function (i1, item1) {
                  if (item1._id == window.localStorage.getItem("selectedChannelId")) {
                  item1.oldestdate = dDate.toISOString();
                  }
                  });
           
           window.localStorage.setItem("channels_list", JSON.stringify(channelListData));
           
           
           },
           error: function (jqXHR, textStatus, errorThrown) {
           // debugger;
           //alert('Network error has occurred while fetching login details,please try again!');//1-
           }
           });
}

function GetChannelInfo() {
    
    $.ajax({
           type: 'GET',
           url: chatUrl + 'im.list',
           data: "",
           headers: {
           "Content-Type": "application/x-www-form-urlencoded",
           "X-Auth-Token": window.localStorage.getItem("chat_authToken"),
           "X-User-Id": window.localStorage.getItem("chat_userId")
           },
           
           beforeSend: function () {
           $.mobile.loading('show');
           },
           complete: function () {
           $.mobile.loading('hide');
           },
           success: function (result) {
           
           if (result.ims) {
           
           $.each(result.ims, function (i, item) {
                  if (item._id == window.localStorage.getItem("selectedChannelId")) {
                  
                  var username = "";
                  //if (item.usernames[0] != window.localStorage.getItem("rocketchatusername")) {
                  //    username = item.usernames[0];
                  //} else {
                  //    username = item.usernames[1];
                  //}
                  
                  $('.channelname').html(username);
                  
                  window.localStorage.setItem("channelname", username);
                  
                  loadMoreCount = parseInt(item.msgs / 50);
                  
                  if (loadMoreCount > 0) {
                  $(".chartmore").css("visibility", "visible");
                  } else {
                  $(".chartmore").css("visibility", "hidden");
                  }
                  }
                  });
           
           //window.localStorage.setItem("channel_info", result.channel);
           
           
           } else {
           return false;
           }
           
           },
           error: function (jqXHR, textStatus, errorThrown) {
           //alert('Network error has occurred while fetching login details,please try again!');//1-
           }
           });
}

function LoadMore() {
    
    var bLoadMore = false;
    
    console.log('im.history?roomId=' + window.localStorage.getItem("selectedChannelId") + '&count=50&latest=' + window.localStorage.getItem("chatLatestDate"));
    
    $.ajax({
           type: 'GET',
           url: chatUrl + 'im.history?roomId=' + window.localStorage.getItem("selectedChannelId") + '&count=50&latest=' + window.localStorage.getItem("chatLatestDate"),
           data: "",
           headers: {
           "Content-Type": "application/x-www-form-urlencoded",
           "X-Auth-Token": window.localStorage.getItem("chat_authToken"),
           "X-User-Id": window.localStorage.getItem("chat_userId")
           },
           
           beforeSend: function () {
           $.mobile.loading('show');
           },
           complete: function () {
           $.mobile.loading('hide');
           },
           success: function (result) {
           
           if (result.messages) {
           
           //var dd = new Date(result.messages[0].ts);
           //var ddNew = new Date(dd + 10000);
           window.localStorage.setItem("chatLatestDate", result.messages[result.messages.length - 1].ts);
           
           result.messages.sort(function (a, b) {
                                return new Date(b.ts) - new Date(a.ts);
                                });
           
           $.each(result.messages, function (i, item) {
                  
                  bLoadMore = false;
                  $.map(ChatList, function (elementOfArray, indexInArray) {
                        
                        if (elementOfArray.msgId == item._id) {
                        bLoadMore = true;
                        }
                        });
                  
                  if (bLoadMore == false) {
                  
                  var attachmentTitle = '';
                  var attachmentDescription = '';
                  var attachmentTitle_link = '';
                  var fileType = '';
                  
                  if (item.attachments != null && item.attachments != undefined && item.attachments.length > 0) {
                  attachmentTitle = item.attachments[0].title;
                  attachmentDescription = item.attachments[0].description;
                  attachmentTitle_link = item.attachments[0].title_link;
                  fileType = item.file.type;
                  }
                  
                  ChatList.push({
                                'msgId': item._id,
                                'channelId': item.rid,
                                'msg': item.msg,
                                'msgDate': item.ts,
                                'userId': item.u._id,
                                'username': item.u.username,
                                'name': item.u.name,
                                'attachmentTitle': attachmentTitle,
                                'attachmentDescription': attachmentDescription,
                                'attachmentTitle_link': attachmentTitle_link,
                                'fileType': fileType
                                });
                  
                  var today = new Date(item.ts);
                  var cDate = today.getDate();
                  var cMonth = today.getMonth();
                  var cYear = today.getFullYear();
                  
                  var cHour = today.getHours() < 10 ? "0" + today.getHours() : today.getHours();
                  var cMin = today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes();
                  var cSec = today.getSeconds() < 10 ? "0" + today.getSeconds() : today.getSeconds();
                  
                  var convertedDate = monthNames[cMonth] + " " + cDate + ", " + cYear + " " + tConvert(cHour + ":" + cMin + ":" + cSec);
                  
                  var urlArr = item.msg.match(/(?:https?|ftp|http):\/\/\S+/gi);
                  var msg = item.msg.replace(/(?:https?|ftp):\/\/[\n\S]+/g, '').replace(/(?:http?|ftp):\/\/[\n\S]+/g, '').replace(new RegExp('\n', 'g'), '<br>');
                  
                  if (urlArr != null) {
                  for (var i = 0; i < urlArr.length; i++) {
                  msg = msg + urlArr[i].replace(urlArr[i], '<a onclick="openChatURL(\'' + urlArr[i] + '\')">' + urlArr[i] + '</a><br>')
                  }
                  }
                  
                  if (window.localStorage.getItem("rocketchatusername") == item.username) {
                  if (item.msg != null && item.msg != "" && (item.attachments == null || item.attachments == '' || item.attachments == undefined)) {
                  $('#messages-box').append('<div class="you"> <div class="user"><div class="userimg"><div class="chatusername">' + item.u.username + '</div></div></div> <div class="messagechatbox">' + msg + '</div> <div class="date">' + convertedDate + '</div>  </div>');
                  } else if (item.file != null) {
                  if (item.file.type == 'image/jpeg' || item.file.type == 'image/gif' || item.file.type == 'application/pdf' || item.file.type == 'image/ png') {
                  $('#messages-box').append('<div class="you"> <div class="user"><div class="userimg"><div class="chatusername">' + item.u.username + '</div></div></div> <div class="messagechatbox"> <span class="attachicon"> </span> ' + item.attachments[0].title + '<div> ' + msg + '<div class="inline-image" style="background-image: url("' + chatImageUrl + item.attachments[0].title_link + '");"> <img src= "' + chatImageUrl + item.attachments[0].title_link + '" height= "200" class="gallery-item" ></div > </div> <div class="date">' + convertedDate + '</div>  </div>');
                  }
                  } else {
                  $('#messages-box').append('<div class="you"> <div class="user"><div class="userimg"><div class="chatusername">' + item.u.username + '</div></div></div> <div class="messagechatbox"> <span class="attachicon"> </span> ' + item.attachments[0].title + '<div class="date">' + convertedDate + '</div>  </div>');
                  }
                  } else {
                  if (item.msg != null && item.msg != "" && (item.attachments == null || item.attachments == '' || item.attachments == undefined)) {
                  $('#messages-box').append('<div class="other"> <div class="user"><div class="userimg"><div class="chatusername">' + item.u.username + '</div></div></div>  <div class="messagechatbox">' + msg + '</div> <div class="date">' + convertedDate + '</div> </div>');
                  } else if (item.file != null) {
                  if (item.file.type == 'image/jpeg' || item.file.type == 'image/gif' || item.file.type == 'application/pdf' || item.file.type == 'image/ png') {
                  $('#messages-box').append('<div class="other"> <div class="user"><div class="userimg"><div class="chatusername">' + item.u.username + '</div></div></div>  <div class="messagechatbox"> <span class="attachicon"> </span> ' + item.attachmentTitle + '<div> ' + msg + ' </div><div class="inline-image" style="background-image: url("' + chatImageUrl + item.attachments[0].title_link + '");"> <img src="' + chatImageUrl + item.attachmentTitle_link + '" height= "200" class="gallery-item" ></div ></div> <div class="date">' + convertedDate + '</div> </div>');
                  }
                  } else {
                  $('#messages-box').append('<div class="other"> <div class="user"><div class="userimg"><div class="chatusername">' + item.u.username + '</div></div></div> <div class="messagechatbox"> <span class="attachicon"> </span> ' + item.attachmentTitle + '</div> <div class="date">' + convertedDate + '</div>  </div>');
                  }
                  }
                  
                  }
                  
                  });
           
           loadMoreClicked++;
           
           if (loadMoreCount > loadMoreClicked) {
           $(".chartmore").css("visibility", "visible");
           } else {
           $(".chartmore").css("visibility", "hidden");
           }
           
           ChatList.sort(function (a, b) {
                         return new Date(a.msgDate) - new Date(b.msgDate);
                         });
           
           } else {
           // alert("Unable to get channel list.");
           return false;
           }
           
           },
           error: function (jqXHR, textStatus, errorThrown) {
           // debugger;
           // alert('Network error has occurred while fetching login details,please try again!');//1-
           }
           });
    
}

function RefreshChatList() {
    
    GetChannelInfo();
    
    loadMoreClicked = 0;
    
    ChatList = [];
    $('#messages-box').empty();
    $('#messages-box').listview().listview('refresh');
    
    $.ajax({
           type: 'GET',
           url: chatUrl + 'im.history?roomId=' + window.localStorage.getItem("selectedChannelId") + '&count=50',
           data: "",
           headers: {
           "Content-Type": "application/x-www-form-urlencoded",
           "X-Auth-Token": window.localStorage.getItem("chat_authToken"),
           "X-User-Id": window.localStorage.getItem("chat_userId")
           },
           
           beforeSend: function () {
           $.mobile.loading('show');
           },
           complete: function () {
           $.mobile.loading('hide');
           },
           success: function (result) {
           
           if (result.messages) {
           
           window.localStorage.setItem("chatLatestDate", result.messages[result.messages.length - 1].ts);
           
           $.each(result.messages, function (i, item) {
                  
                  var attachmentTitle = '';
                  var attachmentDescription = '';
                  var attachmentTitle_link = '';
                  var fileType = '';
                  
                  if (item.attachments != null && item.attachments != undefined && item.attachments.length > 0) {
                  attachmentTitle = item.attachments[0].title;
                  attachmentDescription = item.attachments[0].description;
                  attachmentTitle_link = item.attachments[0].title_link;
                  fileType = item.file.type;
                  }
                  
                  
                  ChatList.push({
                                'msgId': item._id,
                                'channelId': item.rid,
                                'msg': item.msg,
                                'msgDate': item.ts,
                                'userId': item.u._id,
                                'username': item.u.username,
                                'name': item.u.name,
                                'attachmentTitle': attachmentTitle,
                                'attachmentDescription': attachmentDescription,
                                'attachmentTitle_link': attachmentTitle_link,
                                'fileType': fileType
                                
                                });
                  
                  });
           
           ChatList.sort(function (a, b) {
                         return new Date(a.msgDate) - new Date(b.msgDate);
                         });
           
           
           $.each(ChatList, function (i, item1) {
                  
                  var today = new Date(item1.msgDate);
                  var cDate = today.getDate();
                  var cMonth = today.getMonth();
                  var cYear = today.getFullYear();
                  
                  var cHour = today.getHours() < 10 ? "0" + today.getHours() : today.getHours();
                  var cMin = today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes();
                  var cSec = today.getSeconds() < 10 ? "0" + today.getSeconds() : today.getSeconds();
                  
                  var convertedDate = monthNames[cMonth] + " " + cDate + ", " + cYear + " " + tConvert(cHour + ":" + cMin + ":" + cSec);
                  
                  var urlArr = item1.msg.match(/(?:https?|ftp|http):\/\/\S+/gi);
                  var msg = item1.msg.replace(/(?:https?|ftp):\/\/[\n\S]+/g, '').replace(/(?:http?|ftp):\/\/[\n\S]+/g, '').replace(new RegExp('\n', 'g'), '<br>');
                  
                  if (urlArr != null) {
                  for (var i = 0; i < urlArr.length; i++) {
                  msg = msg + urlArr[i].replace(urlArr[i], '<a onclick="openChatURL(\'' + urlArr[i] + '\')">' + urlArr[i] + '</a><br>')
                  }
                  }
                  
                  if (window.localStorage.getItem("rocketchatusername") == item1.username) {
                  if (item1.msg != null && item1.msg != "" && item1.attachmentTitle == '') {
                  $('#messages-box').append('<div class="you"> <div class="user"><div class="userimg"><div class="chatusername">' + item1.username + '</div></div></div> <div class="messagechatbox">' + msg + '</div> <div class="date">' + convertedDate + '</div>  </div>');
                  } else if (item1.fileType == 'image/jpeg' || item1.fileType == 'image/gif' || item1.fileType == 'application/pdf' || item1.fileType == 'image/png') {
                  $('#messages-box').append('<div class="you"> <div class="user"><div class="userimg"><div class="chatusername">' + item1.username + '</div></div></div> <div class="messagechatbox"> <span class="attachicon"> </span> ' + item1.attachmentTitle + '<div> ' + msg + '<div class="inline-image" style="background-image: url("' + chatImageUrl + item1.attachmentTitle_link + '");"  > <img src= "' + chatImageUrl + item1.attachmentTitle_link + '" height= "200" class="gallery-item"  onclick="imageViewPopup(\'' + chatImageUrl + item1.attachmentTitle_link + '\')"></div > </div> <div class="date">' + convertedDate + '</div>  </div>');
                  } else {
                  $('#messages-box').append('<div class="you"> <div class="user"><div class="userimg"><div class="chatusername">' + item1.username + '</div></div></div> <div class="messagechatbox"> <span class="attachicon"> </span> ' + item1.attachmentTitle + '<div class="date">' + convertedDate + '</div>  </div>');
                  }
                  } else {
                  if (item1.msg != null && item1.msg != "" && item1.attachmentTitle == '') {
                  $('#messages-box').append('<div class="other"> <div class="user"><div class="userimg"><div class="chatusername">' + item1.username + '</div></div></div>  <div class="messagechatbox">' + msg + '</div> <div class="date">' + convertedDate + '</div> </div>');
                  } else if (item1.fileType == 'image/jpeg' || item1.fileType == 'image/gif' || item1.fileType == 'application/pdf' || item1.fileType == 'image/png') {
                  $('#messages-box').append('<div class="other"> <div class="user"><div class="userimg"><div class="chatusername">' + item1.username + '</div></div></div>  <div class="messagechatbox"> <span class="attachicon"> </span> ' + item1.attachmentTitle + '<div> ' + msg + ' </div><div class="inline-image" style="background-image: url("' + chatImageUrl + item1.attachmentTitle_link + '");" > <img src="' + chatImageUrl + item1.attachmentTitle_link + '" height= "200" class="gallery-item" onclick="imageViewPopup(\'' + chatImageUrl + item1.attachmentTitle_link + '\')"></div ></div> <div class="date">' + convertedDate + '</div> </div>');
                  } else {
                  $('#messages-box').append('<div class="other"> <div class="user"><div class="userimg"><div class="chatusername">' + item1.username + '</div></div></div> <div class="messagechatbox"> <span class="attachicon"> </span> ' + item1.attachmentTitle + '</div> <div class="date">' + convertedDate + '</div>  </div>');
                  }
                  }
                  
                  
                  setTimeout(function () {
                             var wtf = $('#chat');
                             var height = wtf[0].scrollHeight;
                             wtf.scrollTop(height);
                             }, 1000);
                  
                  });
           
           
           
           } else {
           // alert("Unable to get channel list.");
           return false;
           }
           
           ScrollBottom();
           
           var dDate = new Date();
           var channelListData = JSON.parse(window.localStorage.getItem("channels_list"));
           
           $.each(channelListData, function (i1, item1) {
                  
                  if (item1._id == window.localStorage.getItem("selectedChannelId")) {
                  item1.oldestdate = dDate.toISOString();
                  }
                  });
           
           window.localStorage.setItem("channels_list", JSON.stringify(channelListData));
           
           },
           error: function (jqXHR, textStatus, errorThrown) {
           // debugger;
           //alert('Network error has occurred while fetching login details,please try again!');//1-
           }
           });
}

function ScrollBottom() {
    setTimeout(function () {
               $("#chat").animate({ scrollTop: 1000000 }, 800);
               
               }, 1000);
    
    console.log(" ScrollBottom >>>> ");
    
    return false;
}

function tConvert(time) {
    // Check correct time format and split into components
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    
    if (time.length > 1) { // If time format correct
        time = time.slice(1);  // Remove full string match value
        time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
        time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join(''); // return adjusted time or original string
}

function PostMessage(roomId, text, channel) {
    var dataObj = {
    roomId: roomId,
        //channel: channel,
    text: text
    }
    
    $.ajax({
           type: 'POST',
           url: chatUrl + 'chat.postMessage',
           data: dataObj,
           headers: {
           "Content-Type": "application/x-www-form-urlencoded",
           "X-Auth-Token": window.localStorage.getItem("chat_authToken"),
           "X-User-Id": window.localStorage.getItem("chat_userId")
           },
           
           beforeSend: function () {
           $.mobile.loading('show');
           },
           complete: function () {
           $.mobile.loading('hide');
           },
           success: function (result) {
           
           setTimeout(function () {
                      AfterPostGetMessage();
                      
                      }, 2000);
           
           
           },
           error: function (jqXHR, textStatus, errorThrown) {
           //alert('Network error has occurred while fetching login details,please try again!');//1-
           }
           });
}

function AfterPostGetMessage() {
    var bMessageExist = false;
    
    var d = new Date();
    var ddNew = new Date(d.getTime() + 30000);
    var ISODate = ddNew.toISOString();
    
    var oldestISOdate = "";
    if (window.localStorage.getItem("chatSyncDate") == undefined || window.localStorage.getItem("chatSyncDate") == null) {
        oldestISOdate = d.toISOString();
    } else {
        oldestISOdate = window.localStorage.getItem("chatSyncDate");
    }
    
    console.log('im.history?roomId=' + window.localStorage.getItem("selectedChannelId") + '&unreads=true&count=1000&inclusive=true&oldest=' + oldestISOdate + '&latest=' + ISODate);
    
    $.ajax({
           type: 'GET',
           url: chatUrl + 'im.history?roomId=' + window.localStorage.getItem("selectedChannelId") + '&count=1000&unreads=true&count=1000&inclusive=true&oldest=' + oldestISOdate + '&latest=' + ISODate,
           data: "",
           headers: {
           "Content-Type": "application/x-www-form-urlencoded",
           "X-Auth-Token": window.localStorage.getItem("chat_authToken"),
           "X-User-Id": window.localStorage.getItem("chat_userId")
           },
           
           beforeSend: function () {
           
           },
           complete: function () {
           
           },
           success: function (result) {
           
           var dd = new Date(ISODate);
           var ddNew = new Date(dd - 60000);
           window.localStorage.setItem("chatSyncDate", ddNew.toISOString());
           
           if (result.messages) {
           
           console.log("MessageCount >>>> " + result.messages.length);
           
           result.messages.sort(function (a, b) {
                                return new Date(a.ts) - new Date(b.ts);
                                });
           
           $.each(result.messages, function (i, item) {
                  
                  bMessageExist = false;
                  $.map(ChatList, function (elementOfArray, indexInArray) {
                        
                        if (elementOfArray.msgId == item._id) {
                        bMessageExist = true;
                        }
                        });
                  
                  
                  if (bMessageExist == false) {
                  
                  var attachmentTitle = '';
                  var attachmentDescription = '';
                  var attachmentTitle_link = '';
                  var fileType = '';
                  
                  if (item.attachments != null && item.attachments != undefined && item.attachments.length > 0) {
                  attachmentTitle = item.attachments[0].title;
                  attachmentDescription = item.attachments[0].description;
                  attachmentTitle_link = item.attachments[0].title_link;
                  fileType = item.file.type;
                  }
                  
                  ChatList.push({
                                'msgId': item._id,
                                'channelId': item.rid,
                                'msg': item.msg,
                                'msgDate': item.ts,
                                'userId': item.u._id,
                                'username': item.u.username,
                                'name': item.u.name,
                                'attachmentTitle': attachmentTitle,
                                'attachmentDescription': attachmentDescription,
                                'attachmentTitle_link': attachmentTitle_link,
                                'fileType': fileType
                                });
                  
                  var today = new Date(item.ts);
                  var cDate = today.getDate();
                  var cMonth = today.getMonth();
                  var cYear = today.getFullYear();
                  
                  var cHour = today.getHours() < 10 ? "0" + today.getHours() : today.getHours();
                  var cMin = today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes();
                  var cSec = today.getSeconds() < 10 ? "0" + today.getSeconds() : today.getSeconds();
                  
                  var convertedDate = monthNames[cMonth] + " " + cDate + ", " + cYear + " " + tConvert(cHour + ":" + cMin + ":" + cSec);
                  
                  
                  var urlArr = item.msg.match(/(?:https?|ftp|http):\/\/\S+/gi);
                  var msg = item.msg.replace(/(?:https?|ftp):\/\/[\n\S]+/g, '').replace(/(?:http?|ftp):\/\/[\n\S]+/g, '').replace(new RegExp('\n', 'g'), '<br>');
                  
                  if (urlArr != null) {
                  for (var i = 0; i < urlArr.length; i++) {
                  msg = msg + urlArr[i].replace(urlArr[i], '<a onclick="openChatURL(\'' + urlArr[i] + '\')">' + urlArr[i] + '</a><br>')
                  }
                  }
                  
                  
                  if (window.localStorage.getItem("rocketchatusername") == item.u.username) {
                  if (item.msg != null && item.msg != "" && (item.attachments == null || item.attachments == '' || item.attachments == undefined)) {
                  $('#messages-box').append('<div class="you"> <div class="user"><div class="userimg"><div class="chatusername">' + item.u.username + '</div></div></div> <div class="messagechatbox">' + msg + '</div> <div class="date">' + convertedDate + '</div> </div>');
                  } else if (item.file != null) {
                  if (item.file.type == 'image/jpeg' || item.file.type == 'image/gif' || item.file.type == 'application/pdf' || item.file.type == 'image/ png') {
                  $('#messages-box').append('<div class="you"> <div class="user"><div class="userimg"><div class="chatusername">' + item.u.username + '</div></div></div> <div class="messagechatbox"> <span class="attachicon"> </span> ' + item.attachments[0].title + '<div> ' + msg + '<div class="inline-image" style="background-image: url("' + chatImageUrl + item.attachments[0].title_link + '");"> <img src= "' + chatImageUrl + item.attachments[0].title_link + '" height= "200" class="gallery-item" onclick="imageViewPopup(\'' + chatImageUrl + item.attachments[0].title_link + '\')" ></div > </div> <div class="date">' + convertedDate + '</div>  </div>');
                  }
                  }
                  else {
                  $('#messages-box').append('<div class="you"> <div class="user"><div class="userimg"><div class="chatusername">' + item.u.username + '</div></div></div> <div class="messagechatbox"> <span class="attachicon"> </span> ' + item.attachments[0].title + '<div class="date">' + convertedDate + '</div>  </div>');
                  }
                  } else {
                  if (item.msg != null && item.msg != "" && (item.attachments == null || item.attachments == '' || item.attachments == undefined)) {
                  $('#messages-box').append('<div class="other"><div class="user"><div class="userimg"><div class="chatusername">' + item.u.username + '</div></div></div>  <div class="messagechatbox">' + msg + '</div> <div class="date">' + convertedDate + '</div></div>');
                  } else if (item.file != null) {
                  if (item.file.type == 'image/jpeg' || item.file.type == 'image/gif' || item.file.type == 'application/pdf' || item.file.type == 'image/ png') {
                  $('#messages-box').append('<div class="other"> <div class="user"><div class="userimg"><div class="chatusername">' + item.u.username + '</div></div></div>  <div class="messagechatbox"> <span class="attachicon"> </span> ' + item.attachments[0].title + '<div> ' + msg + ' </div><div class="inline-image" style="background-image: url("' + chatImageUrl + item.attachments[0].title_link + '");"> <img src="' + chatImageUrl + item.attachments[0].title_link + '" height= "200" class="gallery-item" onclick="imageViewPopup(\'' + chatImageUrl + item.attachments[0].title_link + '\')" ></div ></div> <div class="date">' + convertedDate + '</div> </div>');
                  }
                  } else {
                  $('#messages-box').append('<div class="other"> <div class="user"><div class="userimg"><div class="chatusername">' + item.u.username + '</div></div></div> <div class="messagechatbox"> <span class="attachicon"> </span> ' + item.attachments[0].title + '</div> <div class="date">' + convertedDate + '</div>  </div>');
                  
                  }
                  }
                  
                  }
                  
                  });
           
           ChatList.sort(function (a, b) {
                         // Turn your strings into dates, and then subtract them
                         // to get a value that is either negative, positive, or zero.
                         return new Date(a.msgDate) - new Date(b.msgDate);
                         });
           
           
           setTimeout(function () {
                      var wtf = $('#chat');
                      var height = wtf[0].scrollHeight;
                      wtf.scrollTop(height);
                      }, 1000);
           
           ScrollBottom();
           
           } else {
           alert("Unable to get channel list.");
           return false;
           }
           
           },
           error: function (jqXHR, textStatus, errorThrown) {
           // debugger;
           //alert('Network error has occurred while fetching login details,please try again!');//1-
           }
           });
}

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

function SendMessage() {
}

$('.message-icon').click(function () {
                         $('.chat').fadeToggle(500);
                         });

function openAttachment() {
    document.getElementById('attachment').click();
}

function fileSelected(input) {
    //document.getElementById('btnAttachment').value = "File: " + input.files[0].name
    $('#fileAttachPopupId').popup('open');
    var file = this.files[0];
    $('#fileName').append(file.name);
    
    $('#uploadImg')[0].src = (window.URL ? URL : webkitURL).createObjectURL(input.files[0]);
    
}

var dataURLToBlob = function (dataURL) {
    var BASE64_MARKER = ';base64,';
    if (dataURL.indexOf(BASE64_MARKER) == -1) {
        var parts = dataURL.split(',');
        var contentType = parts[0].split(':')[1];
        var raw = parts[1];
        
        return new Blob([raw], { type: contentType });
    }
    
    var parts = dataURL.split(BASE64_MARKER);
    var contentType = parts[0].split(':')[1];
    var raw = window.atob(parts[1]);
    var rawLength = raw.length;
    
    var uInt8Array = new Uint8Array(rawLength);
    
    for (var i = 0; i < rawLength; ++i) {
        uInt8Array[i] = raw.charCodeAt(i);
    }
    
    return new Blob([uInt8Array], { type: contentType });
}


$(document).on("imageResized", function (event) {
               var fileData = new FormData($("form[id*='uploadImageForm']")[0]);
               if (event.blob && event.url) {
               fileData.append('file', event.blob);
               
               // Adding one more key to FormData object
               var fileDescription = $('#fileDiscriptionid').val();
               fileData.append('msg', fileDescription);
               
               $.ajax({
                      
                      type: 'POST',
                      url: chatUrl + 'rooms.upload/' + window.localStorage.getItem("selectedChannelId"),
                      headers: {
                      "X-Auth-Token": window.localStorage.getItem("chat_authToken"),
                      "X-User-Id": window.localStorage.getItem("chat_userId")
                      },
                      contentType: false, // Not to set any content header
                      processData: false, // Not to process data
                      data: fileData,
                      
                      beforeSend: function () {
                      $.mobile.loading('show');
                      },
                      complete: function () {
                      $.mobile.loading('hide');
                      },
                      
                      success: function (result) {
                      setTimeout(function () {
                                 $('#fileAttachPopupId').popup('close');
                                 $('#fileDiscriptionid').val("");
                                 AfterPostGetMessage();
                                 
                                 }, 2000);
                      },
                      error: function (err) {
                      // alert(2);
                      alert(JSON.stringify(err));
                      }
                      });
               }
               });

function SendSelectedFile() {
    
    if (window.FormData !== undefined) {
        
        var file = imgURI;// $("#attachment")[0].files[0];
        
        if (file.type.match(/image.*/)) {
            
            console.log('An image has been loaded');
            // Load the image
            var reader = new FileReader();
            reader.onload = function (readerEvent) {
                var tempImg = new Image();
                tempImg.onload = function (imageEvent) {
                    
                    
                    var targetWidth = tempImg.width;
                    var targetHeight = tempImg.height;
                    var aspect = tempImg.width / tempImg.height;
                    var longSideMax = 544;
                    
                    // Calculate shorter side length, keeping aspect ratio on image.
                    // If source image size is less than given longSideMax, then it need to be
                    // considered instead.
                    if (tempImg.width > tempImg.height) {
                        longSideMax = Math.min(tempImg.width, longSideMax);
                        targetWidth = longSideMax;
                        targetHeight = longSideMax / aspect;
                    }
                    else {
                        longSideMax = Math.min(tempImg.height, longSideMax);
                        targetHeight = longSideMax;
                        targetWidth = longSideMax * aspect;
                    }
                    
                    // Create canvas of required size.
                    var canvas = document.createElement('canvas');
                    canvas.width = targetWidth;
                    canvas.height = targetHeight;
                    
                    var ctx = canvas.getContext("2d");
                    // Take image from top left corner to bottom right corner and draw the image
                    // on canvas to completely fill into.
                    ctx.drawImage(this, 0, 0, tempImg.width, tempImg.height, 0, 0, targetWidth, targetHeight);
                    
                    var dataUrl = canvas.toDataURL('image/jpeg');
                    var resizedImage = dataURLToBlob(dataUrl);
                    $.event.trigger({
                                    type: "imageResized",
                                    blob: resizedImage,
                                    url: dataUrl
                                    });
                }
                tempImg.src = readerEvent.target.result;
            }
            reader.readAsDataURL(file);
        }
        
    } else {
        alert("FormData is not supported.");
    }
}

function SendSelectedFileNew() {
    
    if (window.FormData !== undefined) {
        
        //var fileInput = $('#uploadImg');
        //var file = fileInput.files[0];
        var fileData = new FormData();
        fileData.append('file', fileURL);
        var fileDescription = $('#fileDiscriptionid').val();
        fileData.append('msg', fileDescription);
        
        $.ajax({
               
               type: 'POST',
               url: chatUrl + 'rooms.upload/' + window.localStorage.getItem("selectedChannelId"),
               headers: {
               "X-Auth-Token": window.localStorage.getItem("chat_authToken"),
               "X-User-Id": window.localStorage.getItem("chat_userId")
               },
               contentType: false, // Not to set any content header
               processData: false, // Not to process data
               data: fileData,
               
               beforeSend: function () {
               $.mobile.loading('show');
               },
               complete: function () {
               $.mobile.loading('hide');
               },
               
               success: function (result) {
               setTimeout(function () {
                          $('#fileAttachPopupId').popup('close');
                          $('#fileDiscriptionid').val("");
                          AfterPostGetMessage();
                          
                          }, 2000);
               },
               error: function (err) {
               // alert(2);
               alert(JSON.stringify(err));
               }
               });
    } else {
        alert("FormData is not supported.");
    }
}


function closeFilePopup() {
    $('#fileAttachPopupId').popup('close');
}


function chatAttachPhoto() {
    // Retrieve image file location from specified source
    navigator.camera.getPicture(onChatURISuccess, onFail, {
                                quality: 100,
                                encodingType: navigator.camera.EncodingType.JPEG,
                                targetWidth: 500,
                                targetHeight: 500,
                                correctOrientation: true,
                                destinationType: destinationType.FILE_URI,
                                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                                popoverOptions: { // iOS only
                                x: 300,
                                y: 300,
                                width: 100,
                                height: 100,
                                arrowDir: navigator.camera.PopoverArrowDirection.ARROW_ANY
                                }
                                });
}


function onChatURISuccess(imageURI) {
    
    var thisResult = JSON.parse(imageURI);
    
    $.mobile.loading("show", {
                     text: "Image uploading please wait..",
                     textVisible: true,
                     theme: "b",
                     html: ""
                     });
    
    var url = encodeURI(chatUrl + 'rooms.upload/' + window.localStorage.getItem("selectedChannelId"));
    
    //            var params = new Object();
    //            params.your_param_name = "something";  //you can send additional info with the file
    
    var options = new FileUploadOptions();
    options.fileKey = "file"; //depends on the api
    options.fileName = thisResult.filename.substr(thisResult.filename.lastIndexOf('/') + 1);
    options.mimeType = "image/jpeg";
    //            options.params = params;
    options.chunkedMode = true; //this is important to send both data and files
    
    var headers = {
        "X-Auth-Token": window.localStorage.getItem("chat_authToken"),
        "X-User-Id": window.localStorage.getItem("chat_userId")
    };
    options.headers = headers;
    
    var ft = new FileTransfer();
    ft.upload(thisResult.filename, url, winChatUpload, failChatUpload, options);
}

var winChatUpload = function (r) {
    
    setTimeout(function () {
               AfterPostGetMessage();
               
               $.mobile.loading('hide');
               }, 1000);
    
}

var failChatUpload = function (error) {
    $.mobile.loading('hide');
    alert("Error in chat service" + JSON.stringify(error));
}


function createBlob(data, type) {
    var r;
    try {
        r = new window.Blob([data], { type: type });
    }
    catch (e) {
        // TypeError old chrome and FF
        window.BlobBuilder = $window.BlobBuilder ||
        window.WebKitBlobBuilder ||
        window.MozBlobBuilder ||
        window.MSBlobBuilder;
        // consider to use crosswalk for android
        
        if (e.name === 'TypeError' && window.BlobBuilder) {
            var bb = new BlobBuilder();
            bb.append([data.buffer]);
            r = bb.getBlob(type);
        }
        else if (e.name == "InvalidStateError") {
            // InvalidStateError (tested on FF13 WinXP)
            r = new $window.Blob([data.buffer], { type: type });
        }
        else {
            throw e;
        }
    }
    
    return r;
}





function imageViewPopup(url) {
    $('#imageViewImg')[0].src = "";
    //document.getElementById('btnAttachment').value = "File: " + input.files[0].name
    $('#imageViewhPopupId').popup('open');
    $('#imageViewImg')[0].src = url;
}

function closeImageViewPopup() {
    $('#imageViewImg')[0].src = "";
    $('#imageViewhPopupId').popup('close');
}


$('form').submit(function (e) {
                 e.preventDefault();
                 var $messagesBox = $(".messages-box"),
                 messagesBoxHeight = $messagesBox[0].scrollHeight,
                 message = $('textarea', this).val().trim(),
                 messageLength = message.length;
                 
                 
                 if (messageLength > 0) {
                 $('textarea', this).removeClass('error');
                 PostMessage(window.localStorage.getItem("selectedChannelId"), message, window.localStorage.getItem("channelname"));
                 //$messagesBox.append('<div class="message"><i class="fa fa-close"></i> <p>' + message + '</p></div>');
                 } else {
                 $('textarea', this).addClass('error');
                 }
                 
                 $('textarea', this).val('');
                 //$('textarea', this).focus();
                 
                 // scroll to see last message
                 $messagesBox.scrollTop(messagesBoxHeight);
                 
                 });  // form

function myFunction() {
    //alert(1);
    console.log(" Text out >>>> ");
    $('body').removeClass('headerposition');
}

$(document).on("focus", "textarea", function () {
               
               $('body').addClass('headerposition');
               
               console.log(" Text out >>>> ");
               //$(this).blur();
               });

// delete massage
$(document).on('click', '.fa-close', function () {
               $(this).parent().fadeOut(500, function () {
                                        $(this).remove();
                                        });
               });

// mouse enter add class
$(document).on('mouseenter', '.fa-close', function () {
               $(this).parent().addClass('active');
               });

// mouse leave remove class
$(document).on('mouseleave', '.fa-close', function () {
               $(this).parent().removeClass('active');
               });





//=========================================end login=============================================//


//=========================================Home page ============================================//

function moveUp(id, sign_type, signid, target_sign_type, target_signid) {
    var $current = $('#' + id).closest('li')
    var $previous = $current.prev('li');
    if ($previous.length !== 0) {
        var userid = window.localStorage.getItem("userid");
        
        //alert(JSON.stringify({signid: signid, sign_type: sign_type, userid : userid, target_sign_type: target_sign_type, target_signid: target_signid }));
        
        $.ajax({
               type: 'post',
               url: remoteUrl + 'setreorderRun',
               data: {
               signid: signid,
               sign_type: sign_type,
               userid: userid,
               target_sign_type: target_sign_type,
               target_signid: target_signid
               },
               cache: false,
               beforeSend: function () {
               $.mobile.loading('show');
               },
               complete: function () {
               //$.mobile.loading('hide');
               },
               success: function (result) {
               //$current.insertBefore($previous);
               refreshRunsheet();
               },
               error: function (jqXHR, textStatus, errorThrown) {
               // alert(jqXHR+','+ textStatus+','+ errorThrown);1-
               //alert('Network error has occurred please check your network connection and try again!');
               }
               });
        
        
    }
    return false;
}



function moveDown(id, sign_type, signid, target_sign_type, target_signid) {
    var $current = $('#' + id).closest('li')
    var $next = $current.next('li');
    if ($next.length !== 0 && target_signid !== 0) {
        var userid = window.localStorage.getItem("userid");
        
        //alert(JSON.stringify({signid: signid, sign_type: sign_type, userid : userid, target_sign_type: target_sign_type, target_signid: target_signid }));
        
        $.ajax({
               type: 'post',
               url: remoteUrl + 'setreorderRun',
               data: {
               'signid': signid,
               'sign_type': sign_type,
               'userid': userid,
               'target_sign_type': target_sign_type,
               'target_signid': target_signid
               },
               cache: false,
               beforeSend: function () {
               $.mobile.loading('show');
               },
               complete: function () {
               //$.mobile.loading('hide');
               },
               success: function (result) {
               //$current.insertAfter($next);
               refreshRunsheet();
               },
               error: function (jqXHR, textStatus, errorThrown) {
               // alert(jqXHR+','+ textStatus+','+ errorThrown);2-
               //alert('Network error has occurred please check your network connection and try again!');
               }
               });
        
        
    }
    return false;
}

function getWarningMsgScript(item) {
    var func = "";
    if (item.installation_date != null) {
        var instDate = item.installation_date.split("-");
        
        if (instDate.length == 3) {
            var installationDate = new Date(instDate[2], instDate[1] - 1, instDate[0]);
            if (installationDate > new Date()) {
                var day = installationDate.getDate();
                var monthIndex = installationDate.getMonth();
                var year = installationDate.getFullYear();
                if (item.signrelocate == 1 || item.signrelocate == "1" || item.signrelocate == 2 || item.signrelocate == "2") {
                    func = 'warnFuturerelocatableJobView(\'' + day + ' ' + monthNames[monthIndex] + ' ' + year + '\',' + item.sign_type + ',' + item.signid + ',' + item.signrelocate + ', \'' + item.relocateaddress + '\')';
                } else {
                    func = 'warnFutureJobView(\'' + day + ' ' + monthNames[monthIndex] + ' ' + year + '\',' + item.sign_type + ',' + item.signid + ')';
                }
            }/* else if (item.signrelocate == 1 || item.signrelocate == "1" || item.signrelocate == 2 || item.signrelocate == "2") {
              func = 'relocatableJobView(' + item.signrelocate + ', \'' + item.relocateaddress + '\' ,' + item.sign_type + ',' + item.signid + ')';
              }*/ else {
                  func = 'jobview(' + item.sign_type + ',' + item.signid + ')';
              }
        }/* else if (item.signrelocate == 1 || item.signrelocate == "1" || item.signrelocate == 2 || item.signrelocate == "2") {
          func = 'relocatableJobView(' + item.signrelocate + ', \'' + item.relocateaddress + '\' ,' + item.sign_type + ',' + item.signid + ')';
          }*/
        else {
            func = 'jobview(' + item.sign_type + ',' + item.signid + ')';
        }
    } /*else if (item.signrelocate == 1 || item.signrelocate == "1" || item.signrelocate == 2 || item.signrelocate == "2") {
       func = 'relocatableJobView(' + item.signrelocate + ', \'' + item.relocateaddress + '\' ,' + item.sign_type + ',' + item.signid + ')';
       }*/
    else {
        func = 'jobview(' + item.sign_type + ',' + item.signid + ')';
    }
    
    
    return func;
};

function warnFutureJobView(installation_date, sign_type, signid) {
    var popMessage = "WARNING: You are installing a sign BEFORE the installation date of: " + installation_date + ", please acknowledge you are doing this as unless instructed by the agent to install the sign early, the sign should not be installed until " + installation_date;
    
    var pophtml = '<div style="padding:20px 10px 20px 20px ;"><lable>' + popMessage + '</lablel><p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="jobview(' + sign_type + ',' + signid + ')">Ok I understand the installation date is ' + installation_date + '</a></p></div>';
    var currentPageId = $(':mobile-pagecontainer').pagecontainer('getActivePage').attr('id');
    if (currentPageId == "home-page") {
        $('#jnc-popup1').html(pophtml);
        $('#jnc-popup1').popup('open');
    }
    else if (currentPageId == "runsheet-page") {
        $('#jnc-popup2').html(pophtml);
        $('#jnc-popup2').popup('open');
    }
    
}

function warnFuturerelocatableJobView(installation_date, sign_type, signid, signrelocate, relocateaddress) {
    var popMessage = "WARNING: You are installing a sign BEFORE the installation date of: " + installation_date + ", please acknowledge you are doing this as unless instructed by the agent to install the sign early, the sign should not be installed until " + installation_date;
    
    var pophtml = '<div style="padding:20px 10px 20px 20px ;"><lable>' + popMessage + '</lablel><p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="relocatableJobView(' + signrelocate + ', \'' + relocateaddress + '\' ,' + sign_type + ',' + signid + ')">Ok I understand the installation date is ' + installation_date + '</a></p></div>';
    var currentPageId = $(':mobile-pagecontainer').pagecontainer('getActivePage').attr('id');
    if (currentPageId == "home-page") {
        $('#jnc-popup1').html(pophtml);
        $('#jnc-popup1').popup('open');
    }
    else if (currentPageId == "runsheet-page") {
        $('#jnc-popup2').html(pophtml);
        $('#jnc-popup2').popup('open');
    }
    
}

function relocatableJobView(signrelocate, relocateaddress, sign_type, signid) {
    
    if (signrelocate == 1 || signrelocate == "1") {
        var popMessage = "Sign is to be MOVED to <strong>" + relocateaddress + "</strong>. please confirm the sign is at the property and mark it as removed. If the sign is NOT at the property, mark it as missing.";
    } else {
        var popMessage = "This sign is to be relocated from <strong>" + relocateaddress + "</strong>. please ensure you move the correct sign.";
    }
    
    var pophtml = '<div style="padding:20px 10px 20px 20px ;"><lable>' + popMessage + '</lablel><p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="jobview(' + sign_type + ',' + signid + ')">Ok</a></p></div>';
    var currentPageId = $(':mobile-pagecontainer').pagecontainer('getActivePage').attr('id');
    if (currentPageId == "home-page") {
        $('#jnc-popup1').html(pophtml);
        $('#jnc-popup1').popup('open');
    }
    else if (currentPageId == "runsheet-page") {
        $('#jnc-popup2').html(pophtml);
        $('#jnc-popup2').popup('open');
    }
    
}

function homepageAjax(id) {
    var userid = window.localStorage.getItem("userid");
    var username = window.localStorage.getItem("username");
    var userCurrentStatus = '';
    
    // alert('home:'+id);
    var isThroughRefresh = false;
    
    if (id == 7) {
        isThroughRefresh = true;
        id = 1;
    }
    
    $('#run-list').empty();
    $('#run-list-all').empty();
    $('#completed-run-list').empty();
    $('#run-list1').empty();
    $('#run-list1-all').empty();
    $('#completed-run-list1').empty();
    
    
    //$('#timeclock'+id).html('');  //gee  //  alert(userid);
    var interval = setInterval(function () {
                               $.mobile.loading('show');
                               clearInterval(interval);
                               }, 1);
    
    // alert(username);
    
    $.ajax({
           type: 'post',
           url: remoteUrl + 'getRunsheet',
           data: {
           'userid': userid,
           'username': username
           },
           cache: false,
           timeout: 60000,
           beforeSend: function () {
           // $.mobile.loading('show');
           $.mobile.loading('show');
           /* var interval = setInterval(function(){
            $.mobile.loading('show');
            clearInterval(interval);
            },1);*/
           
           },
           complete: function () {
           $.mobile.loading('hide');
           
           },
           success: function (result) {
           //alert(result);
           
           if (result == undefined || result == null) {
           alert('Failed to get job from servcie');
           }
           
           result = JSON.parse(result);
           
           checkConnectionCalls = 0;
           
           // $.mobile.loading('hide');
           var interval = setInterval(function () {
                                      $.mobile.loading('hide');
                                      clearInterval(interval);
                                      }, 1);
           var completed = result.completed;
           var notcompleted = result.notcompleted;
           var alljobs = result.alljobs;
           var appdetails = result.appdetails;
           
           // alert(result.timeclock.current_status);
           userCurrentStatus = result.timeclock.current_status;
           var runsheetStartRun = result.RunsheetStartRun;
           
           if (result.timeclock.useAPPTimeclock == 1) {
           $(".timeClockBtn").show();
           } else {
           $(".timeClockBtn").hide();
           //  alert('hide');
           }
           
           $('.version').html(version);
           if (window.localStorage.getItem("unreadMsgList") == undefined || window.localStorage.getItem("unreadMsgList") == null) {
           $('.username').html('Logged in as: ' + window.localStorage.getItem("username"));
           $('#nameId').removeClass('usernameicon');
           } else {
           $('.username').html('Rocket Chat Logged in as: ' + window.localStorage.getItem("username") + " - " + window.localStorage.getItem("unreadMsgList"));
           $('#nameId').addClass('usernameicon');
           }
           
           if ($(":mobile-pagecontainer").pagecontainer("getActivePage")[0].id == "runsheet-page")
           $('a[href="#one1"]').click();
           else
           $('a[href="#one"]').click();
           
           //  alert( userCurrentStatus);
           //alert( result.timeclock.useAPPTimeclock);
           if ((userCurrentStatus == 'Out' || userCurrentStatus == 'LunchStart') && (result.timeclock.useAPPTimeclock == 1)) {
           // $('#jnc-popup1').popup('close');
           ////$('#jnc-popup1').html('');
           //// $('#timeclock1').popup('close');
           // $('#timeclock1').html('');
           
           if (isThroughRefresh) {
           $('#run-list-all').empty();
           $('#run-list-all').append('<div style=" padding:50px 10px 50px 20px; text-align:center; vertical-align:center;">You are not Clocked in or at lunch, you cannot view the runsheet until you clock in / return from lunch</div>');
           
           var isDismissClick = window.localStorage.getItem("isDismissClick");
           // alert(isDismissClick);
           
           if (isDismissClick == 'true') {
           window.localStorage.setItem("isDismissClick", false);
           return;
           }
           
           }
           // alert(isThroughRefresh);
           window.localStorage.setItem("isnotcheckedin", isThroughRefresh);
           
           //else
           //{
           
           // alert(id);
           var popMessage = "Your current status in the timeclock is <b>" + userCurrentStatus + "</b>.";
           
           var pophtml = '<div style="padding:20px 10px 20px 20px ;"><lable>' + popMessage + '</lablel><br/><label>You must be clocked in or have Ended your lunch to view jobs</label><p><a id="btn-cancel" onclick="showTimerClockPopup(' + id + ')" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">OK</a></p></div>';
           
           $('#jnc-popup' + id).html(pophtml);
           $('#jnc-popup' + id).popup('open');
           //alert(pophtml);
           //}
           
           }
           else {
           
           
           if (username && userid) {
           
           var li = '';
           var cli = '';
           var ali = '';
           var tli = '';
           
           $('#run-list').html('');
           $('#run-list-all').html('');
           $('#completed-run-list').html('');
           
           $('#run-list1').html('');
           $('#run-list1-all').html('');
           $('#completed-run-list1').html('');
           
           $.each(notcompleted, function (i, item) {
                  var trclass = '';
                  var ordertype = '';
                  var aii = '';
                  var sl = i + 1;
                  var nextsignid = 0,
                  nextsigntype = 0,
                  prevsignid = 0,
                  prevsigntype = 0;
                  if (i > 0 && i < notcompleted.length) {
                  prevsignid = notcompleted[i - 1].signid;
                  prevsigntype = notcompleted[i - 1].sign_type;
                  }
                  
                  if (i < notcompleted.length - 1) {
                  nextsignid = notcompleted[i + 1].signid;
                  nextsigntype = notcompleted[i + 1].sign_type;
                  }
                  // alert(item );//gee
                  
                  if (i % 2 != 0) {
                  trclass = ' class= "li-even"';
                  } else {
                  trclass = '';
                  }
                  if (item.ordertype == 'Pending Delivery') {
                  
                  if (item.sign_type == 99)
                  ordertype = 'Product Order';
                  else
                  ordertype = 'New Install';
                  
                  aii = 'Artwork Installation Instruction : ' + item.specialinstructions;
                  } else if (item.oredertype == 'Order Overlay Request') {
                  aii = 'Artwork Installation Instruction : ' + item.specialinstructions;
                  } else {
                  ordertype = item.ordertype;
                  }
                  
                  var address = item.streetaddress + ' ' + item.streetaddress2 + ' ' + item.streetsuburb + ' ' + item.streetpostcode;
                  var secondAgent = (item.name1 != undefined && item.surname1 != undefined) ? ", " + item.name1 + " " + item.surname1 : "";
                  console.log('1 -' + item.agencyname);
                  
                  if (item.signrelocate == 1 || item.signrelocate == "1" || item.signrelocate == 2 || item.signrelocate == "2") {
                  li = li + '<li data-icon="false" ' + trclass + '><a onclick="' + getWarningMsgScript(item) + '" class="btn" data-transition="slide" ><h2>' + ordertype + '-' + ((item.sign_type == 99) ? item.orderid : item.signid) + '</h2><p>Pickup Address: ' + item.pickupaddress + '</p><p>Relcoate this sign to : ' + item.delievryaddress + '</p><p>Agency : ' + item.agencyname + '</p><p>Heading : ' + item.headertitle + '</p><p>' + aii + '</p></a><div class="split-custom-wrapper"><a class="btn ui-icon-carat-u split-custom-button ui-btn-icon-notext" id="move-up-' + i + '" onclick="moveUp(this.id,' + item.sign_type + ',' + item.signid + ',' + prevsigntype + ',' + prevsignid + ')" ></a><a class="btn split-custom-button ui-icon-carat-d ui-btn-icon-notext" id="move-up-' + i + '" onclick="moveDown(this.id,' + item.sign_type + ',' + item.signid + ',' + nextsigntype + ',' + nextsignid + ')"></a></div></li>';
                  
                  } else {
                  li = li + '<li data-icon="false" ' + trclass + '><a onclick="' + getWarningMsgScript(item) + '" class="btn" data-transition="slide" ><h2>' + ordertype + '-' + ((item.sign_type == 99) ? item.orderid : item.signid) + '</h2><p>Address : ' + address + '</p><p>Agent : ' + item.name + ' ' + item.surname + secondAgent + '</p><p>Agency : ' + item.agencyname + '</p><p>Heading : ' + item.headertitle + '</p><p>' + aii + '</p></a><div class="split-custom-wrapper"><a class="btn ui-icon-carat-u split-custom-button ui-btn-icon-notext" id="move-up-' + i + '" onclick="moveUp(this.id,' + item.sign_type + ',' + item.signid + ',' + prevsigntype + ',' + prevsignid + ')" ></a><a class="btn split-custom-button ui-icon-carat-d ui-btn-icon-notext" id="move-up-' + i + '" onclick="moveDown(this.id,' + item.sign_type + ',' + item.signid + ',' + nextsigntype + ',' + nextsignid + ')"></a></div></li>';
                  }
                  
                  
                  /* li = li + '<li data-icon="false" ' + trclass + '><a onclick="'+ getWarningMsgScript(item) +'" class="btn" data-transition="slide" ><h2>' + ordertype + '-' + item.signid + '</h2><p>Address : ' + address + '</p><p>Agent : ' + item.name + ' ' + item.surname + secondAgent + '</p><p>Agency : ' + item.agencyname + '</p><p>Heading : ' + item.headertitle + '</p><p>' + aii + '</p></a><div class="split-custom-wrapper"><a class="btn ui-icon-carat-u split-custom-button ui-btn-icon-notext" id="move-up-' + i + '" onclick="moveUp(this.id,' + item.sign_type + ',' + item.signid + ',' + prevsigntype + ',' + prevsignid + ')" ></a><a class="btn split-custom-button ui-icon-carat-d ui-btn-icon-notext" id="move-up-' + i + '" onclick="moveDown(this.id,' + item.sign_type + ',' + item.signid + ',' + nextsigntype + ',' + nextsignid + ')"></a></div></li>'; */
                  
                  
                  });
           
           
           
           $.each(alljobs, function (i, item) {
                  //
                  var trclass = '';
                  var ordertype = '';
                  var aii = '';
                  var style = '';
                  var sl = i + 1;
                  var nextsignid = 0,
                  nextsigntype = 0,
                  prevsignid = 0,
                  prevsigntype = 0;
                  
                  if (i > 0 && i < alljobs.length) {
                  prevsignid = alljobs[i - 1].signid;
                  prevsigntype = alljobs[i - 1].sign_type;
                  }
                  
                  if (i < alljobs.length - 1) {
                  nextsignid = alljobs[i + 1].signid;
                  nextsigntype = alljobs[i + 1].sign_type;
                  }
                  
                  //alert(item.completed);
                  
                  if (i % 2 != 0) {
                  trclass = ' class= "li-even"';
                  } else {
                  trclass = '';
                  }
                  if (item.ordertype == 'Pending Delivery') {
                  
                  if (item.sign_type == 99)
                  ordertype = '#' + sl + ' - Product Order';
                  else
                  ordertype = '#' + sl + ' - New Install';
                  
                  // ordertype = '#' + sl + ' - New Install';
                  aii = 'Artwork Installation Instruction : ' + item.specialinstructions;
                  } else if (item.oredertype == 'Order Overlay Request') {
                  aii = 'Artwork Installation Instruction : ' + item.specialinstructions;
                  } else {
                  ordertype = '#' + sl + ' - ' + item.ordertype;
                  }
                  
                  // alert(item);
                  if (item.completed == 'yes') {
                  style = 'style="background-color : #01df74 !important"';
                  }
                  else {
                  var returnedData = $.grep(notcompleted, function (element, index) {
                                            return element.signid == item.signid;
                                            });
                  if (returnedData.length > 0)
                  style = 'style="background-color : #df4b01 !important"';
                  }
                  
                  var address = item.streetaddress + ' ' + item.streetaddress2 + ' ' + item.streetsuburb + ' ' + item.streetpostcode;
                  var secondAgent = (item.name1 != undefined && item.surname1 != undefined) ? ", " + item.name1 + " " + item.surname1 : "";
                  
                  console.log('Address -' + address);
                  console.log('Sec Address -' + secondAgent);
                  console.log('2 -' + item.agencyname);
                  if (item.signrelocate == 1 || item.signrelocate == "1" || item.signrelocate == 2 || item.signrelocate == "2") {
                  ali = ali + '<li data-icon="false" ' + style + ' data-theme="b"><a ' + style + ' onclick="' + getWarningMsgScript(item) + '" class="btn" data-transition="slide" ><h2>' + ordertype + '-' + ((item.sign_type == 99) ? item.orderid : item.signid) + '</h2><p> Pickup Address: ' + item.pickupaddress + '</p><p>Relcoate this sign to :' + item.delievryaddress + '</p><p>Agency : ' + item.agencyname + '</p><p>Heading : ' + item.headertitle + '</p><p>' + aii + '</p></a><div class="split-custom-wrapper"><a class="btn ui-icon-carat-u split-custom-button ui-btn-icon-notext" id="move-up-' + i + '" onclick="moveUp(this.id,' + item.sign_type + ',' + item.signid + ',' + prevsigntype + ',' + prevsignid + ')" ></a><a class="btn split-custom-button ui-icon-carat-d ui-btn-icon-notext" id="move-up-' + i + '" onclick="moveDown(this.id,' + item.sign_type + ',' + item.signid + ',' + nextsigntype + ',' + nextsignid + ')"></a></div></li>';
                  } else {
                  ali = ali + '<li data-icon="false" ' + style + ' data-theme="b"><a ' + style + ' onclick="' + getWarningMsgScript(item) + '" class="btn" data-transition="slide" ><h2>' + ordertype + '-' + ((item.sign_type == 99) ? item.orderid : item.signid) + '</h2><p>Address : ' + address + '</p><p>Agent : ' + item.name + ' ' + item.surname + secondAgent + '</p><p>Agency : ' + item.agencyname + '</p><p>Heading : ' + item.headertitle + '</p><p>' + aii + '</p></a><div class="split-custom-wrapper"><a class="btn ui-icon-carat-u split-custom-button ui-btn-icon-notext" id="move-up-' + i + '" onclick="moveUp(this.id,' + item.sign_type + ',' + item.signid + ',' + prevsigntype + ',' + prevsignid + ')" ></a><a class="btn split-custom-button ui-icon-carat-d ui-btn-icon-notext" id="move-up-' + i + '" onclick="moveDown(this.id,' + item.sign_type + ',' + item.signid + ',' + nextsigntype + ',' + nextsignid + ')"></a></div></li>';
                  }
                  
                  // console.log('AL -'+ali);
                  });
           
           
           $.each(completed, function (i, comp) {
                  var trclass = '';
                  var ordertype = '';
                  var sl = i + 1;
                  var nextsignid = 0,
                  nextsigntype = 0,
                  prevsignid = 0,
                  prevsigntype = 0;
                  
                  //  console.log(completed.length);
                  
                  if (i > 0 && i < completed.length) {
                  prevsignid = completed[i - 1].signid;
                  prevsigntype = completed[i - 1].sign_type;
                  }
                  
                  
                  if (i < completed.length - 1) {
                  nextsignid = completed[i + 1].signid;
                  nextsigntype = completed[i + 1].sign_type;
                  }
                  //console.log(completed.length);
                  console.log(comp);
                  if (i % 2 != 0) {
                  trclass = ' class= "li-even"';
                  } else {
                  trclass = '';
                  }
                  /* if (comp.ordertype == 'Pending Delivery') {
                   ordertype = 'New Install';
                   } else {
                   ordertype = comp.ordertype;
                   }*/
                  
                  var completedID = '';
                  // alert(comp.sign_type);
                  
                  if (comp.sign_type == 99) {
                  completedID = comp.orderid;
                  // alert('OrderID:'+comp.orderid);
                  }
                  else {
                  completedID = comp.signid;
                  // alert('SignID:'+comp.signid);
                  }
                  
                  var caddress = comp.streetaddress + ' ' + comp.streetaddress2 + ' ' + comp.streetsuburb + ' ' + comp.streetpostcode;
                  var secondAgent = (comp.name1 != undefined && comp.surname1 != undefined) ? ", " + comp.name1 + " " + comp.surname1 : "";
                  
                  
                  
                  //  console.log('3 -'+comp.orderid+','+caddress+','+comp.name+ ' ' + comp.surname + secondAgent );
                  
                  if (comp.signrelocate == 1 || comp.signrelocate == "1" || comp.signrelocate == 2 || comp.signrelocate == "2") {
                  cli = cli + '<li data-icon="false" ' + trclass + '><a class="btn" data-transition="slide" ><h2>Job Completed -' + completedID + '</h2><p> Pickup Address: ' + comp.pickupaddress + '</p><p>Relcoate this sign to : ' + comp.delievryaddress + '</p><p>Agency : ' + comp.agencyname + '</p><p>Heading : ' + comp.headertitle + '</p></a><div class="split-custom-wrapper"><a class="btn ui-icon-carat-u split-custom-button ui-btn-icon-notext" id="move-up-' + i + '" onclick="moveUp(this.id,' + comp.sign_type + ',' + comp.signid + ',' + prevsigntype + ',' + prevsignid + ')" ></a><a class="btn split-custom-button ui-icon-carat-d ui-btn-icon-notext" id="move-up-' + i + '" onclick="moveDown(this.id,' + comp.sign_type + ',' + comp.signid + ',' + nextsigntype + ',' + nextsignid + ')"></a></div></li>';
                  } else {
                  cli = cli + '<li data-icon="false" ' + trclass + '><a class="btn" data-transition="slide" ><h2>Job Completed -' + completedID + '</h2><p>Address : ' + caddress + '</p><p>Agent : ' + comp.name + ' ' + comp.surname + secondAgent + '</p><p>Agency : ' + comp.agencyname + '</p><p>Heading : ' + comp.headertitle + '</p></a><div class="split-custom-wrapper"><a class="btn ui-icon-carat-u split-custom-button ui-btn-icon-notext" id="move-up-' + i + '" onclick="moveUp(this.id,' + comp.sign_type + ',' + comp.signid + ',' + prevsigntype + ',' + prevsignid + ')" ></a><a class="btn split-custom-button ui-icon-carat-d ui-btn-icon-notext" id="move-up-' + i + '" onclick="moveDown(this.id,' + comp.sign_type + ',' + comp.signid + ',' + nextsigntype + ',' + nextsignid + ')"></a></div></li>';
                  }
                  
                  
                  });
           
           //  console.log('LI:'+li);
           //   console.log('ALI:'+ali);
           // console.log('CLI:'+cli);
           $('#run-list').append(li);
           $('#run-list').listview().listview('refresh');
           
           $('#run-list-all').append(ali);
           $('#run-list-all').listview().listview('refresh');
           
           
           $('#completed-run-list').append(cli);
           $('#completed-run-list').listview().listview('refresh');
           
           $('#run-list1').append(li);
           $('#run-list1').listview().listview('refresh');
           
           $('#run-list1-all').append(ali);
           $('#run-list1-all').listview().listview('refresh');
           
           
           $('#completed-run-list1').append(cli);
           $('#completed-run-list1').listview().listview('refresh');
           
           
           console.log(userCurrentStatus);
           
           $.mobile.loading('hide');
           /*if(userCurrentStatus == 'Out')
            {
            //alert('Out Popup');
            var pophtml='<div style="padding:10px 20px;"><strong>Time Clock</strong><button type="submit" id="timeclock-in" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="UpdateClockStatus(1,'+id+','+runsheetStartRun+','+userCurrentStatus+')">Clock In</button><p><a id="btn-cancel"  data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">Dismiss</a></p></div>';
            
            $('#timeclock'+id).html(pophtml);
            $('#timeclock'+id).popup('open');
            }
            else if(userCurrentStatus == 'LunchStart')
            {
            // alert('Start Popup');
            var pophtml='<div style="padding:10px 20px;"><strong>Time Clock</strong><button type="submit" id="timeclock-endlunch" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="UpdateClockStatus(4,'+id+','+runsheetStartRun+','+userCurrentStatus+')" >End Lunch</button><p><a id="btn-cancel"  data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">Dismiss</a></p></div>';
            
            $('#timeclock'+id).html(pophtml);
            $('#timeclock'+id).popup('open');
            }*/
           }
           }
           
           var updatedDate = appdetails.LastUpdatedDate.substr(0, 10).split("-");
           var varDate = new Date(updatedDate[0], updatedDate[1] - 1, updatedDate[2]);
           
           var today = new Date();
           today.setHours(0, 0, 0, 0);
           
           if (parseInt(appdetails.AppCurrentVersion.replace(/\./g, '')) > parseInt(versionCheck.replace(/\./g, '')) && (varDate < today || varDate == 'Invalid Date')) {
           updateUserCurrentVersion(appdetails.AppCurrentVersion);
           
           var pophtml = '<div style="padding:20px 10px 20px 20px ;"> <div style="padding:20px 0px 20px 0px ;"> <strong>New APP Version available</strong> </div> <lable>There is a new version of the Digital Central Installer APP available, you should upgrade immediately.</lablel><br/><p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="GetRunsheetLatestVersion()" >Update</a><a id="btn-cancel" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">Cancel</a></p></div>';
           
           $('#jnc-popup1').html(pophtml);
           $('#jnc-popup1').popup('open');
           }
           else if (varDate < today || varDate == 'Invalid Date') {
           updateUserCurrentVersion(appdetails.AppCurrentVersion);
           }
           
           checkisGpsLocationEnabled();
           
           //}
           // else {
           //alert("Unable To Authenticate. Check Your Credintials.");
           //}
           
           },
           error: function (jqXHR, textStatus, errorThrown) {
           
           var interval = setInterval(function () {
                                      $.mobile.loading('hide');
                                      clearInterval(interval);
                                      }, 1);
           checkInterntConnection();
           // alert('Network error has occurred while fetching runsheet details, please try again!');//2-
           }
           });
}

function checkisGpsLocationEnabled() {
    if (window.cordova) {
        cordova.plugins.diagnostic.isLocationAvailable(function (enabled) {
                                                       console.log("GPS location is " + (enabled ? "enabled" : "disabled"));
                                                       if (!enabled) {
                                                       var pophtml = '<div style="padding:20px 10px 20px 20px ;"> <div style="padding:20px 0px 20px 0px ;"> <strong>WARNING: Location Services must be enabled for this APP to work properly.</strong> </div> <lable>If Location services is not enabled, the APP will not work properly. Please ensure you have allowed access to Location Services for the "DCInstaller" APP. <br />  <br /> </lable> To check This, Go to : Settings -> Privacy -> Location services -> DCINSTALLER. Then choose "Always" <lable></lablel><br/><p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="switchToSettingPage()"  >Go to settings</a></p></div>';
                                                       
                                                       $('#jnc-popup1').html(pophtml);
                                                       $('#jnc-popup1').popup('open');
                                                       
                                                       
                                                       }
                                                       
                                                       }, function (error) {
                                                       console.error("The following error occurred: " + error);
                                                       });
    }
}

function switchToSettingPage() {
    
    $('#jnc-popup1').popup('close');
    
    cordova.plugins.diagnostic.switchToSettings(function () {
                                                console.log("Successfully switched to Settings app");
                                                }, function (error) {
                                                console.error("The following error occurred: " + error);
                                                });
}

function updateUserCurrentVersion(AppCurrentVersion) {
    
    $.ajax({
           type: 'post',
           url: remoteUrl + 'setUserAppdetails',
           data: {
           "userid": window.localStorage.getItem("userid"),
           "iosversion": device.version,
           "AppCurrentVersion": versionCheck,
           "LastUpdatedDate": new Date().toISOString()
           },
           cache: false,
           beforeSend: function () {
           $.mobile.loading('show');
           },
           complete: function () {
           $.mobile.loading('hide');
           },
           success: function (result) {
           
           },
           error: function (jqXHR, textStatus, errorThrown) {
           
           }
           });
}


$(document).on('pageinit', '#home-page', function (e) {
               e.preventDefault();
               
               //get userid from local storage
               var userid = window.localStorage.getItem("userid");
               var username = window.localStorage.getItem("username");
               
               
               if (userid && username) {
               $(':mobile-pagecontainer').pagecontainer('change', "#home-page");
               homepageAjax(1);
               $('.version').html(version);
               if (window.localStorage.getItem("unreadMsgList") == undefined || window.localStorage.getItem("unreadMsgList") == null) {
               $('.username').html('Logged in as: ' + window.localStorage.getItem("username"));
               $('#nameId').removeClass('usernameicon');
               } else {
               $('.username').html('Rocket Chat Logged in as: ' + window.localStorage.getItem("username") + " - " + window.localStorage.getItem("unreadMsgList"));
               $('#nameId').addClass('usernameicon');
               }
               
               if (myHomeTimer != null || myHomeTimer != "") {
               clearInterval(myHomeTimer);
               }
               myHomeTimer = setInterval(function () { getUnreadMessageCount() }, homeSyncDuration);
               
               } else {
               // $('#home-page-header').show();
               $('.version').html(version);
               $(':mobile-pagecontainer').pagecontainer('change', "#login-page");
               //$.mobile.changePage('#login-page');
               }
               
               });


function viewRunsheet() {
    
    // deletePopClose();
    // alert($('#jnc-popup'));
    // $('#jnc-popup').popup('close');
    //alert('RS');
    
    //get userid from local storage
    var userid = window.localStorage.getItem("userid");
    var username = window.localStorage.getItem("username");
    
    if (userid && username) {
        
        $(':mobile-pagecontainer').pagecontainer('change', "#home-page");
        homepageAjax(2);
        if (window.localStorage.getItem("unreadMsgList") == undefined || window.localStorage.getItem("unreadMsgList") == null) {
            $('.username').html('Logged in as: ' + window.localStorage.getItem("username"));
            $('#nameId').removeClass('usernameicon');
        } else {
            $('.username').html('Rocket Chat Logged in as: ' + window.localStorage.getItem("username") + " - " + window.localStorage.getItem("unreadMsgList"));
            $('#nameId').addClass('usernameicon');
        }
    } else {
        $('.version').html(version);
        $(':mobile-pagecontainer').pagecontainer('change', "#login-page");
    }
    
    
}


//=============================================== Job view page ================================================//
var showAlert = false;
var last_sign_type = "";
var last_signid = 0;

function getJobdetails(sign_type, signid) {
    
    if (window.cordova) {
        cordova.plugins.diagnostic.isLocationAvailable(function (enabled) {
                                                       console.log("GPS location is " + (enabled ? "enabled" : "disabled"));
                                                       if (!enabled) {
                                                       var pophtml = '<div style="padding:20px 10px 20px 20px ;"> <div style="padding:20px 0px 20px 0px ;"> <strong>WARNING: Location Services must be enabled for this APP to work properly.</strong> </div> <lable>If location service is not enabled, this app will not work properly. please ensure you have allowed access to location services for "DCINSTALLER" APP, before viewing the job details.</lable><br/> <br/> </lable> To check This, Go to : Settings -> Privacy -> Location services -> DCINSTALLER. Then choose "Always". <lable> <br/> <p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="switchToSettingPage()"  >Go to settings</a></p></div>';
                                                       
                                                       var currentPageId = $(':mobile-pagecontainer').pagecontainer('getActivePage').attr('id');
                                                       if (currentPageId == "home-page") {
                                                       $('#jnc-popup1').html(pophtml);
                                                       $('#jnc-popup1').popup('open');
                                                       } else if (currentPageId == "runsheet-page") {
                                                       $('#jnc-popup2').html(pophtml);
                                                       $('#jnc-popup2').popup('open');
                                                       }
                                                       
                                                       
                                                       } else {
                                                       allowJobdetails(sign_type, signid);
                                                       }
                                                       
                                                       }, function (error) {
                                                       console.error("The following error occurred: " + error);
                                                       });
    }
}

function allowJobdetails(sign_type, signid) {
    // alert(sign_type);
    var tomtomUrl = '';
    last_sign_type = sign_type;
    last_signid = signid;
    
    $.ajax({
           type: 'post',
           url: remoteUrl + 'getJobdetails',
           data: {
           sign_type: sign_type,
           signid: signid
           },
           cache: false,
           timeout: 60000,
           beforeSend: function () {
           $.mobile.loading('show');
           },
           complete: function () {
           $.mobile.loading('hide');
           },
           
           success: function (result) {
           result = JSON.parse(result);
           
           checkConnectionCalls = 0;
           
           // alert('success');
           var item = result.signdetails;
           var overlay = result.get_overlay;
           var ordertype = '';
           var logs = '<br/><strong>Changes log : </strong><hr>';
           var logss = '';
           var instructionText = '';
           var isFlagHolderAndSolar = '';
           var Comment = '';
           
           var showInstruction = false;
           
           // $('#jobdetails').html('');
           var userid = window.localStorage.getItem("userid");
           $('#notes-signid').val(signid);
           $('#notes-userid').val(userid);
           $('#notes-sign_type').val(sign_type);
           
           //   console.log(item);
           
           
           
           if (item.ordertype == 'Pending Delivery') {
           // ordertype = 'New Install';
           if (item.sign_type == 99)
           ordertype = 'Product Order';
           else
           ordertype = 'New Install';
           
           if (item.sign_flag_holder == 1) {
           isFlagHolderAndSolar = isFlagHolderAndSolar + '<p>Flag Holder : Yes</p>';
           }
           
           if (item.sign_solar != 0) {
           isFlagHolderAndSolar = isFlagHolderAndSolar + '<p>Solar : Yes</p>';
           }
           
           if (item.sign_brochure_holder == 1) {
           isFlagHolderAndSolar = isFlagHolderAndSolar + '<p>Brochure Holder : Yes</p>';
           }
           
           if (item.sign_floodlight != 0) {
           isFlagHolderAndSolar = isFlagHolderAndSolar + '<p>Floodlight : Yes</p>';
           }
           
           if (item.sign_legcolour != null) {
           isFlagHolderAndSolar = isFlagHolderAndSolar + '<p>Leg Color :  Yes (' + item.sign_legcolour + ')</p>';
           }
           
           } else {
           ordertype = item.ordertype;
           }
           var address = item.streetaddress + ' ' + item.streetaddress2 + ' ' + item.streetsuburb + ' ' + item.streetpostcode;
           
           // Comments
           
           if (item.sign_type == 1 && item.comments != null && item.comments != '') {
           Comment = '<div style="text-transform: none"> <br/><strong>Comments : </strong> <br/> <div style="padding-left:30px">' + item.comments + '<br/> </div>';
           }
           //tomtom url
           
           var geocoder = new google.maps.Geocoder();
           
           var tomtomAddress = address.replace(/[^a-zA-Z0-9 /.-]+/g, '');
           
           if (item.instructionPopup > 0) {
           var headerText = "";
           if (item.ordertype == 'Sign Doctor Request') {
           headerText = "<strong>Sign Doctor Details:</strong > <br />";
           }
           instructionText = headerText + '<br/>' + item.instructionText + '<br/>';
           }
           
           if (item.instructionPopup > 0 && item.ordertype_new == 'Pending Delivery') {
           showInstruction = true;
           window.localStorage.setItem("instructionText", item.instructionText);
           } else {
           showInstruction = false;
           window.localStorage.setItem("instructionText", "");
           }
           
           
           $.each(result.logs, function (i, log) {
                  logss = logss + '<p>' + log.date + ' ' + log.heading + '</p><p style="color: red">' + log.details + '</p><hr>';
                  
                  });
           
           
           var smallImgNew = '';
           if (result.dc_img.installinstructions.length > 0) {
           if (ordertype == 'New Install') {
           alert("An installation photo has been uploaded for this job, please follow the instructions");
           }
           
           /*  smallImgNew = '<br/><strong>Installation Instructions : </strong><hr><div class="my-gallery" id="smallImage2" style="float:left;width:100%;" itemscope itemtype="http://schema.org/ImageGallery">';
            
            $.each(result.dc_img.installinstructions, function (i, item) {
            console.log(uploads + item.img);
            
            smallImgNew = smallImgNew + '<figure style="float:left;" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject"><a href="' + uploads + item.img + '" itemprop="contentUrl" data-size="1024x1024"><img src="' + uploads + item.img_thumb + '" itemprop="thumbnail" alt="Image description" /></a><figcaption itemprop="caption description"></figcaption></figure>';*/
           
           smallImgNew = '<br/><strong>Installation Instructions : </strong><hr><p>';
           
           $.each(result.dc_img.installinstructions, function (i, item) {
                  console.log(uploads + item.img);
                  // alert(item.img);
                  
                  smallImgNew = smallImgNew + '<a href="' + uploads + item.img + '" class="fancybox" rel="group"><img src="' + uploads + item.img_thumb + '"  alt="Image description" /></a>';
                  
                  });
           
           smallImgNew = smallImgNew + '</p>';
           }
           
           var func = '';
           if (item.completed == 'yes') {
           func = 'alert(\'This job already been completed\')';
           } else {
           
           var instDate = item.installation_date.split("-");
           
           /*if(typeof(overlay) != "undefined" && overlay.standard_overlay == 1){
            standardOverlay = overlay.standard_overlay;
            }else if(typeof(overlay) != "undefined" && overlay.nonauctionflag == 1){
            standardOverlay = overlay.nonauctiondata;
            }else{
            standardOverlay = null;
            }*/
           
           //((typeof(overlay) != "undefined") && (overlay.standard_overlay == 1 || overlay.nonauctionflag == 1)) ? 1 : null;
           //((typeof(overlay) != "undefined") && (overlay.auction_overlay == 1 || overlay.auctionflag == 1)) ? 1 : null;
           
           /*if(typeof(overlay) != "undefined" && overlay.auction_overlay == 1){
            auctionOverlay = overlay.auction_overlay;
            }else if(typeof(overlay) != "undefined" && overlay.auctionflag == 1){
            auctionOverlay = overlay.auctiondata;
            }else{
            auctionOverlay = null;
            }*/
           
           //standardOverlay = (typeof(overlay) != "undefined" && overlay.standard_overlay !== '') ? overlay.standard_overlay : null;
           //auctionOverlay = (typeof(overlay) != "undefined" && overlay.auction_overlay !== '') ? overlay.auction_overlay : null;
           
           standardOverlay = (typeof (overlay) != "undefined" && overlay.nonauctionflag == 1) ? 1 : null;
           auctionOverlay = (typeof (overlay) != "undefined" && overlay.auctionflag == 1) ? 1 : null;
           
           if (item.flagholder_image != null && item.flagholder_image != '')
           window.localStorage.setItem("flagholder_image", item.flagholder_image);
           else
           window.localStorage.setItem("flagholder_image", "");
           // alert(item.delivery_flag);
           
           if (instDate.length == 3) {
           //console.log(overlay.standard_overlay)
           var installationDate = new Date(instDate[2], instDate[1] - 1, instDate[0]);
           if (installationDate > new Date()) {
           var day = installationDate.getDate();
           var monthIndex = installationDate.getMonth();
           var year = installationDate.getFullYear();
           func = 'confirmcompletedJobView(\'' + day + ' ' + monthNames[monthIndex] + ' ' + year + '\',' + sign_type + ',' + signid + ',\'' + item.ordertype + '\',' + item.delivery_run + ', ' + item.sign_flag_holder + ',' + item.sign_solar + ',' + item.sign_solar_qty + ', ' + item.sign_brochure_holder + ',' + item.sign_floodlight + ',' + item.sign_floodlight_qty + ',' + item.isrequired + ',\'' + item.RoofJobFee + '\', ' + standardOverlay + ', ' + auctionOverlay + ',' + item.sign_type + ')';
           } else {
           //completedJobClick
           /*
            func = 'completedJobView(' + sign_type + ',' + signid + ',\'' + item.ordertype + '\',' + item.delivery_run + ', ' + item.sign_flag_holder + ',' + item.sign_solar + ',' + item.sign_solar_qty + ', ' + item.sign_brochure_holder + ',' + item.sign_floodlight + ',' + item.sign_floodlight_qty + ',' + item.isrequired + ',\'' + item.RoofJobFee + '\', '+ standardOverlay +', '+ auctionOverlay +', '+ item.sign_qty +')';
            */
           //alert( item.sign_type);
           //0,350,Pending Delivery,8, 0,0,0, 0,0,0,0,'27.25', null, null, 1, 99,'New Install
           //0,350,Pending Delivery,8, 0,0,0, 0,0,0,0,'27.25', null, null, 1, New Install,undefined
           console.log(sign_type + ',' + signid + ',' + item.ordertype + ',' + item.delivery_run + ', ' + item.sign_flag_holder + ',' + item.sign_solar + ',' + item.sign_solar_qty + ', ' + item.sign_brochure_holder + ',' + item.sign_floodlight + ',' + item.sign_floodlight_qty + ',' + item.isrequired + ',\'' + item.RoofJobFee + '\', ' + standardOverlay + ', ' + auctionOverlay + ', ' + item.sign_qty + ', ' + item.sign_type + ',\'' + ordertype);
           
           func = 'completedJobClick(' + sign_type + ',' + signid + ',\'' + item.ordertype + '\',' + item.delivery_run + ', ' + item.sign_flag_holder + ',' + item.sign_solar + ',' + item.sign_solar_qty + ', ' + item.sign_brochure_holder + ',' + item.sign_floodlight + ',' + item.sign_floodlight_qty + ',' + item.isrequired + ',\'' + item.RoofJobFee + '\', ' + standardOverlay + ', ' + auctionOverlay + ', ' + item.sign_qty + ', ' + item.sign_type + ',\'' + ordertype + '\'' + ',' + showInstruction + ',\'' + item.sign_legcolour + '\')';
           
           /*func = 'completedJobClick(' + sign_type + ',' + signid + ',\'' + item.ordertype + '\',' + item.delivery_run + ', ' + item.sign_flag_holder + ',' + item.sign_solar + ',' + item.sign_solar_qty + ', ' + item.sign_brochure_holder + ',' + item.sign_floodlight + ',' + item.sign_floodlight_qty + ',' + item.isrequired + ',\'' + item.RoofJobFee + '\', '+ standardOverlay +', '+ auctionOverlay +', '+ item.sign_qty +',\''+ordertype+'\', ' + item.delivery_flag )';*/
           }
           
           } else {
           //console.log(overlay.standard_overlay)
           /*
            func = 'completedJobView(' + sign_type + ',' + signid + ',\'' + item.ordertype + '\',' + item.delivery_run + ', ' + item.sign_flag_holder + ',' + item.sign_solar + ',' + item.sign_solar_qty + ', ' + item.sign_brochure_holder + ',' + item.sign_floodlight + ',' + item.sign_floodlight_qty + ',' + item.isrequired + ',\'' + item.RoofJobFee + '\', '+ standardOverlay +', '+ auctionOverlay +', '+ item.sign_qty+')';
            */
           
           func = 'completedJobClick(' + sign_type + ',' + signid + ',\'' + item.ordertype + '\',' + item.delivery_run + ', ' + item.sign_flag_holder + ',' + item.sign_solar + ',' + item.sign_solar_qty + ', ' + item.sign_brochure_holder + ',' + item.sign_floodlight + ',' + item.sign_floodlight_qty + ',' + item.isrequired + ',\'' + item.RoofJobFee + '\', ' + standardOverlay + ', ' + auctionOverlay + ', ' + item.sign_qty + ',' + item.sign_type + ',\'' + ordertype + '\'' + ',' + showInstruction + ',\'' + item.sign_legcolour + '\')';
           
           }
           }
           
           logs = instructionText + logs + logss;
           var imgSrc = '';
           var stOverLayId = 0;
           
           if (typeof (item.st_overlay_id) != "undefined") {
           stOverLayId = parseInt(item.st_overlay_id, 10);
           // alert(stOverLayId);
           }
           
           // alert(sign_type);
           
           if (item.ordertype == 'Overlay Artwork Pending Delivery' && stOverLayId > 0) {
           // alert(item.ordertype);
           imgSrc = uploads + item.st_overlay_img_proof;
           
           }
           else if (sign_type == 99) {
           imgSrc = uploads + item.imagename;;
           }
           else {
           imgSrc = sign_type == '1' ? uploads + 'dc/dyo/temppdf/proof/agent-proof-' + item.signid + '.jpg' : uploads + 'signimages/' + item.signid + '/image/proof/' + item.imagename;
           console.log(imgSrc);
           }
           
           geocoder.geocode({
                            'address': address
                            }, function (results, status) {
                            var secondAgent = (item.name1 != undefined && item.surname1 != undefined) ? '<p>Agent Name: ' + item.name1 + ' ' + item.surname1 + '</p><p>Agent Mobile : ' + item.mobile1 + '</p>' : '';
                            
                            var overlayBlkDetails = '';
                            var auctionOverlayTxt = '';
                            // alert(item.sign_type);
                            
                            if (typeof (overlay) != "undefined" && overlay !== '') {
                            if ((item.sign_type == 0) && (typeof (overlay.auctionflag) != "undefined" || typeof (overlay.nonauctionflag) != "undefined")) {
                            var stdOverlayTxt = (overlay.nonauctionflag == 1) ? '<p style="color:red;" >Standard Overlay : ' + overlay.st_overlaytext + '</p>' : '';
                            
                            if (overlay.auctionflag == 1 && item.sign_type != 99) {
                            auctionOverlayTxt = '<p style="color:red;" >Auction Overlay</p><p class="sub-aucBlk">Listing Type : ' + overlay.au_listingtype + '</p><p class="sub-aucBlk">Date & Time : ' + overlay.au_datetime + '</p><p class="sub-aucBlk">Location : ' + overlay.au_location + '</p><p class="sub-aucBlk">Overlay Text : ' + overlay.au_overlaytext + '</p>';
                            } else {
                            auctionOverlayTxt = '';
                            }
                            
                            overlayBlkDetails = stdOverlayTxt + '' + auctionOverlayTxt;
                            } else if ((item.sign_type == 1) && (typeof (overlay.auctionflag) != "undefined" || typeof (overlay.nonauctionflag) != "undefined")) {
                            if (overlay.auctionflag == 1) {
                            var auctiondataDis = '<p style="color:red">Auction Overlay</p><ul>';
                            for (var i = 0; i < overlay.auctiondata.length; i++) {
                            auctiondataDis += '<li>' + overlay.auctiondata[i] + '</li>';
                            }
                            auctiondataDis += '</ul>';
                            } else {
                            auctiondataDis = '';
                            }
                            
                            if (overlay.nonauctionflag == 1) {
                            console.log(overlay.nonauctiondata.length)
                            var nonauctiondataDis = '<p style="color:red;">Standard Overlay</p> <ul>';
                            for (var j = 0; j < overlay.nonauctiondata.length; j++) {
                            for (var k = 0; k < overlay.nonauctiondata[j].length; k++) {
                            nonauctiondataDis += '<li>' + overlay.nonauctiondata[k] + '</li>';
                            }
                            }
                            nonauctiondataDis += '</ul>';
                            
                            } else {
                            nonauctiondataDis = '';
                            }
                            overlayBlkDetails = nonauctiondataDis + '' + auctiondataDis;
                            }
                            } else {
                            overlayBlkDetails = '';
                            }
                            
                            if (status == google.maps.GeocoderStatus.OK) {
                            var latitude = results[0].geometry.location.lat();
                            var longitude = results[0].geometry.location.lng();
                            var mapselected = window.localStorage.getItem("mapselected");
                            if (mapselected == "tomtom") {
                            tomtomUrl = 'tomtomhome:geo:action=navigateto&lat=' + latitude + '&long=' + longitude + '&name=' + tomtomAddress;
                            }
                            else if (mapselected == "apple") {
                            tomtomUrl = 'http://maps.apple.com/?daddr=' + latitude + ',' + longitude + '&dirflg=d&t=h&z=10';
                            }
                            else if (mapselected == "google") {
                            // tomtomUrl = 'comgooglemaps://&daddr='+ latitude + ',' + longitude + '&directionsmode=driving';
                            tomtomUrl = 'comgooglemapsurl://maps.google.com/?q=' + latitude + ',' + longitude + '&directionsmode=driving';
                            }
                            else {
                            tomtomUrl = 'waze://?ll=' + latitude + ',' + longitude + '&z=10';
                            }
                            //alert(1);
                            var quantityOfSignin = '<p>Quantity of Signs Ordered : ' + item.sign_qty + '</p>';
                            var installationMeth = '<p>Installation Method : ' + item.installation_method + '</p>';
                            
                            var addressText = '';
                            
                            if (item.signrelocate == 1 || item.signrelocate == "1" || item.signrelocate == 2 || item.signrelocate == "2") {
                            addressText = '<p>Pickup Address: ' + item.pickupaddress + '</p><p>Relcoate this sign to : ' + item.delievryaddress + '</p>';
                            }
                            
                            
                            if (item.sign_qty > 1)
                            quantityOfSignin = '<p style="color:red;" >Quantity of Signs Ordered : ' + item.sign_qty + '</p>';
                            
                            //alert(item.installation_method);
                            if (item.installation_method != "Regular" && item.installation_method != "")
                            installationMeth = '<p style="color:red;" >Installation Method : ' + item.installation_method + '</p>';
                            
                            var InstructionText = '<p>Instruction : ' + item.specialinstructions + '</p>';
                            var SignTypeText = '<p>Sign Type : ' + item.signtype + '</p>';
                            var OrderIdText = '';
                            
                            var SignModelText = '';
                            var SignIDText = '<p>Sign Id : ' + item.signid + '</p>';
                            
                            if (item.sign_type == 99) {
                            //alert(item.product_option);
                            SignModelText = '';
                            installationMeth = '';
                            quantityOfSignin = '';
                            InstructionText = '';
                            SignTypeText = '';
                            OrderIdText = '<p>Product Order Id : ' + item.orderid + '</p>' +
                            '<p>Product Name : ' + item.product_name + '</p>' +
                            ((item.product_option != '') ? '<p>Options : ' + item.product_option + '</p>' : '') +
                            '<p>Quantity : ' + item.pro_qty + '</p>' +
                            '<p>Instructions : ' + item.pro_instruction + '</p>';
                            SignIDText = '';
                            
                            } else {
                            SignModelText = '<p>Sign Model : ' + item.signmodel + '</p>' + isFlagHolderAndSolar;
                            }
                            
                            var customOrderText = '';
                            
                            if (item.sign_type == 2) {
                            customOrderText = '<p>Subject : ' + item.subject + '</p><p>Information : ' + item.information + '</p>'
                            } else {
                            customOrderText = '';
                            }
                            
                            /*  var html = '<div class="ui-bar ui-bar-a" style="background-color: #171D2B; color : #fff"><span><h4 style="margin-right: 20px"><a   href="' + tomtomUrl + '" style="color : #fff; font-size:2em;"> ' + item.streetaddress + ' ' + item.streetaddress2 + ' ' + item.streetsuburb + ' ' + item.streetpostcode + '</a></h4> </span></div><div class="ui-body"><div class="ui-grid-b"><div class="ui-block-a" style="width: 35%"><img src="' + imgSrc + '" width=90% ></div><div class="ui-block-b" style="width: 45%"><p><span style="font-weight: bold; border: 2px solid red">' + ordertype + '</span></p>'+OrderIdText+SignTypeText+ SignModelText + InstructionText+ quantityOfSignin+ '</p>'+ installationMeth+ overlayBlkDetails +customOrderText+'</div><div class="ui-block-c" style="background-color: #171D2B; color : #fff; padding : 5px; width: 20%"><p>Agency : ' + item.agencyname + '</p><p>Agent Name: ' + item.name + ' ' + item.surname + '</p><p>Agent Mobile : ' + item.mobile + '</p>' + secondAgent + '</div></div><div><div style="text-align:center;"><img style="-webkit-user-select: none" src="http://maps.googleapis.com/maps/api/streetview?size=' + $(window).width() + 'x275&amp;location=' + latitude + ',' + longitude + '"></div>' + smallImgNew + logs + '</div></div>';*/
                            
                            var html = '<div class="ui-bar ui-bar-a" style="background-color: #171D2B; color : #fff"><span><h4 style="margin-right: 20px"><a   href="' + tomtomUrl + '" style="color : #fff; font-size:2em;"> ' + item.streetaddress + ' ' + item.streetaddress2 + ' ' + item.streetsuburb + ' ' + item.streetpostcode + '</a></h4> </span></div><div class="ui-body"><div class="ui-grid-b"><div class="ui-block-a" style="width: 35%"><img src="' + imgSrc + '" width=90% ></div><div class="ui-block-b" style="width: 45%"><p><span style="font-weight: bold; border: 2px solid red">' + item.ordertype_new + '</span></p>' + SignIDText + OrderIdText + SignTypeText + SignModelText + InstructionText + quantityOfSignin + '</p>' + installationMeth + overlayBlkDetails + customOrderText + addressText + '</div><div class="ui-block-c" style="background-color: #171D2B; color : #fff; padding : 5px; width: 20%"><p>Agency : ' + item.agencyname + '</p><p>Agent Name: ' + item.name + ' ' + item.surname + '</p><p>Agent Mobile : ' + item.mobile + '</p>' + secondAgent + '</div></div><div><div style="text-align:center;"><img style="-webkit-user-select: none" src="http://maps.googleapis.com/maps/api/streetview?size=' + $(window).width() + 'x275&amp;location=' + latitude + ',' + longitude + '"></div>' + smallImgNew + Comment + logs + '</div></div>';
                            
                            /*  var html = '<div class="ui-bar ui-bar-a" style="background-color: #171D2B; color : #fff"><span><h4 style="margin-right: 20px"><a   href="' + tomtomUrl + '" style="color : #fff; font-size:2em;"> ' + item.streetaddress + ' ' + item.streetaddress2 + ' ' + item.streetsuburb + ' ' + item.streetpostcode + '</a></h4> </span></div><div class="ui-body"><div class="ui-grid-b"><div class="ui-block-a" style="width: 35%"><img src="' + imgSrc + '" width=90% ></div><div class="ui-block-b" style="width: 45%"><p><span style="font-weight: bold; border: 2px solid red">' + ordertype + '</span></p><p>Sign Id : ' + item.signid + '</p>'+OrderIdText+SignTypeText+ SignModelText + InstructionText+ quantityOfSignin+ '</p>'+ installationMeth+ overlayBlkDetails +customOrderText+'</div><div class="ui-block-c" style="background-color: #171D2B; color : #fff; padding : 5px; width: 20%"><p>Agency : ' + item.agencyname + '</p><p>Agent Name: ' + item.name + ' ' + item.surname + '</p><p>Agent Mobile : ' + item.mobile + '</p>' + secondAgent + '</div></div><div><div style="text-align:center;"><img style="-webkit-user-select: none" src="http://maps.googleapis.com/maps/api/streetview?size=' + $(window).width() + 'x275&amp;location=' + latitude + ',' + longitude + '"></div>' + smallImgNew + logs + '</div></div>'; */
                            
                            /* var html = '<div class="ui-bar ui-bar-a" style="background-color: #171D2B; color : #fff"><span><h4 style="margin-right: 20px"><a   href="' + tomtomUrl + '" style="color : #fff; font-size:2em;"> ' + item.streetaddress + ' ' + item.streetaddress2 + ' ' + item.streetsuburb + ' ' + item.streetpostcode + '</a></h4> </span></div><div class="ui-body"><div class="ui-grid-b"><div class="ui-block-a" style="width: 35%"><img src="' + imgSrc + '" width=90% ></div><div class="ui-block-b" style="width: 45%"><p><span style="font-weight: bold; border: 2px solid red">' + ordertype + '</span></p><p>Sign Id : ' + item.signid + '</p><p>Sign Type : ' + item.signtype + '</p><p>Sign Model : ' + item.signmodel + '</p>' + isFlagHolderAndSolar + '<p>Instruction : ' + item.specialinstructions + '</p>'+ quantityOfSignin+ '</p>'+ installationMeth+ overlayBlkDetails +'</div><div class="ui-block-c" style="background-color: #171D2B; color : #fff; padding : 5px; width: 20%"><p>Agency : ' + item.agencyname + '</p><p>Agent Name: ' + item.name + ' ' + item.surname + '</p><p>Agent Mobile : ' + item.mobile + '</p>' + secondAgent + '</div></div><div><div style="text-align:center;"><img style="-webkit-user-select: none" src="http://maps.googleapis.com/maps/api/streetview?size=' + $(window).width() + 'x275&amp;location=' + latitude + ',' + longitude + '"></div>' + smallImgNew + logs + '</div></div>'; */
                            /*  var html = '<div class="ui-bar ui-bar-a" style="background-color: #171D2B; color : #fff"><span><h4 style="margin-right: 20px"><a   href="' + tomtomUrl + '" style="color : #fff; font-size:2em;"> ' + item.streetaddress + ' ' + item.streetaddress2 + ' ' + item.streetsuburb + ' ' + item.streetpostcode + '</a></h4> </span></div><div class="ui-body"><div class="ui-grid-b"><div class="ui-block-a" style="width: 35%"><img src="' + imgSrc + '" width=90% ></div><div class="ui-block-b" style="width: 45%"><p><span style="font-weight: bold; border: 2px solid red">' + ordertype + '</span></p><p>Sign Id : ' + item.signid + '</p><p>Sign Type : ' + item.signtype + '</p><p>Sign Model : ' + item.signmodel + '</p>' + isFlagHolderAndSolar + '<p>Instruction : ' + item.specialinstructions + '</p>'+ '<p>Quantity of Signs Ordered : ' + item.sign_qty + '</p>'+ '</p>'+ '<p>Installation Method : ' + item.installation_method + '</p>'+ overlayBlkDetails +'</div><div class="ui-block-c" style="background-color: #171D2B; color : #fff; padding : 5px; width: 20%"><p>Agency : ' + item.agencyname + '</p><p>Agent Name: ' + item.name + ' ' + item.surname + '</p><p>Agent Mobile : ' + item.mobile + '</p>' + secondAgent + '</div></div><div><div style="text-align:center;"><img style="-webkit-user-select: none" src="http://maps.googleapis.com/maps/api/streetview?size=' + $(window).width() + 'x275&amp;location=' + latitude + ',' + longitude + '"></div>' + smallImgNew + logs + '</div></div>'; */
                            
                            } else {
                            
                            var quantityOfSignin = '<p>Quantity of Signs Ordered : ' + item.sign_qty + '</p>';
                            var installationMeth = '<p>Installation Method : ' + item.installation_method + '</p>';
                            
                            if (item.sign_qty > 1)
                            quantityOfSignin = '<p style="color:red;" >Quantity of Signs Ordered : ' + item.sign_qty + '</p>';
                            
                            // alert('2:'+item.installation_method);
                            if (item.installation_method != "Regular" && item.installation_method != "")
                            installationMeth = '<p style="color:red;" >Installation Method : ' + item.installation_method + '</p>';
                            
                            var addressText = '';
                            
                            if (item.signrelocate == 1 || item.signrelocate == "1" || item.signrelocate == 2 || item.signrelocate == "2") {
                            addressText = '<p>Pickup Address: ' + item.pickupaddress + '</p><p>Relcoate this sign to : ' + item.delievryaddress + '</p>';
                            }
                            
                            var SignTypeText = '<p>Sign Type : ' + item.signtype + '</p>';
                            var InstructionText = '<p>Instruction : ' + item.specialinstructions + '</p>';
                            var OrderIdText = '';
                            
                            var SignModelText = '';
                            var SignIdText = '<p>Sign Id : ' + item.signid + '</p>';
                            
                            if (item.sign_type == 99) {
                            
                            SignModelText = '';
                            quantityOfSignin = '';
                            installationMeth = '';
                            SignTypeText = '';
                            InstructionText = '';
                            OrderIdText = '<p>Product Order Id : ' + item.orderid + '</p>' +
                            '<p>Product Name : ' + item.product_name + '</p>' +
                            '<p>Quantity : ' + item.pro_qty + '</p>' +
                            ((item.product_option != '') ? '<p>Options : ' + item.product_option + '</p>' : '') +
                            '<p>Instructions : ' + item.pro_instruction + '</p>';
                            SignIdText = '';
                            } else {
                            SignModelText = '<p>Sign Model : ' + item.signmodel + '</p>' + isFlagHolderAndSolar;
                            
                            }
                            
                            var customOrderText = '';
                            if (item.sign_type == 2) {
                            customOrderText = '<p>Subject : ' + item.subject + '</p><p>Information : ' + item.information + '</p>'
                            } else {
                            customOrderText = '';
                            }
                            
                            /*  var html = '<div class="ui-bar ui-bar-a" style="background-color: #171D2B; color : #fff"><span><h4 style="margin-right: 20px" style="color : #fff; font-size:2em;">' + item.streetaddress + ' ' + item.streetaddress2 + ' ' + item.streetsuburb + ' ' + item.streetpostcode + '</h4> </span></div><div class="ui-body"><div class="ui-grid-b"><div class="ui-block-a" style="width: 35%"><img src="' + imgSrc + '" width=90% ></div><div class="ui-block-b" style="width: 45%"><p><span style="font-weight: bold; border: 2px solid red">' + ordertype + '</span></p>'+OrderIdText +SignTypeText+ SignModelText + InstructionText+ quantityOfSignin+ '</p>'+ installationMeth+ overlayBlkDetails +customOrderText+'</div><div class="ui-block-c" style="background-color: #171D2B; color : #fff; padding : 5px; width: 20%"><p>Agency : ' + item.agencyname + '</p><p>Agent Name: ' + item.name + ' ' + item.surname + '</p><p>Agent Mobile : ' + item.mobile + '</p>' + secondAgent + '</div></div><div><br/>' + smallImgNew + logs + '</div></div>'; */
                            
                            var html = '<div class="ui-bar ui-bar-a" style="background-color: #171D2B; color : #fff"><span><h4 style="margin-right: 20px" style="color : #fff; font-size:2em;">' + item.streetaddress + ' ' + item.streetaddress2 + ' ' + item.streetsuburb + ' ' + item.streetpostcode + '</h4> </span></div><div class="ui-body"><div class="ui-grid-b"><div class="ui-block-a" style="width: 35%"><img src="' + imgSrc + '" width=90% ></div><div class="ui-block-b" style="width: 45%"><p><span style="font-weight: bold; border: 2px solid red">' + item.ordertype_new + '</span></p>' + SignIdText + OrderIdText + SignTypeText + SignModelText + InstructionText + quantityOfSignin + '</p>' + installationMeth + overlayBlkDetails + customOrderText + addressText + '</div><div class="ui-block-c" style="background-color: #171D2B; color : #fff; padding : 5px; width: 20%"><p>Agency : ' + item.agencyname + '</p><p>Agent Name: ' + item.name + ' ' + item.surname + '</p><p>Agent Mobile : ' + item.mobile + '</p>' + secondAgent + '</div></div><div><br/>' + smallImgNew + Comment + logs + '</div></div>';
                            /* var html = '<div class="ui-bar ui-bar-a" style="background-color: #171D2B; color : #fff"><span><h4 style="margin-right: 20px" style="color : #fff; font-size:2em;">' + item.streetaddress + ' ' + item.streetaddress2 + ' ' + item.streetsuburb + ' ' + item.streetpostcode + '</h4> </span></div><div class="ui-body"><div class="ui-grid-b"><div class="ui-block-a" style="width: 35%"><img src="' + imgSrc + '" width=90% ></div><div class="ui-block-b" style="width: 45%"><p><span style="font-weight: bold; border: 2px solid red">' + ordertype + '</span></p><p>Sign Id : ' + item.signid + '</p>'+OrderIdText +SignTypeText+ SignModelText + InstructionText+ quantityOfSignin+ '</p>'+ installationMeth+ overlayBlkDetails +customOrderText+'</div><div class="ui-block-c" style="background-color: #171D2B; color : #fff; padding : 5px; width: 20%"><p>Agency : ' + item.agencyname + '</p><p>Agent Name: ' + item.name + ' ' + item.surname + '</p><p>Agent Mobile : ' + item.mobile + '</p>' + secondAgent + '</div></div><div><br/>' + smallImgNew + logs + '</div></div>'; */
                            
                            /*   var html = '<div class="ui-bar ui-bar-a" style="background-color: #171D2B; color : #fff"><span><h4 style="margin-right: 20px" style="color : #fff; font-size:2em;">' + item.streetaddress + ' ' + item.streetaddress2 + ' ' + item.streetsuburb + ' ' + item.streetpostcode + '</h4> </span></div><div class="ui-body"><div class="ui-grid-b"><div class="ui-block-a" style="width: 35%"><img src="' + imgSrc + '" width=90% ></div><div class="ui-block-b" style="width: 45%"><p><span style="font-weight: bold; border: 2px solid red">' + ordertype + '</span></p><p>Sign Id : ' + item.signid + '</p><p>Sign Type : ' + item.signtype + '</p><p>Sign Model : ' + item.signmodel + '</p>' + isFlagHolderAndSolar + '<p>Instruction : ' + item.specialinstructions + '</p>'+ quantityOfSignin+ '</p>'+ installationMeth+ overlayBlkDetails +'</div><div class="ui-block-c" style="background-color: #171D2B; color : #fff; padding : 5px; width: 20%"><p>Agency : ' + item.agencyname + '</p><p>Agent Name: ' + item.name + ' ' + item.surname + '</p><p>Agent Mobile : ' + item.mobile + '</p>' + secondAgent + '</div></div><div><br/>' + smallImgNew + logs + '</div></div>';*/
                            
                            /*   var html = '<div class="ui-bar ui-bar-a" style="background-color: #171D2B; color : #fff"><span><h4 style="margin-right: 20px" style="color : #fff; font-size:2em;">' + item.streetaddress + ' ' + item.streetaddress2 + ' ' + item.streetsuburb + ' ' + item.streetpostcode + '</h4> </span></div><div class="ui-body"><div class="ui-grid-b"><div class="ui-block-a" style="width: 35%"><img src="' + imgSrc + '" width=90% ></div><div class="ui-block-b" style="width: 45%"><p><span style="font-weight: bold; border: 2px solid red">' + ordertype + '</span></p><p>Sign Id : ' + item.signid + '</p><p>Sign Type : ' + item.signtype + '</p><p>Sign Model : ' + item.signmodel + '</p>' + isFlagHolderAndSolar + '<p>Instruction : ' + item.specialinstructions + '</p>'+ '<p>Quantity of Signs Ordered : ' + item.sign_qty + '</p>'+ '</p>'+ '<p>Installation Method : ' + item.installation_method + '</p>'+ overlayBlkDetails +'</div><div class="ui-block-c" style="background-color: #171D2B; color : #fff; padding : 5px; width: 20%"><p>Agency : ' + item.agencyname + '</p><p>Agent Name: ' + item.name + ' ' + item.surname + '</p><p>Agent Mobile : ' + item.mobile + '</p>' + secondAgent + '</div></div><div><br/>' + smallImgNew + logs + '</div></div>';*/
                            
                            }
                            
                            $('#jobdetails').html(html);
                            if (result.dc_img.installinstructions.length > 0) {
                            //initPhotoSwipeFromDOM('#smallImage2');
                            initFancyBox();
                            }
                            });
           
           
           //<a class="ui-btn ui-btn-inline  ui-corner-all" onclick="showReallocateView(' + sign_type + ',' + signid + ')" class="ui-btn ui-shadow ui-corner-all " >Re-Allocate Job</a>
           // alert(func);
           
           var AddNotesText = '';
           
           if (item.sign_type == 99) {
           AddNotesText = '';
           } else {
           AddNotesText = '<a class="ui-btn ui-btn-inline  ui-corner-all" href="#signnotes" data-rel="popup" data-position-to="window" >Add Notes</a>';
           }
           
           var useonlyapp = window.localStorage.getItem("useonlyapp");
           
           
           if (useonlyapp != null && (useonlyapp == true || useonlyapp == "true" || useonlyapp == "1" || useonlyapp == 1)) {
           var footerhtml = '<div style="text-align: center; font-size: 18px;"><a class="ui-btn ui-btn-inline  ui-corner-all" onclick="' + func + '" class="ui-btn ui-shadow ui-corner-all " >Job Completed</a>' + AddNotesText + '<a class="ui-btn ui-btn-inline  ui-corner-all" onclick="showReallocateView(' + sign_type + ',' + signid + ')">Change Installer</a><div id="sign-address" style="display:none;">' + item.streetaddress + ' ' + item.streetaddress2 + ' ' + item.streetsuburb + ' ' + item.streetpostcode + '</div></div>';
           
           } else {
           
           var footerhtml = '<div style="text-align: center; font-size: 18px;"><a class="ui-btn ui-btn-inline  ui-corner-all" onclick="' + func + '" class="ui-btn ui-shadow ui-corner-all " >Job Completed</a>' + AddNotesText + '<a class="ui-btn ui-btn-inline  ui-corner-all" data-rel="popup" data-position-to="window" onclick="redirectTosignview(' + item.sign_type + ',' + item.signid + ',' + item.orderid + ',' + item.agencyid + ',' + item.somid + ')">View Job Details</a><a class="ui-btn ui-btn-inline  ui-corner-all" onclick="showReallocateView(' + sign_type + ',' + signid + ')">Change Installer</a><div id="sign-address" style="display:none;">' + item.streetaddress + ' ' + item.streetaddress2 + ' ' + item.streetsuburb + ' ' + item.streetpostcode + '</div></div>';
           }
           
           /*var footerhtml = '<div style="text-align: center; font-size: 18px;"><a class="ui-btn ui-btn-inline  ui-corner-all" onclick="' + func + '" class="ui-btn ui-shadow ui-corner-all " >Job Completed</a><a class="ui-btn ui-btn-inline  ui-corner-all" href="#signnotes" data-rel="popup" data-position-to="window" >Add Notes</a><a class="ui-btn ui-btn-inline  ui-corner-all" data-rel="popup" data-position-to="window" onclick="redirectTosignview(' + item.sign_type + ',' + item.signid + ',' + item.orderid + ',' + item.agencyid + ')">View Job Details</a><a class="ui-btn ui-btn-inline  ui-corner-all" onclick="showReallocateView(' + sign_type + ',' + signid + ')">Change Installer</a><div id="sign-address" style="display:none;">' + item.streetaddress + ' ' + item.streetaddress2 + ' ' + item.streetsuburb + ' ' + item.streetpostcode + '</div></div>'; */
           
           //$('#jobdetails').html(html);
           $('#jv-footer').html(footerhtml);
           
           $('.version').html(version);
           showAlert = true;
           $(':mobile-pagecontainer').pagecontainer({
                                                    change: function (e, u) {
                                                    if (u.toPage[0].id == "jobview-page") {
                                                    var stOverLayId = 0;
                                                    var isHavingAgent = false;
                                                    
                                                    if (typeof (item.st_overlay_id) != "undefined") {
                                                    stOverLayId = parseInt(item.st_overlay_id, 10);
                                                    //alert(stOverLayId);
                                                    }
                                                    
                                                    var instructionText = "";
                                                    if (item.instructionPopup > 0)
                                                    instructionText = item.instructionText;
                                                    
                                                    if ((item.agentid != 0 || item.agentid2 != 0) && item.WarnJobIsNext == 1 && showAlert)
                                                    isHavingAgent = true;
                                                    
                                                    if (item.ordertype == 'Overlay Artwork Pending Delivery' && stOverLayId > 0) {
                                                    dispalyOverlayMessage(sign_type, signid, item.orderid, isHavingAgent, instructionText, item.instructionPopup);
                                                    }
                                                    else if ((item.agentid != 0 || item.agentid2 != 0) && item.WarnJobIsNext == 1 && showAlert) {
                                                    
                                                    var relocateaddress = '';
                                                    
                                                    if (item.relocateFlag == 1 || item.relocateFlag == "1") {
                                                    relocateaddress = item.relocateaddressto;
                                                    } else if (item.relocateFlag == 2 || item.relocateFlag == "2") {
                                                    relocateaddress = item.relocateaddressfrom;
                                                    }
                                                    
                                                    confirmWarningForNextJob(sign_type, signid, item.orderid, relocateaddress, instructionText, item.instructionPopup);
                                                    } else if (item.instructionPopup > 0) {
                                                    if (item.instructionText != '') {
                                                    setTimeout(function () {
                                                               showinstructionPopUp(instructionText, item.instructionPopup);
                                                               }, 500);
                                                    }
                                                    }
                                                    
                                                    // dispalyOverlayMessage(sign_type, signid, item.orderid);
                                                    
                                                    if (item.relocateFlag == 1 || item.relocateFlag == "1") {
                                                    //alert(+ '\'' + item.relocateaddressto + '\'');
                                                    setTimeout(function () {
                                                               showRelocatePopUp(item.relocateaddressto, instructionText, item.instructionPopup);
                                                               }, 500);
                                                    }
                                                    else if (item.relocateFlag == 2 || item.relocateFlag == "2") {
                                                    //alert(+ '\'' + item.relocateaddressfrom + '\'');
                                                    setTimeout(function () {
                                                               showRelocatePopUp(item.relocateaddressfrom, instructionText, item.instructionPopup);
                                                               }, 500);
                                                    }
                                                    }
                                                    
                                                    }
                                                    });
           $(':mobile-pagecontainer').pagecontainer('change', "#jobview-page", {
                                                    transition: "slide"
                                                    });
           },
           error: function (jqXHR, textStatus, errorThrown) {
           checkInterntConnection();
           //alert('Network error has occurred please try again!');//3-
           }
           });
    
}

function showRelocatePopUp(popMessage, instructionText, instructionPopup) {
    var pophtml = '<div style="padding:20px 10px 20px 20px ;"><lable>' + popMessage + '</lablel><br/><p><a id="btn-cancel" onclick="closePopup(\'' + instructionText + '\', ' + instructionPopup + ')" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">OK</a></p></div>';
    $('#jnc-popup4').html(pophtml);
    $('#jnc-popup4').popup('open');
}

function showinstructionPopUp(instructionText, instructionPopup) {
    var headerText = "";
    if (instructionPopup == 1) {
        headerText = "Installation Instructions have been included for this sign, see below:"
    } else {
        headerText = "Sign Doctor Instructions have been included for this sign, see below:"
    }
    var pophtml = '<div style="padding:20px 10px 20px 20px ;"><lable> <strong>' + headerText + '</strong> <br/> <br/>' + instructionText + '</lablel><br/><p><a id="btn-cancel" onclick="closeinstructionPopup()" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">OK</a></p></div>';
    $('#jnc-popup4').html(pophtml);
    $('#jnc-popup4').popup('open');
}


function closeinstructionPopup() {
    $('#jnc-popup4').popup('close');
}

function closePopup(instructionText, instructionPopup) {
    $('#jnc-popup4').popup('close');
    
    if (instructionText != '') {
        setTimeout(function () {
                   showinstructionPopUp(instructionText, instructionPopup);
                   }, 500);
    }
}

function redirectTosignview(sign_type, signid, orderid, agencyid, somid) {
    if (sign_type == 99) {
        //console.log(inappUrlProductOrder + orderid + '/fromapp' );
        var ref = window.open(inappUrlProductOrder + somid + '/fromapp', '_blank', 'location=yes');
    }
    else if (sign_type == 1) {
        var ref = window.open(inappUrl + signid + '/' + agencyid + '/' + orderid + '/fromapp?sign_type=dyo', '_blank', 'location=yes');
    }
    else if (sign_type == 2) {
        var ref = window.open(inappUrl + signid + '/' + agencyid + '/' + orderid + '/fromapp?sign_type=custom', '_blank', 'location=yes');
    }
    else {
        var ref = window.open(inappUrl + signid + '/' + agencyid + '/' + orderid + '/fromapp', '_blank', 'location=yes');
    }
    
}

function jobview(sign_type, signid) {
    var userid = window.localStorage.getItem("userid");
    var username = window.localStorage.getItem("username");
    
    if (userid && username) {
        getJobdetails(sign_type, signid);
        
    } else {
        $('.version').html(version);
        $(':mobile-pagecontainer').pagecontainer('change', "#login-page");
    }
}

//=============================================== Job Completed page ===========================================================//


function confirmcompletedJobView(installation_date, sign_type, signid, signtype, run, flagHolder, solar, solarQty, brochureHolder, floodlight, floodlightQty, isrequired, RoofJobFee, standard_overlay, auction_overlay, itemSignType) {
    var popMessage = "WARNING: You are installing a sign BEFORE the installation date of: " + installation_date + ", please acknowledge you are doing this as unless instructed by the agent to install the sign early, the sign should not be installed until " + installation_date;
    
    var pophtml = '<div style="padding:20px 10px 20px 20px ;"><lable>' + popMessage + '</lablel><p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="completedJobView(' + sign_type + ',' + signid + ',\'' + signtype + '\',' + run + ', ' + flagHolder + ',' + solar + ',' + solarQty + ', ' + brochureHolder + ',' + floodlight + ',' + floodlightQty + ',' + isrequired + ',\'' + RoofJobFee + '\', ' + standard_overlay + ', ' + auction_overlay + ',' + itemSignType + ')">I Acknowledge I am installing the sign early</a><a id="btn-cancel" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">Cancel, don\'t set as installed</a></p></div>';
    $('#jnc-popup3').html(pophtml);
    $('#jnc-popup3').popup('open');
}



function dispalyOverlayMessage(sign_type, signid, orderid, isHavingAgent, instructionText, instructionPopup) {
    var popMessage = "An Overlay design was chosen by the customer for this job, please ensure you place the overlay as shown in the top left of the job sheet";
    var popMessage2 = "";
    var pophtml = '<div style="padding:20px 10px 20px 20px ;"><lable>' + popMessage + '</lablel><p><a id="btn-cancel" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="callWarningPopup(' + sign_type + ',' + signid + ', ' + orderid + ',' + isHavingAgent + ',\'' + instructionText + '\', ' + instructionPopup + ')" >OK</a></p></div>';
    $('#jnc-popup3').html(pophtml);
    $('#jnc-popup3').popup('open');
    showAlert = true;   /* var popMessage2 = "please ensure you place the overlay as shown in the top left of the job sheet";
                         var pophtml = '<div style="padding:20px 10px 20px 20px ;"><lable>' + popMessage +'<br/>'+popMessage2+ '</lablel><p><a id="btn-cancel" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check">OK</a></p></div>';
                         $('#jnc-popup3').html(pophtml);
                         $('#jnc-popup3').popup('open');
                         showAlert = false;*/
    
}

function callWarningPopup(sign_type, signid, orderid, isHavingAgent, instructionText, instructionPopup) {
    $('#jnc-popup3').popup('close');
    
    if (isHavingAgent) {
        
        setTimeout(function () {
                   confirmWarningForNextJob(sign_type, signid, orderid, '', instructionText, instructionPopup);
                   }, 500);
    }
    
}

function confirmWarningForNextJob(sign_type, signid, orderid, relocateaddress, instructionText, instructionPopup) {
    
    // $('#jnc-popup3').html("");
    // $('#jnc-popup3').popup('close');
    var popMessage = "Do you want to notify the Agent this job is next?";
    
    var pophtml = '<div style="padding:20px 10px 20px 20px ;"><lable>' + popMessage + '</lablel><p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="WarningForNextJob(' + sign_type + ',' + signid + ', ' + orderid + ',\'' + relocateaddress + '\'' + ',\'' + instructionText + '\', ' + instructionPopup + ' )">Yes</a><a id="btn-cancel" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete"  onclick="closeWarningForNextJob(\'' + relocateaddress + '\'' + ',\'' + instructionText + '\', ' + instructionPopup + ')" >No</a></p></div>';
    // alert('hai');
    $('#jnc-popup3').html(pophtml);
    $('#jnc-popup3').popup('open');
    showAlert = false;
    
}

function closeWarningForNextJob(relocateaddress, instructionText, instructionPopup) {
    $('#jnc-popup3').popup('close');
    
    if (relocateaddress != '') {
        setTimeout(function () {
                   showRelocatePopUp(relocateaddress, instructionText, instructionPopup);
                   }, 500);
    } else if (instructionText != '') {
        setTimeout(function () {
                   showinstructionPopUp(instructionText, instructionPopup);
                   }, 500);
    }
    
}

function WarningForNextJob(sign_type, signid, orderid, relocateaddress, instructionText, instructionPopup) {
    
    var userid = window.localStorage.getItem("userid");
    $.ajax({
           type: 'post',
           url: remoteUrl + 'warnjobisnext',
           data: { signid: signid, sign_type: sign_type, orderid: orderid, userid: userid },
           cache: false,
           timeout: 60000,
           beforeSend: function () {
           $.mobile.loading('show');
           },
           complete: function () {
           $.mobile.loading('hide');
           },
           success: function (result) {
           $('#jnc-popup3').popup('close');
           if (relocateaddress != '') {
           setTimeout(function () {
                      showRelocatePopUp(relocateaddress, instructionText, instructionPopup);
                      }, 500);
           } else if (instructionText != '') {
           setTimeout(function () {
                      showinstructionPopUp(instructionText, instructionPopup);
                      }, 500);
           }
           },
           error: function (jqXHR, textStatus, errorThrown) {
           //alert('Network error has occurred please try again!');//4-
           }
           });
    
}


function completedJobClick(sign_type, signid, signtype, run, flagHolder, solar, solarQty, brochureHolder, floodlight, floodlightQty, isrequired, RoofJobFee, standard_overlay, auction_overlay, sign_qty, itemSignType, ordertype, showInstruction, sign_legcolour) {
    //console.log(ordertype);//Sign Doctor Request  //Remove Sign
    //alert(itemSignType);
    
    window.localStorage.removeItem("selectedoptionb");
    window.localStorage.removeItem("signatureFileName");
    window.localStorage.removeItem("uploadImageName");
    
    gOrdertype = ordertype
    
    if (sign_qty > 1 && ordertype == 'New Install') {
        //alert(sign_qyt);
        var func = 'completedJobView(' + sign_type + ',' + signid + ',\'' + signtype + '\',' + run + ', ' + flagHolder + ',' + solar + ',' + solarQty + ', ' + brochureHolder + ',' + floodlight + ',' + floodlightQty + ',' + isrequired + ',\'' + RoofJobFee + '\', ' + standard_overlay + ', ' + auction_overlay + ', ' + sign_qty + ',' + itemSignType + ',' + showInstruction + ',\'' + sign_legcolour + '\')';
        
        var popMessage = "The Quantity of Signs Ordered was " + sign_qty + ", have you installed " + sign_qty + " signs?";
        
        var pophtml = '<div style="padding:20px 10px 20px 20px ;"><lable>' + popMessage + '</lablel><p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="' + func + '">Yes</a><a id="btn-cancel" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">No</a></p></div>';
        $('#jnc-popup3').html(pophtml);
        $('#jnc-popup3').popup('open');
        showAlert = false;
    }
    else if ((sign_type == 2 && ordertype == 'New Install') || (sign_type == 99 && ordertype == 'Product Order')) //else if((sign_type == 2 || sign_type == 99) && ordertype == 'New Install' )
    {
        // alert(sign_type);
        
        var funcYES = 'completedJobView(' + sign_type + ',' + signid + ',\'' + signtype + '\',' + run + ', ' + flagHolder + ',' + solar + ',' + solarQty + ', ' + brochureHolder + ',' + floodlight + ',' + floodlightQty + ',' + isrequired + ',\'' + RoofJobFee + '\', ' + standard_overlay + ', ' + auction_overlay + ', ' + sign_qty + ',' + itemSignType + ',' + showInstruction + ',\'' + sign_legcolour + '\',\'YES\')';
        
        var funcNO = 'completedJobView(' + sign_type + ',' + signid + ',\'' + signtype + '\',' + run + ', ' + flagHolder + ',' + solar + ',' + solarQty + ', ' + brochureHolder + ',' + floodlight + ',' + floodlightQty + ',' + isrequired + ',\'' + RoofJobFee + '\', ' + standard_overlay + ', ' + auction_overlay + ', ' + sign_qty + ',' + itemSignType + ',' + showInstruction + ',\'' + sign_legcolour + '\',\'NO\')';     /* var funcNewInstaller = 'completedJobViewForNewInstaller(' + sign_type + ',' + signid + ',\'' + signtype + '\',' + run + ', ' + flagHolder + ',' + solar + ',' + solarQty + ', ' +brochureHolder+ ',' + floodlight + ',' + floodlightQty + ',' + isrequired + ',\'' + RoofJobFee + '\', '+ standard_overlay +', '+ auction_overlay +', '+ sign_qty +')';
                                                                                                                                                                                                                                                                                                                                                                                                                                   
                                                                                                                                                                                                                                                                                                                                                                                                                                   var func = 'completedJobView(' + sign_type + ',' + signid + ',\'' + signtype + '\',' + run + ', ' + flagHolder + ',' + solar + ',' + solarQty + ', ' +brochureHolder+ ',' + floodlight + ',' + floodlightQty + ',' + isrequired + ',\'' + RoofJobFee + '\', '+ standard_overlay +', '+ auction_overlay +', '+ sign_qty +')'; */
        
        var popMessage = "Are you delivering this to the office?";
        
        var pophtml = '<div style="padding:20px 10px 20px 20px ;"><lable>' + popMessage + '</lablel><p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="' + funcYES + '">Yes</a><a id="btn-cancel" onclick="' + funcNO + '" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">No</a></p></div>';
        
        $('#jnc-popup3').html(pophtml);
        $('#jnc-popup3').popup('open');
        showAlert = false;
    }
    else if (sign_qty > 1 && ordertype == 'Remove Sign') {
        var func = 'completedJobView(' + sign_type + ',' + signid + ',\'' + signtype + '\',' + run + ', ' + flagHolder + ',' + solar + ',' + solarQty + ', ' + brochureHolder + ',' + floodlight + ',' + floodlightQty + ',' + isrequired + ',\'' + RoofJobFee + '\', ' + standard_overlay + ', ' + auction_overlay + ', ' + sign_qty + ',' + itemSignType + ',' + showInstruction + ',\'' + sign_legcolour + '\')';
        
        var popMessage = "Please confirm that " + sign_qty + " of signs has been picked up, as more than 1 sign was ordered. Ensure you have retrieved all the signs ordered.";
        
        var pophtml = '<div style="padding:20px 10px 20px 20px ;"><lable>' + popMessage + '</lablel><p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="' + func + '">Yes</a><a id="btn-cancel" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">No</a></p></div>';
        $('#jnc-popup3').html(pophtml);
        $('#jnc-popup3').popup('open');
        showAlert = false;
    }
    else {
        completedJobView(sign_type, signid, signtype, run, flagHolder, solar, solarQty, brochureHolder, floodlight, floodlightQty, isrequired, RoofJobFee, standard_overlay, auction_overlay, sign_qty, itemSignType, showInstruction, sign_legcolour);
    }
}

function showtructionPopup(sign_type, signid, run, signtype, standard_overlay, auction_overlay, selectedoption, showInstruction, sign_legcolour) {
    
    if (showInstruction == true || showInstruction == 'true') {
        
        var pophtml = '<div style="padding:20px 10px 20px 20px ;"><lable>' + window.localStorage.getItem("instructionText") + '</lablel><p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="completedInstall(' + sign_type + ',' + signid + ', ' + run + ',\'' + signtype + '\', ' + standard_overlay + ', ' + auction_overlay + ')">Yes</a><a id="btn-cancel" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete"  onclick="closeInstructionCompletePopupNo()" >No</a></p></div>';
        
        $('#jnc-popup').html(pophtml);
        $('#jnc-popup').popup('open');
        
    } else {
        completedInstall(sign_type, signid, run, signtype, standard_overlay, auction_overlay, selectedoption);
    }
    
}


function closeInstructionCompletePopupNo() {
    $('#jnc-popup5').popup('close');
}


function completedJobView(sign_type, signid, signtype, run, flagHolder, solar, solarQty, brochureHolder, floodlight, floodlightQty, isrequired, RoofJobFee, standard_overlay, auction_overlay, sign_qty, itemSignType, showInstruction, sign_legcolour, selectedoption) {
    
    console.log(sign_type + ',' + signid + ',' + signtype + ',' + run + ',' + flagHolder + ',' + solar + ',' + solarQty + ',' + brochureHolder + ',' + floodlight + ',' + floodlightQty + ',' + isrequired + ',' + RoofJobFee + ',' + standard_overlay + ',' + auction_overlay + ',' + sign_qty + ',' + itemSignType + ',' + selectedoption);
    // alert(selectedoption);
    var signaddress = $('#sign-address').html();
    
    var smallImg;
    
    $.ajax({
           type: 'post',
           url: remoteUrl + 'getSignImages',
           data: {
           signid: signid,
           signtype: signtype,
           sign_type: sign_type
           },
           cache: false,
           async: false,
           beforeSend: function () {
           $.mobile.loading('show');
           },
           complete: function () {
           $.mobile.loading('hide');
           },
           success: function (result) {
           if (result) {
           result = JSON.parse(result);
           var img = '';
           $.each(result, function (i, item) {
                  img = img + '<span style="margin-right: 10px;" onclick="deletePopup(\'' + item + '\')"><img src="' + item + '" height=100 width=120></span>';
                  });
           smallImg = img;
           } else {
           smallImg = '';
           }
           //// alert(sign_type);
           //alert('bye');
           // console.log(selectedoption);
           // alert(signtype);
           
           if (signtype == 'Pending Delivery') {
           if (flagHolder == 1 || solar != 0 || brochureHolder == 1 || floodlight != 0 || sign_legcolour != null || sign_legcolour != "") {
           // alert('bye3');
           var func = 'flagHolderPopup(' + sign_type + ',' + signid + ',\'' + signtype + '\',' + run + ', ' + flagHolder + ',' + solar + ',' + solarQty + ',' + brochureHolder + ',' + floodlight + ',' + floodlightQty + ', ' + standard_overlay + ', ' + auction_overlay + ',' + showInstruction + ',\'' + sign_legcolour + '\')';
           } else {
           //alert('bye4');
           var func = 'showtructionPopup(' + sign_type + ',' + signid + ',' + run + ',\'' + signtype + '\',' + standard_overlay + ',' + auction_overlay + ',\'' + selectedoption + '\'' + ',' + showInstruction + ',\'' + sign_legcolour + '\')';
           }
           
           // alert(func);
           // alert(itemSignType);
           var InstallationTypeText = '';
           var ChoosePicture = '';
           
           if ((itemSignType == 99 || itemSignType == 2) && selectedoption == 'YES') {
           InstallationTypeText = '';
           ChoosePicture = '';
           } else {
           // alert(1);
           
           // <a onclick="" class="ui-btn ui-shadow ui-corner-all ui-btn-inline" name="photos" value="" data-theme="a" style="width: 43%;pointer-events: //none;cursor: default;">Capture Photos</a>
           InstallationTypeText = '<label for="ut" >Installation Type:</label><select name="installation-type" id="ut" value="" data-theme="a"><option value= "1" >Normal</option><option value= "2">Roof Job(+$' + RoofJobFee + ')</option><option value= "3">Sign delivered to office</option></select>';
           
           ChoosePicture = '<label>Chose picture(s) to email Agent: </label><a onclick="capturePhoto()" class="ui-btn ui-shadow ui-corner-all ui-btn-inline" name="photos" value="" data-theme="a" style="width: 43%;">Capture Photos</a><a onclick="getPhotos()" class="ui-btn ui-shadow ui-corner-all ui-btn-inline" name="photos" value="" data-theme="a" style="width: 43%; float: right">Use Album Photos</a>';
           
           }
           
           // alert('11');
           var html = '';
           
           if (selectedoption == 'YES') {
           //alert(5);
           var signInstalledLabel = '';
           if (itemSignType == 99 && selectedoption == 'YES') {
           signInstalledLabel = '<p>Product Delivered</p>';
           showInstruction = false;
           }
           else {
           signInstalledLabel = '<p>Sign Installed</p>';
           }
           
           
           html = '<input type="hidden" name="signid" id="jc-signid" value="' + signid + '"><input type="hidden" name="sign_type" id="jc-sign_type" value="' + sign_type + '"><input type="hidden" name="signtype" id="jc-signtype" value="' + signtype + '"><span style="padding-top: 15px; padding-right:5px;"><img src="img/icons/OrderComplete_Small.png" width=40 height=40></span><span style="font-size:20px;">' + signaddress + ' : Completed</span></span><hr><br/>' + InstallationTypeText + ChoosePicture + '<div id="smallImage">' + smallImg + '</div><br><br><div style="text-align: center;"> <a href="#" class="ui-btn ui-btn-inline  ui-corner-all" id="btn-signinstalled" onclick="' + func + '" style="width:140px; height:200px;word-wrap: break-word !important; white-space: normal !important;"><img src="img/icons/SignInstalled.png" width=100 height=100>' + signInstalledLabel + '</a><a href="#" class="ui-btn ui-btn-inline  ui-corner-all"  onclick="notcompletedPopup(' + sign_type + ',' + signid + ',' + run + ')" style="width:140px;word-wrap: break-word !important; white-space: normal !important;"><img src="img/icons/SignNotInstalled.png" width=100 height=100><p >Unable to complete job today</p></a></div>';
           
           /*html = '<input type="hidden" name="signid" id="jc-signid" value="' + signid + '"><input type="hidden" name="sign_type" id="jc-sign_type" value="' + sign_type + '"><input type="hidden" name="signtype" id="jc-signtype" value="' + signtype + '"><span style="padding-top: 15px; padding-right:5px;"><img src="img/icons/OrderComplete_Small.png" width=40 height=40></span><span style="font-size:20px;">' + signaddress + ' : Completed</span></span><hr><br/><label for="ut" >Installation Type:</label><select name="installation-type" id="ut" value="" data-theme="a"><option value= "1" >Normal</option><option value= "2">Roof Job(+$'+ RoofJobFee +')</option><option value= "3">Sign delivered to office</option></select><label>Chose picture(s) to email Agent: </label><a onclick="" class="ui-btn ui-shadow ui-corner-all ui-btn-inline" name="photos" value="" data-theme="a" style="width: 43%;pointer-events: none;cursor: default;">Capture Photos</a><a onclick="" class="ui-btn ui-shadow ui-corner-all ui-btn-inline" name="photos" value="" data-theme="a" style="width: 43%; float: right;pointer-events: none;cursor: default;">Use Album Photos</a><div id="smallImage">' + smallImg + '</div><br><br><div style="text-align: center;"> <a href="#" class="ui-btn ui-btn-inline  ui-corner-all" id="btn-signinstalled" onclick="' + func + '" style="width:140px; height:200px"><img src="img/icons/SignInstalled.png" width=100 height=100><p>Sign Installed</p></a><a href="#" class="ui-btn ui-btn-inline  ui-corner-all"  onclick="notcompletedPopup(' + sign_type + ',' + signid + ',' + run + ')" style="width:140px;word-wrap: break-word !important; white-space: normal !important;"><img src="img/icons/SignNotInstalled.png" width=100 height=100><p >Unable to complete job today</p></a></div>'; */
           
           window.localStorage.setItem("selectedoptionb", 'YES');
           
           }
           else {
           // alert(6);
           var signInstalledLabel = '';
           
           if (itemSignType == 99 && selectedoption == 'YES') {
           signInstalledLabel = '<p>Product Delivered</p>';
           showInstruction = false;
           }
           else {
           signInstalledLabel = '<p>Sign Installed</p>';
           }
           
           html = '<input type="hidden" name="signid" id="jc-signid" value="' + signid + '"><input type="hidden" name="sign_type" id="jc-sign_type" value="' + sign_type + '"><input type="hidden" name="signtype" id="jc-signtype" value="' + signtype + '"><span style="padding-top: 15px; padding-right:5px;"><img src="img/icons/OrderComplete_Small.png" width=40 height=40></span><span style="font-size:20px;">' + signaddress + ' : Completed</span></span><hr><br/>' + InstallationTypeText + ChoosePicture + '<div id="smallImage">' + smallImg + '</div><br><br><div style="text-align: center;"> <a href="#" class="ui-btn ui-btn-inline  ui-corner-all" id="btn-signinstalled" onclick="' + func + '" style="width:140px; height:200px;word-wrap: break-word !important; white-space: normal !important;"><img src="img/icons/SignInstalled.png" width=100 height=100>' + signInstalledLabel + '</a><a href="#" class="ui-btn ui-btn-inline  ui-corner-all"  onclick="notcompletedPopup(' + sign_type + ',' + signid + ',' + run + ')" style="width:140px;word-wrap: break-word !important; white-space: normal !important;"><img src="img/icons/SignNotInstalled.png" width=100 height=100><p >Unable to complete job today</p></a></div>';
           
           /*  html = '<input type="hidden" name="signid" id="jc-signid" value="' + signid + '"><input type="hidden" name="sign_type" id="jc-sign_type" value="' + sign_type + '"><input type="hidden" name="signtype" id="jc-signtype" value="' + signtype + '"><span style="padding-top: 15px; padding-right:5px;"><img src="img/icons/OrderComplete_Small.png" width=40 height=40></span><span style="font-size:20px;">' + signaddress + ' : Completed</span></span><hr><br/><label for="ut" >Installation Type:</label><select name="installation-type" id="ut" value="" data-theme="a"><option value= "1" >Normal</option><option value= "2">Roof Job(+$'+ RoofJobFee +')</option><option value= "3">Sign delivered to office</option></select><label>Chose picture(s) to email Agent: </label><a onclick="capturePhoto()" class="ui-btn ui-shadow ui-corner-all ui-btn-inline" name="photos" value="" data-theme="a" style="width: 43%;">Capture Photos</a><a onclick="getPhotos()" class="ui-btn ui-shadow ui-corner-all ui-btn-inline" name="photos" value="" data-theme="a" style="width: 43%; float: right">Use Album Photos</a><div id="smallImage">' + smallImg + '</div><br><br><div style="text-align: center;"> <a href="#" class="ui-btn ui-btn-inline  ui-corner-all" id="btn-signinstalled" onclick="' + func + '" style="width:140px; height:200px"><img src="img/icons/SignInstalled.png" width=100 height=100><p>Sign Installed</p></a><a href="#" class="ui-btn ui-btn-inline  ui-corner-all"  onclick="notcompletedPopup(' + sign_type + ',' + signid + ',' + run + ')" style="width:140px;word-wrap: break-word !important; white-space: normal !important;"><img src="img/icons/SignNotInstalled.png" width=100 height=100><p >Unable to complete job today</p></a></div>'; */
           
           }
           
           
           } else if (signtype == 'Remove Sign') {
           //alert(3);
           var dropOtion = '';
           if (window.localStorage.getItem("removalprompt") == "1" || window.localStorage.getItem("removalprompt") == 1) {
           dropOtion = '<option>-- Select Option --</option> <option>Sign was retrieved successfully</option><option>Sign Missing, home owner not aware of location</option><option>Sign Missing, no one home, card left</option><option>Sign Missing, agent unaware of location</option><option>Sign Missing, no one knows location</option>';
           } else {
           dropOtion = '<option>Sign was retrieved successfully</option><option>Sign Missing, home owner not aware of location</option><option>Sign Missing, no one home, card left</option><option>Sign Missing, agent unaware of location</option><option>Sign Missing, no one knows location</option>';
           }
           
           var html = '<input type="hidden" name="signid" id="jc-signid" value="' + signid + '"><input type="hidden" name="sign_type" id="jc-sign_type" value="' + sign_type + '"><input type="hidden" name="signtype" id="jc-signtype" value="' + signtype + '"><span style="padding-top: 15px; padding-right:5px;"><img src="img/icons/OrderComplete_Small.png" width=40 height=40></span><span style="font-size:20px;">' + signaddress + ' : Completed</span></span><hr><br/><label for="ut" >Sign Retrival:</label><select name="installation-type" id="ut" value="" data-theme="a">' + dropOtion + '</select><label>Chose picture(s) to email Agent: </label><a onclick="capturePhoto()" class="ui-btn ui-shadow ui-corner-all ui-btn-inline" name="photos" value="" data-theme="a" style="width: 43%;">Capture Photo</a><a onclick="getPhotos()" class="ui-btn ui-shadow ui-corner-all ui-btn-inline" name="photos" value="" data-theme="a" style="width: 43%; float: right">Use Album Photos</a><div id="smallImage">' + smallImg + '</div><div style="text-align: center;"> <a href="#" class="ui-btn ui-btn-inline  ui-corner-all" id="btn-signinstalled" onclick="instockAlert(' + sign_type + ',' + signid + ',' + run + ',\'' + signtype + '\',\'' + encodeURIComponent(signaddress).replace(/'/g, "%27") + '\', ' + isrequired + ',' + showInstruction + ',\'' + sign_legcolour + '\')"><img src="img/icons/SignRemoved.png" width=100 height=100><p>Sign Removed</p></a><a href="#" class="ui-btn ui-btn-inline  ui-corner-all" id="btn-notinstalled" onclick="notcompletedPopup(' + sign_type + ',' + signid + ',' + run + ')"><img src="img/icons/SignNotRemoved.png" width=100 height=100><p>Not Removed</p></a></div>';
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         } else if (signtype == 'Overlay Artwork Pending Delivery') {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         var html = '<input type="hidden" name="signid" id="jc-signid" value="' + signid + '"><input type="hidden" name="sign_type" id="jc-sign_type" value="' + sign_type + '"><input type="hidden" name="signtype" id="jc-signtype" value="' + signtype + '"><span style="padding-top: 15px; padding-right:5px;"><img src="img/icons/OrderComplete_Small.png" width=40 height=40></span><span style="font-size:20px;">' + signaddress + ' : Completed</span></span><hr><br/><label>Chose picture(s) to email Agent: </label><a onclick="capturePhoto()" class="ui-btn ui-shadow ui-corner-all ui-btn-inline" name="photos" value="" data-theme="a" style="width: 43%;">Capture Photo</a><a onclick="getPhotos()" class="ui-btn ui-shadow ui-corner-all ui-btn-inline" name="photos" value="" data-theme="a" style="width: 43%; float: right">Use Album Photos</a><div id="smallImage">' + smallImg + '</div><div style="text-align: center;"> <a href="#" class="ui-btn ui-btn-inline  ui-corner-all" id="btn-signinstalled" onclick="showtructionPopup(' + sign_type + ',' + signid + ',' + run + ',\'' + signtype + '\',' + standard_overlay + ',' + auction_overlay + ',\'' + selectedoption + '\'' + ',' + showInstruction + ',\'' + sign_legcolour + '\')" style="width:140px; height:200px; word-wrap: break-word !important; white-space: normal !important;"><img src="img/icons/OverlayInstalled.png" width=100 height=100><p>Overlay Installed</p></a><a href="#" class="ui-btn ui-btn-inline  ui-corner-all" id="btn-notinstalled" onclick="notcompletedPopup(' + sign_type + ',' + signid + ',' + run + ')" style="width:140px;word-wrap: break-word !important; white-space: normal !important;"><img src="img/icons/OverlayNotInstalled.png" width=100 height=100><p>Unable to complete job today</p></a></div>';
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         } else {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         var html = '<input type="hidden" name="signid" id="jc-signid" value="' + signid + '"><input type="hidden" name="sign_type" id="jc-sign_type" value="' + sign_type + '"><input type="hidden" name="signtype" id="jc-signtype" value="' + signtype + '"><span style="padding-top: 15px; padding-right:5px;"><img src="img/icons/OrderComplete_Small.png" width=40 height=40></span><span style="font-size:20px;">' + signaddress + ' : Completed</span></span><hr><br/><label>Chose picture(s) to email Agent: </label><a onclick="capturePhoto()" class="ui-btn ui-shadow ui-corner-all ui-btn-inline " name="photos" value="" data-theme="a" style="width: 43%;">Capture Photo</a><a onclick="getPhotos()" class="ui-btn ui-shadow ui-corner-all ui-btn-inline" name="photos" value="" data-theme="a" style="width: 43%; float: right">Use Album Photos</a><div id="smallImage">' + smallImg + '</div><div style="text-align: center;"> <a href="#" class="ui-btn ui-btn-inline  ui-corner-all" id="btn-signinstalled" onclick="showtructionPopup(' + sign_type + ',' + signid + ',' + run + ',\'' + signtype + '\',' + standard_overlay + ',' + auction_overlay + ',\'' + selectedoption + '\'' + ',' + showInstruction + ',\'' + sign_legcolour + '\')" style="width:140px; height:200px"><img src="img/icons/SignDoctor.png" width=100 height=100><p>SD Completed</p></a><a href="#" class="ui-btn ui-btn-inline  ui-corner-all" id="btn-notinstalled" onclick="notcompletedPopup(' + sign_type + ',' + signid + ',' + run + ')" style="width:140px;word-wrap: break-word !important; white-space: normal !important;"><img src="img/icons/SignNotDoctored.png" width=100 height=100><p>Unable to complete job today</p></a><a href="#" class="ui-btn ui-btn-inline  ui-corner-all" id="btn-signDocReprint" onclick="showtructionPopup(' + sign_type + ',' + signid + ',' + run + ',\'' + signtype + '\',' + standard_overlay + ',' + auction_overlay + ',\'' + 'SDReprint' + '\'' + ',' + showInstruction + ',\'' + sign_legcolour + '\')" style="width:140px;word-wrap: break-word !important; white-space: normal !important;"><img src="img/icons/SignDoctor.png" width=100 height=100><p>Sign Doctor Re-print Required</p></a></div>';
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         $('#jobcompleted-form').html(html);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         $('#jobcompleted-form').trigger('create');
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         //$( "#jobcompleted-page" ).popup( "open");
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         $('.version').html(version);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         $(':mobile-pagecontainer').pagecontainer('change', "#jobcompleted-page", {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  transition: "pop"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  });
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         if (selectedoption == 'YES') {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         setTimeout(function () {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    //alert('hai');
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    signpadpopup();
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    window.localStorage.removeItem("selectedoptionb");
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    }, 500);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         //Window.localStorage.setItem("selectedoptionb",'YES');
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         },
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         error: function (jqXHR, textStatus, errorThrown) {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         //alert('Network error has occurred please try again!');//5-
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         });
           
           }
           //pagecontainerbeforechange  pagecontainershow
           
    /*   $(document).on("pagecontainershow", function (e, ui) {
     var activePage = $.mobile.pageContainer.pagecontainer("getActivePage");
     
     // var use//age.getItem("userid1");
     //alert(userid
     //console.log(ui);
     alert(activePage[0].id );
     
     if (activePage[0].id == "jobcompleted-page") {
     
     var selectedopt = window.localStorage.getItem("selectedoptionb");
     
     if(selectedopt == 'YES')
     {
     
     //alert(1);
     //window.localStorage.setItem("selectedoptionb",'NO');
     signpadpopup();
     window.localStorage.removeItem("selectedoptionb");
     // $("#popup1").popup("open")
     }
     }
     
     });*/
           
           
           
           
           function signpadpopup() {
           
           /*  $('#jnc-popup').on({
            popupbeforeposition: function () {
            $('.ui-popup-screen').off();
            }
            });*/
           
           window.localStorage.setItem("signName", '');
           window.localStorage.setItem("signatureFileName", '');
           window.localStorage.setItem("IsSignatureCompleted", '');
           
           //geetha signaturepad
           var pophtml = '<form id="frm-nc"><div><lable style="font-size:14px;word-wrap: break-word !important; white-space: normal !important;">Please enter the name of the person you have delivered this to</lablel><p><textarea style="min-height: 50px; min-width: 500px; " name="signaturename" id="signaturename" ></textarea></p><label style="font-size:14px;word-wrap: break-word !important; white-space: normal !important;">Please draw your signature to confirm you have received this item</label><div id="signature-pad" class="m-signature-pad"><div class="m-signature-pad--body"><canvas style="width:500px;height:350px;backgroung-color:lightgray;" ></canvas></div><div class="m-signature-pad--footer"><div class="description">Sign above</div><div class="left"><button type="button" class="button clear" data-action="clear">Clear</button></div><div class="right"><button type="button" data-action="save-png" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" >Complete Delivery</button></div></div></div></div> <img id="imagen_firma" style="display:none;" >  </div><div class="errorBlk">Name and signature are mandatory.</div></div></form>';
           
           $('#jnc-popup').html(pophtml);
           //$('#jobcompleted-page').trigger( "pagecreate" );
           $('#jnc-popup').popup('open');
           
           //*********** onclick="completeSignPad()"
           
           var wrapper = document.getElementById("signature-pad"),
           clearButton = wrapper.querySelector("[data-action=clear]"),
           savePNGButton = wrapper.querySelector("[data-action=save-png]"),
           saveSVGButton = wrapper.querySelector("[data-action=save-svg]"),
           canvas = wrapper.querySelector("canvas"),
           signaturePad;
           
           //closePNGButton = wrapper.querySelector("[data-action=close-png]"),
           //<button type="button" data-action="close-png" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" >Close</button>
           
           // Adjust canvas coordinate space taking into account pixel ratio,
           // to make it look crisp on mobile devices.
           // This also causes canvas to be cleared.
           function resizeCanvas() {
           // When zoomed out to less than 100%, for some very strange reason,
           // some browsers report devicePixelRatio as less than 1
           // and only part of the canvas is cleared then.
           var ratio = Math.max(window.devicePixelRatio || 1, 1);
           canvas.width = canvas.offsetWidth * ratio;
           canvas.height = canvas.offsetHeight * ratio;
           canvas.getContext("2d").scale(ratio, ratio);
           }
           
           window.onresize = resizeCanvas;
           resizeCanvas();
           
           // signaturePad = new SignaturePad(canvas);
           /* var signaturePad = new SignaturePad(canvas, {
            backgroundColor: 'rgba(255, 255, 255, 0)',
            penColor: 'rgb(0, 0, 0)'
            }); backgroundColor: 'rgb(255,255,255)' */
           
           var signaturePad = new SignaturePad(canvas, {
                                               backgroundColor: 'lightgray'
                                               });
           
           clearButton.addEventListener("click", function (event) {
                                        signaturePad.clear();
                                        });
           
           /*  closePNGButton.addEventListener("click", function (event) {
            signaturePad.clear();
            $('#jnc-popup').popup('close');
            }); */
           
           savePNGButton.addEventListener("click", function (event) {
                                          //  alert('hai');
                                          if (signaturePad.isEmpty()) {
                                          alert("Please provide signature first.");
                                          } else {
                                          //  var dataUrl = signaturePad.toDataURL();
                                          var sid = $('#jc-signid').val();
                                          // '\includes\signaturepad.php
                                          var dataUrl = signaturePad.toDataURL();
                                          // document.getElementById('imagen_firma').src = dataUrl;
                                          var imagen = dataUrl.replace(/^data:image\/(png|jpg);base64,/, "");
                                          /* $.ajax({
                                           url: remoteUrl + 'savesignature',
                                           type: 'POST',
                                           data: {
                                           signid: sid,
                                           imageData: imagen
                                           },
                                           })
                                           .done(function(msg) {
                                           // Image saved successfuly.
                                           alert("success: " + msg);
                                           //  document.getElementById("my_form").submit(); //I do this to save other information.
                                           })
                                           .fail(function(msg) {
                                           alert("error: " + msg);
                                           }); */
                                          
                                          var reasonTxt = '';
                                          $(".errorBlk").css("display", "none");
                                          reasonTxt = $('#signaturename').val();
                                          
                                          // var signature = window.localStorage.getItem("signatureFileName");
                                          // alert(signature);
                                          
                                          if (reasonTxt.length > 3 && reasonTxt != '' && imagen.length > 3) {
                                          window.localStorage.setItem("signName", reasonTxt);
                                          
                                          navigator.geolocation.getCurrentPosition(function (position) {
                                                                                   var crd = position.coords;
                                                                                   completeSignPad(sid, imagen, crd.latitude, crd.longitude, gOrdertype);
                                                                                   
                                                                                   }, function (err) {
                                                                                   completeSignPad(sid, imagen, '', '', gOrdertype);
                                                                                   }, {
                                                                                   enableHighAccuracy: true,
                                                                                   timeout: 20000,
                                                                                   maximumAge: 60000
                                                                                   });
                                          
                                          //completeSignPad(sid, imagen);
                                          
                                          // window.localStorage.setItem("signImage",'test.png');
                                          //   $('#jnc-popup').popup('close');
                                          //status = (status == 'Out') ? 'In' : 'Out';
                                          // insertTimeClock(timestatus, id, userid, status, lat, lng, reasonTxt);
                                          
                                          
                                          /*$.ajax({
                                           type: 'post',
                                           url: remoteUrl + 'savesignature',
                                           data:  {
                                           signid: sid,
                                           imgdata: imagen
                                           },
                                           cache: false,
                                           beforeSend: function () {
                                           $.mobile.loading('show');
                                           },
                                           complete: function () {
                                           $.mobile.loading('hide');
                                           },
                                           success: function (result) {
                                           // alert('success1');
                                           $.mobile.loading('hide');
                                           console.log(result);
                                           
                                           var results = JSON.parse(result);
                                           console.log(results);
                                           if(results.signature_filename != '')
                                           {
                                           //alert(results.signature_filename);
                                           window.localStorage.setItem("signatureFileName", resultssignature_filename);
                                           
                                           //$('#jnc-popup').popup('close');
                                           }
                                           
                                           
                                           },
                                           error: function (jqXHR, textStatus, errorThrown) {
                                           alert('Network error has occurred please try again!');
                                           }
                                           }); */
                                          
                                          } else {
                                          $(".errorBlk").css("display", "block");
                                          }
                                          
                                          /*    // var image = signaturePad.toDataURL("image/png").replace("image/png", "image/octet-stream");
                                           // here is the most important part because if you dont replace you will get a DOM 18 exception.
                                           var imagen = dataUrl.replace(/^data:image\/(png|jpg);base64,/, "");
                                           
                                           // alert(window.location.href);
                                           // window.location.href=image; // it will save locally
                                           //console.log(window.location.href);
                                           //$.mobile.loading('show');
                                           
                                           //  var reader = new FileReader();
                                           //   reader.readAsDataURL(image);
                                           //  console.log(reader);
                                           
                                           var options = new FileUploadOptions();
                                           options.fileKey = "file";
                                           options.fileName = "Sample.png";
                                           options.mimeType = "image/png";
                                           
                                           var sid = $('#jc-signid').val();
                                           alert(sid);
                                           var stype = $('#jc-signtype').val();
                                           var s_type = $('#jc-sign_type').val();
                                           
                                           var params = new Object();
                                           params.signid = sid;
                                           //  params.signtype = stype;
                                           // params.sign_type = s_type;
                                           
                                           options.params = params;
                                           options.chunkedMode = false;
                                           var url = remoteUrl + 'savesignature';
                                           
                                           var ft = new FileTransfer();
                                           // imgURI = signaturePad.toDataURL("image/png");
                                           
                                           ft.upload(signaturePad.toDataURL("image/png"), url, win, fail, options);
                                           
                                           //camera roll
                                           // var canvas = document.getElementById("my-canvas"), ctx = canvas.getContext("2d");
                                           // draw to canvas...
                                           
                                           var wrapper1 = document.getElementById("signature-pad");
                                           var canvas1 = wrapper1.querySelector("canvas")     ;
                                           alert('hai');
                                           canvas1.toBlob(function(blob) {
                                           alert('s');
                                           saveAs(blob, "prettyimage.png");
                                           
                                           });*/
                                          //  alert('success');
                                          //  var fs = require('fs');
                                          
                                          // var dataUrl = signaturePad.toDataURL("image/jpeg", 100);
                                          //console.log('****');
                                          // alert(dataUrl);
                                          
                                          //  document.getElementById('imagen_firma').src = dataUrl;
                                          // var imagen = dataUrl.replace(/^data:image\/(png|jpg);base64,/, "");
                                          // window.localStorage.setItem("signature", imagen);
                                          // completeSignPad();
                                          
                                          // console.log(imagen);
                                          // alert(imagen);
                                          // window.open(signaturePad.toDataURL());
                                          }
                                          });
           
           saveSVGButton.addEventListener("click", function (event) {
                                          if (signaturePad.isEmpty()) {
                                          alert("Please provide signature first.");
                                          } else {
                                          window.open(signaturePad.toDataURL('image/svg+xml'));
                                          }
                                          });
           //**************************************************************************************
           /*   var win = function (r) {
            console.log("Code = " + r.responseCode);
            console.log("Response = " + r.response);
            console.log("Sent = " + r.bytesSent);
            }
            
            var fail = function (error) {
            alert("An error has occurred: Code = " + error.code);
            console.log("upload error source " + error.source);
            console.log("upload error target " + error.target);
            }*/
           
           }
           
           function completeSignPad(sid, imagen, latitude, longitude, Ordertype) {
           var signtype = $('#jc-sign_type').val();
           // alert(sid+','+signtype+','+imagen);
           // console.log(remoteUrl + 'savesignature');
           // var remoteUrlhome ="http://realestate.digitalcentral.com.au/dcinstaller_beta/index.php/home/";
           
           $.ajax({
                  type: 'post',
                  url: remoteUrl + 'savesignature',
                  data: {
                  signid: sid,
                  sign_type: signtype,
                  imgdata: imagen,
                  latitude: latitude,
                  longitude: longitude,
                  Ordertype: Ordertype
                  },
                  cache: false,
                  beforeSend: function () {
                  $.mobile.loading('show');
                  },
                  complete: function () {
                  $.mobile.loading('hide');
                  },
                  success: function (result) {
                  // alert('success');
                  $.mobile.loading('hide');
                  console.log(result);
                  
                  var results = JSON.parse(result);
                  console.log(results);
                  
                  if (results.signature_filename != '') {
                  deletePopClose();
                  //alert(results.signature_filename);
                  window.localStorage.setItem("signatureFileName", results.signature_filename);
                  window.localStorage.setItem("IsSignatureCompleted", "true");
                  // IsSignatureCompleted
                  //$('#jnc-popup').popup('close');
                  }
                  
                  
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                  console.log('error1:' + textStatus);
                  console.log('error2:' + errorThrown);
                  checkInterntConnection();
                  //alert('Network error has occurred please try again!');
                  }
                  });
           
           // event.preventDefault();
           // $('#jnc-popup').popup('close');
           // alert('hai');
           // event.preventDefault();
           // var userid = window.localStorage.getItem("userid");
           //var $insType = $('#ut').val();
           //  var no_of_photos = $('#signaturename').value;
           //  console.log(no_of_photos);
           //var no_of_photos = $('#signaturename span img').length;
           //console.log(userid+"===>"+$insType+"===>"+sign_type+"===>"+signid+"===>"+run+"===>"+stype)
           // if (no_of_photos == 0) {
           // alert("At least one photo must be taken.");
           //  }
           
           
           /* var reasonTxt = '';
            $(".errorBlk").css("display", "none");
            reasonTxt = $('#signaturename').val();
            
            var signature = window.localStorage.getItem("signatureFileName");
            // alert(signature);
            
            if (reasonTxt.length > 3 && reasonTxt != '' && signature.length > 3) {
            window.localStorage.setItem("signName",reasonTxt);
            window.localStorage.setItem("signImage",'test.png');
            $('#jnc-popup').popup('close');
            //status = (status == 'Out') ? 'In' : 'Out';
            // insertTimeClock(timestatus, id, userid, status, lat, lng, reasonTxt);
            
            } else {
            $(".errorBlk").css("display", "block");
            } */
           
           
           }
           
           
           //Flage holder and solar alert
           
           function flagHolderPopup(sign_type, signid, signtype, run, flagHolder, solar, solarQty, brochureHolder, floodlight, floodlightQty, standard_overlay, auction_overlay, showInstruction, sign_legcolour) {
           
           /*  $.ajax({
            type: 'post',
            url: remoteUrl + 'getjobavailability',
            data: {signid : signid, sign_type: sign_type},
            cache: false,
            beforeSend: function () {
            $.mobile.loading('show');
            },
            complete: function () {
            $.mobile.loading('hide');
            },
            success: function (result) {
            
            result = JSON.parse(result);
            // alert(result);
            // return;
            if(result == 1)
            {
            
            if (flagHolder != 0) {
            popMessage = 'Flag holder was ordered for this sign, has it been attached?';
            
            var pophtml = '<div><lable>' + popMessage + '</lablel><p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="solarPopup(' + sign_type + ',' + signid + ',\'' + signtype + '\',' + run + ',' + flagHolder + ',' + solar + ',' + solarQty + ',' + brochureHolder + ',' + floodlight + ',' + floodlightQty + ', '+ standard_overlay +', '+ auction_overlay +')">Yes</a><a id="btn-cancel" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">No</a></p></div>';
            $('#jnc-popup').html(pophtml);
            $('#jnc-popup').popup('open');
            } else {
            solarPopup(sign_type, signid, signtype, run, flagHolder, solar, solarQty, brochureHolder, floodlight, floodlightQty, standard_overlay, auction_overlay);
            }
            
            }
            else  if(result == 0)
            {
            
            alert("Its already marked as Delivered.");
            $(':mobile-pagecontainer').pagecontainer('change', "#home-page");
            homepageAjax(1);
            }
            
            },
            error: function (jqXHR, textStatus, errorThrown) {
            alert('Network error has occurred while fetching login details,please try again!');//1-
            }
            }); */
           
           if (flagHolder != 0) {
           popMessage = 'Flag holder was ordered for this sign, has it been attached?';
           
           
           var flagholderImgTag = '';
           if (window.localStorage.getItem("flagholder_image") != undefined && window.localStorage.getItem("flagholder_image") != "", window.localStorage.getItem("flagholder_image") != null)
           flagholderImgTag = '<div class="ui-block-a" style="width: 100%; text-align:center; padding:15px; "><img src="' + window.localStorage.getItem("flagholder_image") + '" width=35% ></div>';
           
           
           var pophtml = '<div><lable>' + popMessage + '</lablel> ' + flagholderImgTag + ' <p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="solarPopup(' + sign_type + ',' + signid + ',\'' + signtype + '\',' + run + ',' + flagHolder + ',' + solar + ',' + solarQty + ',' + brochureHolder + ',' + floodlight + ',' + floodlightQty + ', ' + standard_overlay + ', ' + auction_overlay + ', ' + showInstruction + ',\'' + sign_legcolour + '\')">Yes</a><a id="btn-cancel" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">No</a></p></div>';
           $('#jnc-popup').html(pophtml);
           $('#jnc-popup').popup('open');
           } else {
           solarPopup(sign_type, signid, signtype, run, flagHolder, solar, solarQty, brochureHolder, floodlight, floodlightQty, standard_overlay, auction_overlay, showInstruction, sign_legcolour);
           }
           
           }
           
           function solarPopup(sign_type, signid, signtype, run, flagHolder, solar, solarQty, brochureHolder, floodlight, floodlightQty, standard_overlay, auction_overlay, showInstruction, sign_legcolour) {
           
           if (solar != 0) {
           popMessage = solarQty + ' solar lights were ordered for this sign, have they been attached?';
           
           var pophtml = '<div><lable>' + popMessage + '</lablel><p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="brochureHolderPopup(' + sign_type + ',' + signid + ',\'' + signtype + '\',' + run + ',' + flagHolder + ',' + solar + ',' + solarQty + ',' + brochureHolder + ',' + floodlight + ',' + floodlightQty + ', ' + standard_overlay + ', ' + auction_overlay + ', ' + showInstruction + ',\'' + sign_legcolour + '\')">Yes</a><a id="btn-cancel" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">No</a></p></div>';
           $('#jnc-popup').html(pophtml);
           $('#jnc-popup').popup('open');
           } else {
           brochureHolderPopup(sign_type, signid, signtype, run, flagHolder, solar, solarQty, brochureHolder, floodlight, floodlightQty, standard_overlay, auction_overlay, showInstruction, sign_legcolour);
           }
           
           }
           
           function brochureHolderPopup(sign_type, signid, signtype, run, flagHolder, solar, solarQty, brochureHolder, floodlight, floodlightQty, standard_overlay, auction_overlay, showInstruction, sign_legcolour) {
           
           if (brochureHolder != 0) {
           popMessage = 'Brochure holder was ordered for this sign, has it been attached?';
           
           var pophtml = '<div><lable>' + popMessage + '</lablel><p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="floodlightPopup(' + sign_type + ',' + signid + ',\'' + signtype + '\',' + run + ',' + flagHolder + ',' + solar + ',' + solarQty + ',' + brochureHolder + ',' + floodlight + ',' + floodlightQty + ', ' + standard_overlay + ', ' + auction_overlay + ', ' + showInstruction + ',\'' + sign_legcolour + '\')">Yes</a><a id="btn-cancel" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">No</a></p></div>';
           $('#jnc-popup').html(pophtml);
           $('#jnc-popup').popup('open');
           } else {
           floodlightPopup(sign_type, signid, signtype, run, flagHolder, solar, solarQty, brochureHolder, floodlight, floodlightQty, standard_overlay, auction_overlay, showInstruction, sign_legcolour);
           }
           
           }
           
           function floodlightPopup(sign_type, signid, signtype, run, flagHolder, solar, solarQty, brochureHolder, floodlight, floodlightQty, standard_overlay, auction_overlay, showInstruction, sign_legcolour) {
           
           if (floodlight != 0) {
           popMessage = floodlightQty + ' flood lights were ordered for this sign, have they been attached?';
           
           var pophtml = '<div><lable>' + popMessage + '</lablel><p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="showlegcolourPopup(' + sign_type + ',' + signid + ',' + run + ',\'' + signtype + '\',' + standard_overlay + ',' + auction_overlay + ',\'' + '' + '\',' + showInstruction + ',\'' + sign_legcolour + '\')">Yes</a><a id="btn-cancel" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">No</a></p></div>';
           $('#jnc-popup').html(pophtml);
           $('#jnc-popup').popup('open');
           } else {
           showlegcolourPopup(sign_type, signid, run, signtype, standard_overlay, auction_overlay, "", showInstruction, sign_legcolour);
           }
           
           }
           
           
           //function floodlightPopup(sign_type, signid, signtype, run, flagHolder, solar, solarQty, brochureHolder, floodlight, floodlightQty, standard_overlay, auction_overlay, showInstruction, sign_legcolour) {
           
           //    if (floodlight != 0) {
           //        popMessage = floodlightQty + ' flood lights were ordered for this sign, have they been attached?';
           
           //        var pophtml = '<div><lable>' + popMessage + '</lablel><p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="showlegcolourPopup(' + sign_type + ',' + signid + ',' + run + ',\'' + signtype + '\',' + standard_overlay + ',' + auction_overlay + ',\'' + '' + '\',' + showInstruction + ',\'' + sign_legcolour + '\')">Yes</a><a id="btn-cancel" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">No</a></p></div>';
           //        $('#jnc-popup').html(pophtml);
           //        $('#jnc-popup').popup('open');
           //    } else {
           //        showlegcolourPopup(sign_type, signid, run, signtype, standard_overlay, auction_overlay, '', showInstruction, sign_legcolour);
           //    }
           
           //}
           
           
           function showlegcolourPopup(sign_type, signid, run, signtype, standard_overlay, auction_overlay, selectedoption, showInstruction, sign_legcolour) {
           
           if (sign_legcolour != null && sign_legcolour != "" && sign_legcolour != 'null') {
           
           var popMessage = sign_legcolour + ' legs were ordered for this sign, does this sign have ' + sign_legcolour + ' legs?';
           
           var pophtml = '<div><lable>' + popMessage + '</lablel><p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="showtructionPopup(' + sign_type + ',' + signid + ',' + run + ',\'' + signtype + '\',' + standard_overlay + ',' + auction_overlay + ',\'' + '' + '\',' + showInstruction + ',\'' + sign_legcolour + '\')">Yes</a><a id="btn-cancel" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">No</a></p></div>';
           
           $('#jnc-popup').html(pophtml);
           $('#jnc-popup').popup('open');
           } else {
           showtructionPopup(sign_type, signid, run, signtype, standard_overlay, auction_overlay, '', showInstruction, sign_legcolour);
           }
           }
           
           
           function instockAlert(sign_type, signid, run, stype, address, isrequired, showInstruction, sign_legcolour) {
           
           var insType = $('#ut').val();
           
           if (insType == '-- Select Option --') {
           alert("Please select sign retrival.")
           return
           }
           
           address = decodeURIComponent(address);
           
           // alert(signid +','+ sign_type );
           
           $.ajax({
                  type: 'post',
                  url: remoteUrl + 'getJobavailability',
                  data: { signid: signid, sign_type: sign_type },
                  cache: false,
                  beforeSend: function () {
                  $.mobile.loading('show');
                  },
                  complete: function () {
                  $.mobile.loading('hide');
                  },
                  success: function (result) {
                  
                  result = JSON.parse(result);
                  //alert(result);
                  if (result == 1) {
                  
                  var no_of_photos = $('#smallImage span img').length;
                  if (no_of_photos == 0 && isrequired == '1' && stype == 'Remove Sign') {
                  alert("At least one photo must be taken.");
                  } else {
                  $.ajax({
                         type: 'post',
                         url: remoteUrl + 'keepInstockAlert',
                         data: {
                         signid: signid,
                         run: run,
                         sign_type: sign_type
                         },
                         cache: false,
                         timeout: 60000,
                         beforeSend: function () {
                         $.mobile.loading('show');
                         },
                         complete: function () {
                         $.mobile.loading('hide');
                         },
                         success: function (result) {
                         
                         result = JSON.parse(result);
                         if (result.instock_alert) {
                         var pophtml = '<form id="frm-nc"><div><div data-role="header" data-theme="a"><h2>In Stock</h2></div><p>Standard sign from "' + address + ' " from "' + result.signdetails.agencyname + '"</p><p>Will this sign be kept in stock?</p><p><input type="hidden" name="signid" value="' + signid + '"><input type="hidden" name="sign_type" value="' + sign_type + '"><a id="job-notcompleted-btn" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check ui-btn-inline" onclick="keepInstock(' + sign_type + ',' + signid + ',' + run + ',\'' + stype + '\',\'yes\', ' + isrequired + ')">Yes</a><a id="job-notcompleted-btn" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete ui-btn-inline" onclick="keepInstock(' + sign_type + ',' + signid + ',' + run + ',\'' + stype + '\',\'no\', ' + isrequired + ')">No</a></p></div></form>';
                         $('#jnc-popup').html(pophtml);
                         //$('#jobcompleted-page').trigger( "pagecreate" );
                         $('#jnc-popup').popup('open');
                         } else {
                         keepInstock(sign_type, signid, run, stype, '', isrequired);
                         }
                         },
                         error: function (jqXHR, textStatus, errorThrown) {
                         //alert('Network error has occurred please try again!');//6-
                         }
                         
                         });
                  }
                  }
                  else if (result == 0) {
                  
                  alert("Its already marked as Removed.");
                  $(':mobile-pagecontainer').pagecontainer('change', "#home-page");
                  homepageAjax(1);
                  return;
                  }
                  
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                  //alert('Network error has occurred while fetching login details,please try again!');//1-
                  }
                  });
           
           /* var no_of_photos = $('#smallImage span img').length;
            if (no_of_photos == 0 && isrequired == '1' && stype == 'Remove Sign') {
            alert("At least one photo must be taken.");
            } else {
            $.ajax({
            type: 'post',
            url: remoteUrl + 'keepInstockAlert',
            data: {
            signid: signid,
            run: run,
            sign_type: sign_type
            },
            cache: false,
            timeout: 60000,
            beforeSend: function () {
            $.mobile.loading('show');
            },
            complete: function () {
            $.mobile.loading('hide');
            },
            success: function (result) {
            
            result = JSON.parse(result);
            if (result.instock_alert) {
            var pophtml = '<form id="frm-nc"><div><div data-role="header" data-theme="a"><h2>In Stock</h2></div><p>Standard sign from "' + address + ' " from "' + result.signdetails.agencyname + '"</p><p>Will this sign be kept in stock?</p><p><input type="hidden" name="signid" value="' + signid + '"><input type="hidden" name="sign_type" value="' + sign_type + '"><a id="job-notcompleted-btn" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check ui-btn-inline" onclick="keepInstock(' + sign_type + ',' + signid + ',' + run + ',\'' + stype + '\',\'yes\', ' + isrequired + ')">Yes</a><a id="job-notcompleted-btn" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete ui-btn-inline" onclick="keepInstock(' + sign_type + ',' + signid + ',' + run + ',\'' + stype + '\',\'no\', ' + isrequired + ')">No</a></p></div></form>';
            $('#jnc-popup').html(pophtml);
            //$('#jobcompleted-page').trigger( "pagecreate" );
            $('#jnc-popup').popup('open');
            } else {
            keepInstock(sign_type, signid, run, stype, '', isrequired);
            }
            },
            error: function (jqXHR, textStatus, errorThrown) {
            alert('Network error has occurred please try again!');//6-
            }
            
            });
            }*/
           }
           
           function keepInstock(sign_type, signid, run, stype, btnType, isrequired) {
           
           
           event.preventDefault();
           var userid = window.localStorage.getItem("userid");
           var insType = $('#ut').val();
           
           //alert(JSON.stringify({ userid: userid, run: run, signid: signid, instype: insType, instocked: 1, btnType: btnType, sign_type: sign_type }));
           //console.log(JSON.stringify({ userid: userid, run: run, signid: signid, instype: insType, instocked: 1, btnType: btnType, sign_type: sign_type }));
           
           $.ajax({
                  type: 'post',
                  url: remoteUrl + 'keepInstock',
                  data: {
                  userid: userid,
                  run: run,
                  signid: signid,
                  instype: insType,
                  instocked: 1,
                  btnType: btnType,
                  sign_type: sign_type
                  },
                  cache: false,
                  timeout: 60000,
                  beforeSend: function () {
                  $.mobile.loading('show');
                  },
                  complete: function () {
                  $.mobile.loading('hide');
                  },
                  success: function (result) {
                  // console.log(JSON.stringify(result));
                  result = JSON.parse(result);
                  var signid = result.signid;
                  var sign_type = result.sign_type;
                  if (signid && sign_type) {
                  jobview(sign_type, signid);
                  } else {
                  $(':mobile-pagecontainer').pagecontainer('change', "#home-page");
                  homepageAjax(4);
                  }
                  
                  checkConnectionCalls = 0;
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                  checkInterntConnection();
                  //alert('Network error has occurred please try again!');//7-
                  }
                  });
           }
           
           function auctionOverlayCall(userid, run, signid, instype, sign_type, standard_overlay, auction_overlay) {
           if (auction_overlay == 1) {
           popMessage = 'An AUCTION overlay was ordered for this sign, has it been applied?';
           var pophtml = '<div><label>' + popMessage + '</label><p><a id="auction-yes" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="standardOverlayCall(' + userid + ', ' + run + ', ' + signid + ', ' + instype + ', ' + sign_type + ', \'yes\', ' + standard_overlay + ', ' + auction_overlay + ')">Yes</a><a id="auction-no" onclick="standardOverlayCall(' + userid + ', ' + run + ', ' + signid + ', ' + instype + ', ' + sign_type + ', \'no\', ' + standard_overlay + ', ' + auction_overlay + ')" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">No</a></p></div>';
           $('#jnc-popup').html(pophtml);
           $('#jnc-popup').popup('open');
           } else {
           standardOverlayCall(userid, run, signid, instype, sign_type, '', standard_overlay, auction_overlay);
           }
           }
           
           function standardOverlayCall(userid, run, signid, instype, sign_type, auctionStatus, standard_overlay, auction_overlay) {
           if (standard_overlay == 1) {
           popMessage = 'An Additional/Other overlay was ordered for this sign, has it been applied?';
           var pophtml = '<div><label>' + popMessage + '</label><p><a id="auction-yes" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="finishSignInstallation(' + userid + ', ' + run + ', ' + signid + ', ' + instype + ', ' + sign_type + ', \'' + auctionStatus + '\', \'yes\')">Yes</a><a id="auction-no" onclick="finishSignInstallation(' + userid + ', ' + run + ', ' + signid + ', ' + instype + ', ' + sign_type + ', \'' + auctionStatus + '\', \'no\')" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">No</a></p></div>';
           $('#jnc-popup').html(pophtml);
           $('#jnc-popup').popup('open');
           } else {
           finishSignInstallation(userid, run, signid, instype, sign_type, auctionStatus, '');
           }
           }
           
           function finishSignInstallation(userid, run, signid, insType, sign_type, auctionStatus, stdOverlayStatus) {
           
           var reasonTxt = '';
           var isSDReprint = false;
           
           if (auctionStatus == 'SDReprint') {
           reasonTxt = stdOverlayStatus;
           isSDReprint = true;
           }
           
           var std_overlay = '';
           var auc_overlay = '';
           
           std_overlay = (stdOverlayStatus != '' && stdOverlayStatus == 'yes') ? 1 : 0;
           auc_overlay = (stdOverlayStatus != '' && auctionStatus == 'yes') ? 1 : 0;
           
           var signName = window.localStorage.getItem("signName");
           var signature = window.localStorage.getItem("signatureFileName");
           var uploadImageName = window.localStorage.getItem("uploadImageName");
           
           if (uploadImageName == null || uploadImageName == undefined) {
           uploadImageName = '';
           }
           
           if ((sign_type == 2 || sign_type == 99) && signName != '' && signName != null && signature != null && signature != '') {
           // alert('YES:'+userid+','+run+','+signid+','+insType+','+sign_type+','+std_overlay+','+auc_overlay+','+signName+','+signature+','+1);
           $.ajax({
                  type: 'post',
                  url: remoteUrl + 'finishSignInstallation',
                  data: {
                  userid: userid,
                  run: run,
                  signid: signid,
                  instype: insType,
                  sign_type: sign_type,
                  std_overlay: std_overlay,
                  auc_overlay: auc_overlay,
                  signature_name: signName,
                  signature_filename: signature,
                  delivery_flag: 1
                  },
                  cache: false,
                  timeout: 60000,
                  beforeSend: function () {
                  $.mobile.loading('show');
                  },
                  complete: function () {
                  $.mobile.loading('hide');
                  },
                  success: function (result) {
                  //alert('success'+result);
                  result = JSON.parse(result);
                  var signid = result.signid;
                  var sign_type = result.sign_type;
                  
                  if (signid && sign_type) {
                  jobview(sign_type, signid);
                  } else {
                  $(':mobile-pagecontainer').pagecontainer('change', "#home-page");
                  homepageAjax(5);
                  }
                  
                  checkConnectionCalls = 0;
                  
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                  checkInterntConnection();
                  //alert('Network error has occurred please try again!');//8-
                  }
                  });
           }
           else {
           
           $.ajax({
                  type: 'post',
                  url: remoteUrl + 'finishSignInstallation',
                  data: {
                  userid: userid,
                  run: run,
                  signid: signid,
                  instype: insType,
                  sign_type: sign_type,
                  std_overlay: std_overlay,
                  auc_overlay: auc_overlay,
                  signature_filename: uploadImageName,
                  signdoc_reprinttxt: reasonTxt,
                  signdoc_reprintflag: isSDReprint
                  },
                  cache: false,
                  timeout: 60000,
                  beforeSend: function () {
                  $.mobile.loading('show');
                  },
                  complete: function () {
                  $.mobile.loading('hide');
                  },
                  success: function (result) {
                  
                  result = JSON.parse(result);
                  // alert('Success:'+result);
                  var signid = result.signid;
                  var sign_type = result.sign_type;
                  
                  if (signid && sign_type) {
                  jobview(sign_type, signid);
                  } else {
                  
                  $(':mobile-pagecontainer').pagecontainer('change', "#home-page");
                  homepageAjax(5);
                  }
                  
                  checkConnectionCalls = 0;
                  
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                  checkInterntConnection();
                  //alert('Network error has occurred please try again!');//8-
                  }
                  });
           }
           }
           
           function completedInstall(sign_type, signid, run, stype, standard_overlay, auction_overlay, selectedoption) {
           
           // console.log(sign_type, signid, run, stype, standard_overlay, auction_overlay,selectedoption);
           event.preventDefault();
           var $insType = $('#ut').val();
           
           $.ajax({
                  type: 'post',
                  url: remoteUrl + 'getJobavailability',
                  data: { signid: signid, sign_type: sign_type },
                  cache: false,
                  beforeSend: function () {
                  $.mobile.loading('show');
                  },
                  complete: function () {
                  $.mobile.loading('hide');
                  },
                  success: function (result) {
                  
                  result = JSON.parse(result);
                  
                  console.log(result);
                  
                  $('#jnc-popup5').popup('close');
                  
                  if (result == 1) {
                  if ((sign_type == 2 || sign_type == 99) && selectedoption == 'YES') {
                  var reasonTxt = window.localStorage.getItem("signName");
                  
                  var IsSignatureCompleted = window.localStorage.getItem("IsSignatureCompleted");
                  
                  if (reasonTxt == null || reasonTxt.length <= 3 || IsSignatureCompleted != "true") {
                  
                  signpadpopup();
                  return;
                  }
                  }
                  
                  
                  var userid = window.localStorage.getItem("userid");
                  var optionselected = window.localStorage.getItem("selectedoptionb");
                  var $insType = $('#ut').val();
                  var no_of_photos = $('#smallImage span img').length;
                  //console.log(userid+"===>"+$insType+"===>"+sign_type+"===>"+signid+"===>"+run+"===>"+stype)
                  if (no_of_photos == 0 && stype != 'Remove Sign' && optionselected != 'YES') {
                  alert("At least one photo must be taken.");
                  } else {
                  
                  //overlay status check
                  if (stype == 'Pending Delivery' && (standard_overlay == 1 || auction_overlay == 1)) {
                  setTimeout(function () {
                             auctionOverlayCall(userid, run, signid, $insType, sign_type, standard_overlay, auction_overlay);
                             }, 500);
                  
                  } else {
                  
                  if (stype == 'Sign Doctor Request' && selectedoption == 'SDReprint') {
                  setTimeout(function () {
                             SignDocReprintPopup(userid, run, signid, $insType, sign_type, 'SDReprint', '');
                             }, 500);
                  }
                  else {
                  setTimeout(function () {
                             finishSignInstallation(userid, run, signid, $insType, sign_type, '', '');
                             }, 500);
                  
                  }
                  
                  }
                  
                  }
                  }
                  else if (result == 0) {
                  
                  if (sign_type == 99 && selectedoption == 'YES')
                  alert("Its already marked as Delivered.");
                  else
                  alert("Its already marked as Installed.");
                  
                  $(':mobile-pagecontainer').pagecontainer('change', "#home-page");
                  homepageAjax(1);
                  }
                  
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                  $('#jnc-popup5').popup('close');
                  //alert('Network error has occurred while fetching login details,please try again!');//1-
                  }
                  });
           
           /*   if((sign_type == 2 || sign_type == 99) && selectedoption == 'YES' )
            {
            var reasonTxt = window.localStorage.getItem("signName");
            //alert(reasonTxt);
            var IsSignatureCompleted = window.localStorage.getItem("IsSignatureCompleted");
            // alert(IsSignatureCompleted);
            // window.localStorage.setItem("IsSignatureCompleted", "true");
            if (reasonTxt == null || reasonTxt.length <= 3 || IsSignatureCompleted != "true") {
            //window.localStorage.setItem("signName",reasonTxt);
            signpadpopup();
            return;
            }
            }
            //return;
            
            var userid = window.localStorage.getItem("userid");
            var optionselected = window.localStorage.getItem("selectedoptionb");
            var $insType = $('#ut').val();
            var no_of_photos = $('#smallImage span img').length;
            //console.log(userid+"===>"+$insType+"===>"+sign_type+"===>"+signid+"===>"+run+"===>"+stype)
            if (no_of_photos == 0 && stype != 'Remove Sign' && optionselected != 'YES') {
            alert("At least one photo must be taken.");
            } else {
            
            //overlay status check
            if(stype == 'Pending Delivery' && (standard_overlay == 1 || auction_overlay == 1)){
            auctionOverlayCall(userid, run, signid, $insType, sign_type, standard_overlay, auction_overlay);
            }else{
            finishSignInstallation(userid, run, signid, $insType, sign_type, '', '');
            }
            
            
            }*/
           
           }
           
           function SignDocReprintPopup(userid, run, signid, insType, sign_type, auctionStatus, stdOverlayStatus) {
           
           var pophtml = '<form id="frm-nc"><div><lable>Please enter reason for re-print</lablel><p><label style="font-size:12px"><i>This comment will be added to the log, please be careful</i></label></p><p><textarea style="min-height: 150px; min-width: 200px; " name="reason" id="reason" placeholder="Write reason here"></textarea></p><div class="errorBlk">Please enter atleast 10 characters</div><p><input type="hidden" name="signid" value="' + signid + '"><input type="hidden" name="sign_type" value="' + sign_type + '"><input type="hidden" name="run" value="' + run + '"><input type="hidden" name="userid" value="' + userid + '"><a id="job-notcompleted-btn" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="callSignInstall(' + userid + ', ' + run + ', ' + signid + ', ' + insType + ', ' + sign_type + ', \'' + auctionStatus + '\',\'' + stdOverlayStatus + '\')" >Submit</a></p></div></form>';
           
           $('#jnc-popup').html(pophtml);
           $('#jnc-popup').popup('open');
           
           }
           
           function callSignInstall(userid, run, signid, insType, sign_type, auctionStatus, stdOverlayStatus) {
           var reasonTxt = '';
           $(".errorBlk").css("display", "none");
           reasonTxt = $('#reason').val();
           
           if (reasonTxt.length >= 5 && reasonTxt != '') {
           $('#jnc-popup').popup('close');
           finishSignInstallation(userid, run, signid, insType, sign_type, auctionStatus, reasonTxt);
           }
           else {
           $(".errorBlk").css("display", "block");
           return;
           }
           }
           
           
           function notcompletedPopup(sign_type, signid, run) {
           
           var insType = $('#ut').val();
           
           if (insType == '-- Select Option --') {
           alert("Please select sign retrival.")
           return
           }
           
           var userid = window.localStorage.getItem("userid");
           var pophtml = '<form id="frm-nc"><div><lable>Why the job not completed?</lablel><p><textarea style="min-height: 150px; min-width: 200px; " name="reason" id="reason" placeholder="Write reason here"></textarea></p><p><input type="hidden" name="signid" value="' + signid + '"><input type="hidden" name="sign_type" value="' + sign_type + '"><input type="hidden" name="run" value="' + run + '"><input type="hidden" name="userid" value="' + userid + '"><a id="job-notcompleted-btn" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="notCompletedInstall()">Submit</a></p></div></form>';
           $('#jnc-popup').html(pophtml);
           //$('#jobcompleted-page').trigger( "pagecreate" );
           $('#jnc-popup').popup('open');
           
           }
           
           
           function notCompletedInstall() {
           event.preventDefault();
           var userid = window.localStorage.getItem("userid");
           var reason = $('#reason').text();
           // alert(reason);
           
           if ($('#frm-nc').validate({
                                     rules: {
                                     reason: {
                                     required: true
                                     },
                                     }
                                     }).form()) {
           
           $.ajax({
                  type: 'post',
                  url: remoteUrl + 'notFinishJob',
                  data: $('#frm-nc').serialize(),
                  cache: false,
                  timeout: 60000,
                  beforeSend: function () {
                  $.mobile.loading('show');
                  },
                  complete: function () {
                  $.mobile.loading('hide');
                  },
                  success: function (result) {
                  result = JSON.parse(result);
                  var signid = result.signid;
                  var sign_type = result.sign_type;
                  if (signid && sign_type) {
                  jobview(sign_type, signid);
                  } else {
                  //$('#jnc-popup').popup('close');
                  $(':mobile-pagecontainer').pagecontainer('change', "#home-page");
                  homepageAjax(6);
                  }
                  
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                  //alert('Network error has occurred please try again!');//9-
                  }
                  });
           } else {
           return false;
           }
           }
           
           $(document).on('click', '#submit-notes', function (event) {
                          event.preventDefault();
                          
                          if ($('#frm-notes').validate({
                                                       rules: {
                                                       notes: {
                                                       required: true
                                                       },
                                                       }
                                                       }).form()) {
                          
                          $.ajax({
                                 type: 'post',
                                 url: remoteUrl + 'savenotes',
                                 data: $('#frm-notes').serialize(),
                                 cache: false,
                                 timeout: 60000,
                                 beforeSend: function () {
                                 $.mobile.loading('show');
                                 },
                                 complete: function () {
                                 $.mobile.loading('hide');
                                 },
                                 success: function (result) {
                                 $('#signnotes').popup('close');
                                 
                                 },
                                 error: function (jqXHR, textStatus, errorThrown) {
                                 //alert('Network error has occurred please try again!');//10-
                                 }
                                 });
                          
                          
                          
                          } else {
                          return false;
                          }
                          
                          });
           
           
           
           $(document).on('click', '#sign-search', function (event) {
                          event.preventDefault();
                          
                          if ($(this).closest(".frm-search").validate({
                                                                      rules: {
                                                                      slsignid: {
                                                                      required: true
                                                                      },
                                                                      }
                                                                      }).form()) {
                          
                          $.ajax({
                                 type: 'post',
                                 url: remoteUrl + 'getJobdetails',
                                 data: $(this).closest(".frm-search").serialize(),
                                 cache: false,
                                 timeout: 60000,
                                 beforeSend: function () {
                                 $.mobile.loading('show');
                                 },
                                 complete: function () {
                                 $.mobile.loading('hide');
                                 },
                                 success: function (result) {
                                 result = JSON.parse(result);
                                 var item = result.signdetails;
                                 
                                 checkConnectionCalls = 0;
                                 
                                 //var ref = window.open(inappUrl + item.signid + '/' + item.agencyid + '/' + item.orderid + '/fromapp' + (item.sign_type == '1' ? '?sign_type=dyo' : ''), '_blank', 'location=yes');
                                 
                                 if (item.sign_type == 99) {
                                 var ref = window.open(inappUrlProductOrder + item.somid + '/fromapp', '_blank', 'location=yes');
                                 }
                                 else if (item.sign_type == 1) {
                                 var ref = window.open(inappUrl + item.signid + '/' + item.agencyid + '/' + item.orderid + '/fromapp?sign_type=dyo', '_blank', 'location=yes');
                                 }
                                 else if (item.sign_type == 2) {
                                 var ref = window.open(inappUrl + item.signid + '/' + item.agencyid + '/' + item.orderid + '/fromapp?sign_type=custom', '_blank', 'location=yes');
                                 }
                                 else {
                                 var ref = window.open(inappUrl + item.signid + '/' + item.agencyid + '/' + item.orderid + '/fromapp', '_blank', 'location=yes');
                                 }
                                 
                                 },
                                 error: function (jqXHR, textStatus, errorThrown) {
                                 checkInterntConnection();
                                 //alert('Network error has occurred while fetching the job details, please try again!');//11-
                                 }
                                 });
                          
                          
                          
                          } else {
                          return false;
                          }
                          
                          });
           
           
           function showTimerClockPopup(id) {
           
           var userid = window.localStorage.getItem("userid");
           var outMessage = '';
           
           $('#jnc-popup' + id).popup('close');
           $('#timeclock' + id).popup('close');
           console.log(id + '- Close1');
           //$('#timeclock1').popup('close');
           //$('#timeclock2').popup('close');
           // $('#timeclock3').popup('close');
           
           
           $.ajax({
                  type: 'post',
                  url: remoteUrl + 'timeclock',
                  data: {
                  'userid': userid
                  },
                  cache: false,
                  timeout: 60000,
                  beforeSend: function () {
                  $.mobile.loading('show');
                  },
                  complete: function () {
                  //$.mobile.loading('hide');
                  },
                  success: function (result) {
                  result = JSON.parse(result);
                  $.mobile.loading('hide');
                  console.log(result.current_status);
                  var runsheetStartRun = "'" + result.RunsheetStartRun + "'";
                  
                  var currentStatus = "'" + result.current_status + "'";
                  //      alert(runsheetStartRun);
                  
                  var currentStatusCount = 0;
                  
                  if (result.current_status == 'In') {
                  
                  //alert('In Popup');
                  $('#timeclock' + id).popup('close');
                  console.log(id + '- Close2');
                  $('#timeclock' + id).html('');
                  
                  var outMessage = 'Out';
                  var pophtml = '<div style="padding:10px 20px;"><strong>Time Clock</strong><button type="submit" id="sam" onclick="UpdateClockStatus(2,' + id + ',' + runsheetStartRun + ',' + currentStatus + ',' + currentStatusCount + ')" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check">Clock Out</button><button type="submit" id="timeclock-startlunch" onclick="UpdateClockStatus(3,' + id + ',' + runsheetStartRun + ',' + currentStatus + ',' + currentStatusCount + ')" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check">Start Lunch</button><p><a id="btn-cancel" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">Dismiss</a></p></div>';
                  
                  console.log(pophtml);
                  
                  $('#timeclock' + id).html(pophtml);
                  $('#timeclock' + id).popup('open');
                  }
                  else if (result.current_status == 'Out') {
                  currentStatusCount = result.current_status_count;
                  // alert(id+','+runsheetStartRun+','+currentStatus);
                  $('#timeclock' + id).popup('close');
                  $('#timeclock' + id).html('');
                  console.log('before');
                  console.log(id + '- Close3');
                  //  alert(result.current_status_count);
                  /*  var pophtml='<div data-role="popup" style="padding:10px 20px;"><strong>Time Clock</strong><button type="submit" id="timeclock-in" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="UpdateClockStatus(1,'+id+','+runsheetStartRun+','+currentStatus+','+currentStatusCount+')" >Clock In</button><p><a id="btn-cancel" onclik="refreshRunsheet()"  data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">Dismiss</a></p></div>'; */
                  
                  // $('#run-list-all').append('<div style=" padding:50px 10px 50px 20px; text-align:center; vertical-align:center;">You are not Clocked in or at lunch, you cannot view the runsheet until you clock in / return from lunch</div>');
                  // alert($('#run-list-all'));
                  //console.log($('#run-list-all'));
                  var pophtml = '';
                  var isnotcheckedin = window.localStorage.getItem("isnotcheckedin");
                  var isDissmissClick = false;
                  
                  //console.log(isnotcheckedin);
                  if (isnotcheckedin == 'true') {
                  //alert(true);
                  pophtml = '<div data-role="popup" style="padding:10px 20px;"><strong>Time Clock</strong><button type="submit" id="timeclock-in" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="UpdateClockStatus(1,' + id + ',' + runsheetStartRun + ',' + currentStatus + ',' + currentStatusCount + ')" >Clock In</button><p><a id="btn-cancel"  data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">Dismiss</a></p></div>';
                  
                  }
                  else {
                  //alert(false);
                  pophtml = '<div data-role="popup" style="padding:10px 20px;"><strong>Time Clock</strong><button type="submit" id="timeclock-in" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="UpdateClockStatus(1,' + id + ',' + runsheetStartRun + ',' + currentStatus + ',' + currentStatusCount + ')" >Clock In</button><p><button type="submit" id="dismissbtn" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="dismissClick()" >Dismiss</button></p></div>';
                  
                  }
                  
                  // alert('clock:'+id);
                  $('#timeclock' + id).html(pophtml);
                  $('#timeclock' + id).popup('open');
                  console.log('after');
                  }
                  else if (result.current_status == 'LunchStart') {
                  // alert('Start Popup');
                  
                  /* var pophtml='<div style="padding:10px 20px;"><strong>Time Clock</strong><button type="submit" id="timeclock-endlunch" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="UpdateClockStatus(4,'+id+','+runsheetStartRun+','+currentStatus+','+currentStatusCount+')" >End Lunch</button><p><a id="btn-cancel"  data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">Dismiss</a></p></div>'; */
                  var pophtml = '';
                  var isnotcheckedin = window.localStorage.getItem("isnotcheckedin");
                  var isDissmissClick = false;
                  
                  //console.log(isnotcheckedin);
                  if (isnotcheckedin == 'true') {
                  //alert(true);
                  pophtml = '<div style="padding:10px 20px;"><strong>Time Clock</strong><button type="submit" id="timeclock-endlunch" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="UpdateClockStatus(4,' + id + ',' + runsheetStartRun + ',' + currentStatus + ',' + currentStatusCount + ')" >End Lunch</button><p><a id="btn-cancel"  data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">Dismiss</a></p></div>';
                  
                  }
                  else {
                  //alert(false);
                  
                  pophtml = '<div style="padding:10px 20px;"><strong>Time Clock</strong><button type="submit" id="timeclock-endlunch" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="UpdateClockStatus(4,' + id + ',' + runsheetStartRun + ',' + currentStatus + ',' + currentStatusCount + ')" >End Lunch</button><p><a id="btn-cancel" onclick="dismissClick()" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">Dismiss</a></p></div>';
                  
                  }
                  
                  $('#timeclock' + id).html(pophtml);
                  $('#timeclock' + id).popup('open');
                  }
                  else if (result.current_status == 'LunchEnd') {
                  //alert('End Popup');
                  var pophtml = '<div style="padding:10px 20px;"><strong>Time Clock</strong><button type="submit" id="timeclock-out" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="UpdateClockStatus(2,' + id + ',' + runsheetStartRun + ',' + currentStatus + ',' + currentStatusCount + ')" >Clock Out</button><p><a id="btn-cancel"  data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">Dismiss</a></p></div>';
                  
                  $('#timeclock' + id).html(pophtml);
                  $('#timeclock' + id).popup('open');
                  
                  }
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                  
                  $.mobile.loading('hide');
                  viewRunsheet();
                  }
                  });
           }
           
           function dismissClick() {
           window.localStorage.setItem("isDismissClick", true);
           refreshRunsheet();
           }
           
           function calculateDistance(timestatus, id, userid, runsheetStartRun, currentStatus, pos1, pos2, currentStatusCount) {
           
           /*Latitude and Longitude calculation*/
           var rad = function (x) {
           return x * Math.PI / 180;
           };
           
           var getDistance = function (p1, p2) {
           var R = 6378137; // EarthÃs mean radius in meter
           var dLat = rad(p2.lat - p1.lat);
           var dLong = rad(p2.lng - p1.lng);
           var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
           Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) *
           Math.sin(dLong / 2) * Math.sin(dLong / 2);
           var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
           var d = R * c;
           return d; // returns the distance in meter
           };
           
           //alert('Position1 -lat:'+pos1.lat+' , long:'+pos1.lng);
           // alert('position2 -lat:'+pos2.lat+' , long:'+pos2.lng);
           // alert(pos2.lng);
           
           if (pos2.lat != undefined && pos2.lng != undefined) {
           var distance = getDistance(pos1, pos2);
           
           if (currentStatusCount == undefined)
           currentStatusCount = 0;
           
           //var distance = 950;
           //alert('Distance:'+distance);
           
           /*  if(distance > 1000 && currentStatus=='Out' && timestatus==1 ){
            if($("#reasonTxt").val() != undefined)
            {
            $("#reasonTxt").val("");
            }
            $('#timeclock'+id).popup('close');
            
            var pophtml='<div style="padding:10px 20px;"><div class="distance_blk"><strong>You are too far from the Shed to clock in, please enter a message as to why you are clocking in from here, it will be checked for verification purposes.</strong></div><div class="textboxArea"><textarea id="reasonTxt" cols="30" rows="10" minlength="10" ></textarea></div><div class="errorBlk">Please enter atleast 10 characters</div><div><button type="submit" class="submit-btn ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="addTimeClock('+timestatus+','+id+','+ userid+',\''+currentStatus+'\','+pos2.lat+','+pos2.lng+','+distance+','+currentStatusCount+')" >Submit Reason</button><a id="btn-cancel" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">Cancel</a></div></div>';
            
            $('#timeclock'+id).html(pophtml);
            $('#timeclock'+id).popup('open');
            
            } */
           
           if (currentStatus == 'Out' && timestatus == 1) {
           
           var displaytext = '';
           //alert(distance,currentStatusCount);
           if (distance > 1000 && currentStatusCount < 1) {
           displaytext = 'You are too far from the Shed to clock in, please enter a message as to why you are clocking in from here, it will be checked for verification purposes.';
           }
           else if (currentStatusCount >= 1) {
           displaytext = 'You have already checked in at least once today, Please provide a reason for checking in a second time.';
           }
           // var sample ='#reasonTxt'+id;
           
           // alert($('#reasonTxt'+id).val());
           
           if ($('#reasonTxt' + id).val() != undefined) {
           // alert('hai');
           $('#reasonTxt' + id).val("");
           // $('#timeclock'+id).popup('close');
           //alert('bye');
           }
           $('#timeclock' + id).popup('close');
           console.log(distance + ',' + currentStatusCount);
           // $('#timeclock'+id).html('');
           console.log('before:');
           
           setTimeout(function () {
                      var sam = 'addTimeClock(' + timestatus + ',' + id + ',' + userid + ',\'' + currentStatus + '\',' + pos2.lat + ',' + pos2.lng + ',' + distance + ',' + currentStatusCount + ')';
                      //if(pos2.lng)
                      console.log(sam);
                      var pophtml = '<div style="padding:10px 20px;"><div class="distance_blk"><strong>' + displaytext + '</strong></div><div class="textboxArea"><textarea id="reasonTxt' + id + '" cols="30" rows="10" minlength="10" class="reasonTxt" ></textarea></div><div class="errorBlk">Please enter atleast 10 characters</div><div><button type="submit" class="submit-btn ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="addTimeClock(' + timestatus + ',' + id + ',' + userid + ',\'' + currentStatus + '\',' + pos2.lat + ',' + pos2.lng + ',' + distance + ',' + currentStatusCount + ')" >Submit Reason</button><a id="btn-cancel" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">Cancel</a></div></div>';
                      $('#timeclock' + id).html(pophtml);
                      $('#timeclock' + id).popup('open');
                      }, 500);
           
           /* var pophtml='<div style="padding:10px 20px;"><div class="distance_blk"><strong>'+displaytext+'</strong></div><div class="textboxArea"><textarea id="reasonTxt'+id+'" cols="30" rows="10" minlength="10" ></textarea></div><div class="errorBlk">Please enter atleast 10 characters</div><div><button type="submit" class="submit-btn ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="addTimeClock('+timestatus+','+id+','+ userid+',\''+currentStatus+'\','+pos2.lat+','+pos2.lng+','+distance+','+currentStatusCount+')" >Submit Reason</button><a id="btn-cancel" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">Cancel</a></div></div>';*/
           
           console.log('after:');
           }
           else if (distance > 1000 && (currentStatus == 'In' || currentStatus == 'LunchEnd') && (timestatus == 2)) {
           if ($('#reasonTxt' + id).val() != undefined) {
           $('#reasonTxt' + id).val("");
           }
           $('#timeclock' + id).popup('close');
           console.log(id + '- Close4');
           var pophtml = '<div style="padding:10px 20px;"><div class="distance_blk"><strong>You are too far from the Shed to clock out, please enter a message as to why you are clocking OUT from here, it will be checked for verification purposes.</strong></div><div class="textboxArea"><textarea id="reasonTxt' + id + '" cols="30" rows="10" minlength="10" class="reasonTxt" ></textarea></div><div class="errorBlk">Please enter atleast 10 characters</div><div><button type="submit" class="submit-btn ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="addTimeClock(' + timestatus + ',' + id + ',' + userid + ',\'' + currentStatus + '\',' + pos2.lat + ',' + pos2.lng + ',' + distance + ',' + currentStatusCount + ')" >Submit Reason</button><a id="btn-cancel" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">Cancel</a></div></div>';
           
           $('#timeclock' + id).html(pophtml);
           $('#timeclock' + id).popup('open');
           
           } else {
           // alert(1);
           addTimeClock(timestatus, id, userid, currentStatus, pos2.lat, pos2.lng, distance, currentStatusCount);
           }
           
           }
           else {
           alert('Network error has occurred while fetching current location through geolocation, please check your network connection and try again!');
           }
           }
           
           
           
           function twoPointDistance(timestatus, id, userid, runsheetStartRun, currentStatus, currentStatusCount) {
           
           if (runsheetStartRun != '') {
           var pos1 = [];
           var pos2 = [];
           
           var geocoder = new google.maps.Geocoder();
           geocoder.geocode({ 'address': runsheetStartRun }, function (data, status) {
                            // alert('Google_Map_GeocoderStatus:'+status);
                            
                            if (status != google.maps.GeocoderStatus.OK) {
                            alert('Google map geocoder unable to find the address ' + runsheetStartRun);
                            }
                            
                            if (status == google.maps.GeocoderStatus.OK) {
                            var myylocation = data[0].geometry.location;
                            
                            pos1.lat = myylocation.lat();
                            pos1.lng = myylocation.lng();
                            //console.log(longitude2);
                            console.log(currentStatus + '=====>' + timestatus);
                            //alert(navigator.geolocation);
                            // navigator.geolocation.getCurrentPosition(showMap,handleError);
                            
                            //alert('hai3');
                            navigator.geolocation.getCurrentPosition(function (position) {
                                                                     var crd = position.coords;
                                                                     pos2.lat = crd.latitude;
                                                                     pos2.lng = crd.longitude;
                                                                     // alert('1');
                                                                     console.log('latitude====>' + crd.latitude);
                                                                     console.log('longitude=====>' + crd.longitude);
                                                                     // alert('hai');
                                                                     calculateDistance(timestatus, id, userid, runsheetStartRun, currentStatus, pos1, pos2, currentStatusCount);
                                                                     
                                                                     
                                                                     }, function (err) {
                                                                     // alert(err.code+','+ error.message);  // 4-
                                                                     
                                                                     //alert(2);
                                                                     $.getJSON('https://ipinfo.io/geo', function (response) {
                                                                               var loc = response.loc.split(',');
                                                                               var coords = {
                                                                               lat: loc[0], lng: loc[1]
                                                                               };
                                                                               
                                                                               calculateDistance(timestatus, id, userid, runsheetStartRun, currentStatus, pos1, coords, currentStatusCount);
                                                                               
                                                                               //alert(coords.latitude +','+ coords.longitude);
                                                                               
                                                                               }, function (err) {
                                                                               alert('Network error has occurred while fetching current location through geolocation, please check your network connection and try again!');
                                                                               });
                                                                     
                                                                     
                                                                     }, {
                                                                     enableHighAccuracy: true,
                                                                     timeout: 20000,
                                                                     maximumAge: 60000
                                                                     });
                            
                            }
                            }, function (err) {
                            alert('Network error has occurred while fetching geocoder status, please check your network connection and try again!');
                            
                            }
                            );
           }
           
           }
           
           
           function UpdateClockStatus(timestatus, id, runsheetStartRun, currentStatus, currentStatusCount) {
           // alert(timestatus);
           var userid = window.localStorage.getItem("userid");
           var username = window.localStorage.getItem("username");
           var isUpdateToDb = true;
           $('#timeclock' + id).popup('close');
           console.log(id + '- Close5');
           
           if (timestatus == 2) {
           
           $.ajax({
                  type: 'post',
                  url: remoteUrl + 'checkanypendingjob',
                  data: {
                  'userid': userid,
                  'username': username
                  },
                  cache: false,
                  timeout: 60000,
                  beforeSend: function () {
                  $.mobile.loading('show');
                  },
                  complete: function () {
                  $.mobile.loading('hide');
                  },
                  
                  success: function (result) {
                  //alert("sa1");
                  
                  result = JSON.parse(result);
                  console.log(result)
                  //no jobs-0, any pending jobs 1
                  if (result == 1) {
                  // $('#timeclock'+id).popup('close');
                  var pophtml = '<div class="popText" style="padding:20px 10px 20px 20px ;">You cannot <b>clock out</b> until all of your jobs are completed or marked as not completed for the day. Please ensure all jobs are completed (Green) or marked as not able to be completed today (Red) in your runsheet before <b>clocking out</b><p><a id="btn-cancel"  data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">OK</a></p></div>';
                  
                  $('#timeclock' + id).html(pophtml);
                  $('#timeclock' + id).popup('open');
                  //alert('selct');
                  }
                  else
                  twoPointDistance(timestatus, id, userid, runsheetStartRun, currentStatus, currentStatusCount);
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                  // alert(jqXHR+','+ textStatus+','+ errorThrown);                  //alert(errorThrown);5-
                  //alert('Network error has occurred while fetching pending jobs, please check your network connection and try again!');
                  }
                  });
           } else {
           twoPointDistance(timestatus, id, userid, runsheetStartRun, currentStatus, currentStatusCount);
           }
           window.localStorage.setItem("selectedTimeClock", timestatus);
           
           }
           
           function addTimeClock(timestatus, id, userid, status, lat, lng, distance, currentStatusCount) {
           
           var timeClockdata = [];
           var reasonTxt = '';
           $(".errorBlk").css("display", "none");
           reasonTxt = $('#reasonTxt' + id).val();
           
           if (timestatus == 1 || timestatus == 2) {
           if (distance > 1000 || currentStatusCount >= 1) {
           if (reasonTxt.length > 10 && reasonTxt != '') {
           
           $('#timeclock' + id).popup('close');
           status = (status == 'Out') ? 'In' : 'Out';
           insertTimeClock(timestatus, id, userid, status, lat, lng, reasonTxt);
           
           // alert("in:"+timestatus);
           //viewRunsheet();
           
           } else {
           $(".errorBlk").css("display", "block");
           }
           }
           else {
           // alert(status);
           $('#timeclock' + id).popup('close');
           
           if (status == 'Out')
           status = 'In';
           else if (status == 'In')
           status = 'Out';
           else if (status == 'LunchStart')
           status = 'LunchEnd';
           else if (status == 'LunchEnd')
           status = 'LunchStart';
           
           insertTimeClock(timestatus, id, userid, status, lat, lng, '');
           }
           
           } else if (timestatus == 3 || timestatus == 4) {
           //alert("in:"+timestatus);
           $('#timeclock' + id).popup('close');
           status = (status == 'LunchStart') ? 'LunchEnd' : 'LunchStart';
           insertTimeClock(timestatus, id, userid, status, lat, lng, reasonTxt);
           // viewRunsheet();
           }
           }
           
           function insertTimeClock(timestatus, id, userid, status, lat, lng, reasonTxt) {
           // alert(reasonTxt);
           $.ajax({
                  type: 'post',
                  url: remoteUrl + 'addtimeclock',
                  data: {
                  'userID': userid,
                  'timeclockEvent': status,
                  'timeclockLatitude': lat,
                  'timeclockLongitude': lng,
                  'timeclockReason': reasonTxt
                  },
                  cache: false,
                  timeout: 60000,
                  beforeSend: function () {
                  $.mobile.loading('show');
                  },
                  complete: function () {
                  $.mobile.loading('hide');
                  },
                  success: function (result) {
                  //
                  result = JSON.parse(result);
                  console.log(result)
                  if (result == 1) {
                  homepageAjax(id)
                  $('#timeclock' + id).popup('close');
                  console.log(id + '- Close6');
                  }
                  
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                  //alert(errorThrown);
                  // alert(jqXHR+','+ textStatus+','+ errorThrown);6-
                  //alert('Network error has occurred while adding timeclock status, please check your network connection and try again!');
                  }
                  });
           }
           
           
           function getPhotos() {
           // Retrieve image file location from specified source
           navigator.camera.getPicture(onPhotoURISuccess, onFail, {
                                       quality: 40,
                                       encodingType: navigator.camera.EncodingType.JPEG,
                                       targetWidth: 1800,
                                       targetHeight: 1800,
                                       correctOrientation: true,
                                       destinationType: destinationType.FILE_URI,
                                       sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                                       popoverOptions: { // iOS only
                                       x: 300,
                                       y: 300,
                                       width: 100,
                                       height: 100,
                                       arrowDir: navigator.camera.PopoverArrowDirection.ARROW_ANY
                                       }
                                       });
           }
           
           
           function onFail(message) {
           //alert('Failed because: ' + message);
           }
           
           
           
           function uploadPhoto(imageURI) {
           
           //$.mobile.loading('show');
           $.mobile.loading("show", {
                            text: "Image uploading please wait..",
                            textVisible: true,
                            theme: "b",
                            html: ""
                            });
           var options = new FileUploadOptions();
           options.fileKey = "file";
           options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
           options.mimeType = "image/jpeg";
           
           var uploadImageName = window.localStorage.getItem("uploadImageName");
           // alert(uploadImageName);
           
           if (uploadImageName == null || uploadImageName == undefined) {
           window.localStorage.setItem("uploadImageName", options.fileName);
           //alert(options.fileName);//gee
           }
           
           var sid = $('#jc-signid').val();
           var stype = $('#jc-signtype').val();
           var s_type = $('#jc-sign_type').val();
           
           
           var latitude = "";
           var longitude = "";
           
           navigator.geolocation.getCurrentPosition(function (position) {
                                                    var crd = position.coords;
                                                    var params = new Object();
                                                    params.signid = sid;
                                                    params.signtype = stype;
                                                    params.sign_type = s_type;
                                                    
                                                    params.latitude = crd.latitude;
                                                    params.longitude = crd.longitude;
                                                    params.Ordertype = gOrdertype;
                                                    
                                                    //alert(JSON.stringify(params));
                                                    
                                                    options.params = params;
                                                    options.chunkedMode = false;
                                                    var url = remoteUrl + 'jobcompleted';
                                                    
                                                    var ft = new FileTransfer();
                                                    imgURI = imageURI;
                                                    
                                                    ft.upload(imageURI, url, win, fail, options);
                                                    
                                                    
                                                    }, function (err) {
                                                    
                                                    var params = new Object();
                                                    params.signid = sid;
                                                    params.signtype = stype;
                                                    params.sign_type = s_type;
                                                    
                                                    params.latitude = "";
                                                    params.longitude = "";
                                                    params.Ordertype = gOrdertype;
                                                    
                                                    //alert(JSON.stringify(params));
                                                    
                                                    options.params = params;
                                                    options.chunkedMode = false;
                                                    var url = remoteUrl + 'jobcompleted';
                                                    
                                                    var ft = new FileTransfer();
                                                    imgURI = imageURI;
                                                    
                                                    ft.upload(imageURI, url, win, fail, options);
                                                    }, {
                                                    enableHighAccuracy: true,
                                                    timeout: 20000,
                                                    maximumAge: 60000
                                                    });
           }
           
           function win(r) {
           console.log("Code = " + r.responseCode);
           console.log("Response = " + r.response);
           console.log("Sent = " + r.bytesSent);
           var photos = JSON.parse(r.response);
           
           var largeImage = document.getElementById('smallImage');
           largeImage.style.display = 'block';
           var img = '';
           $.each(photos, function (i, item) {
                  //alert(img);//gee
                  img = img + '<span style="margin-right: 10px;" onclick="deletePopup(\'' + item + '\')"><img src="' + item + '" height=100 width=120></span>';
                  });
           
           //var img = '<span style="margin-right: 10px;"><button style="position:absolute; top:0; right:0;" onclick="deleteImage();">X</button><img src="'+imgURI+'" height=100 width=120></span>';
           $('#smallImage').html(img);
           $.mobile.loading('hide');
           
           }
           
           function fail(error) {
           alert("An error has occurred: Code = " = error.code);
           checkInterntConnection();
           }
           
           function onPhotoURISuccess(imageURI) {
           var thisResult = JSON.parse(imageURI);
           
           // convert json_metadata JSON string to JSON Object 
           var metadata = JSON.parse(thisResult.json_metadata);
           
           if (thisResult.json_metadata != "{}") {
           if (metadata.GPS != null) {
           //alert(metadata.GPS.Latitude);
           //alert(metadata.GPS.Longitude);
           }
           }
           
           uploadPhoto(thisResult.filename);
           }
           
           
           function deletePopup(image) {
           var signid = $('#jc-signid').val();
           var signtype = $('#jc-signtype').val();
           var sign_type = $('#jc-sign_type').val();
           
           var pophtml = '<form id="frm-delete"><div><lable>Are you sure to delete this image?</lablel><p><input type="hidden" name="signid" value="' + signid + '"><input type="hidden" name="sign_type" value="' + sign_type + '"><input type="hidden" name="imgname" value="' + image + '"><input type="hidden" name="signtype" value="' + signtype + '"><a id="btn-imgdelete" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="imageDelete()">Delete</a><a id="btn-cancel" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete" onclick="deletePopClose();">Cancel</a></p></div></form>';
           $('#jnc-popup').html(pophtml);
           //$('#jobcompleted-page').trigger( "pagecreate" );
           $('#jnc-popup').popup('open');
           }
           
           function imageDelete() {
           
           $.ajax({
                  type: 'post',
                  url: remoteUrl + 'imageDelete',
                  data: $('#frm-delete').serialize(),
                  cache: false,
                  timeout: 60000,
                  beforeSend: function () {
                  $.mobile.loading('show');
                  },
                  complete: function () {
                  $.mobile.loading('hide');
                  },
                  success: function (result) {
                  result = JSON.parse(result);
                  var img = '';
                  $.each(result, function (i, item) {
                         img = img + '<span style="margin-right: 10px;" onclick="deletePopup(\'' + item + '\')"><img src="' + item + '" height=100 width=120></span>';
                         });
                  
                  $('#smallImage').html(img);
                  $.mobile.loading('hide');
                  $('#jnc-popup').popup('close');
                  
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                  //alert(jqXHR+','+ textStatus+','+ errorThrown);7-
                  //alert('Network error has occurred please check your network connection and try again!');
                  }
                  });
           
           }
           
           function deletePopClose() {
           $('#jnc-popup').popup('close');
           }
           
           
           //=============================================================================================//
           
           function refreshRunsheet() {
           // alert(h1);
           var userid = window.localStorage.getItem("userid");
           var username = window.localStorage.getItem("username");
           
           
           if (userid && username) {
           $(':mobile-pagecontainer').pagecontainer('change', "#home-page");
           homepageAjax(7);
           } else {
           $(':mobile-pagecontainer').pagecontainer('change', "#login-page");
           }
           }
           
           function showChkStockReqPopup(id) {
           var userid = window.localStorage.getItem("userid");
           var username = window.localStorage.getItem("username");
           
           $.ajax({
                  type: 'post',
                  url: remoteUrl + 'checkstockrequired',
                  data: {
                  'userid': userid
                  },
                  cache: false,
                  timeout: 60000,
                  beforeSend: function () {
                  $.mobile.loading('show');
                  },
                  complete: function () {
                  $.mobile.loading('hide');
                  },
                  success: function (result) {
                  //alert(result);
                  result = JSON.parse(result);
                  
                  var pophtml = '<div style="padding:10px 50px;"><div><h1 style="padding: 10px 0px;">Stock Required</h1></div><div><p style="display: inline-block;width:100%;margin:10px;"><span style="width:90%; float:left;">New Signs to Be Installed:</span><span>' + result.newsigninstall + '</span></p></div> <div><p style="display: inline-block;width:100%;margin:10px;"><span style="width:90%; display:block; float:left;">Overlays to be Installed:</span><span>' + result.overlayartworkpendingdelivery + '</span></p></div> <div><p style="display: inline-block;width:100%;margin:10px;"><span style="width:90%; display:block; float:left;">Flag Holders Required:</span><span>' + result.flagholder + '</span></p></div> <div><p style="display: inline-block;width:100%;margin:10px;"><span style="width:90%; display:block; float:left;">Solar Lights Required:</span><span>' + result.solar + '</span></p></div> <div><p style="display: inline-block;width:100%;margin:10px;"><span style="width:90%; display:block; float:left;">Brochure Holders Required:</span><span>' + result.brochureholder + '</span></p></div> <div><p style="display: inline-block;width:100%;margin:10px;"><span style="width:90%; display:block; float:left;">Floodlights Required:</span><span>' + result.floodlight + '</span></p></div><p><a id="btn-cancel" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">Dismiss</a></p></div>';
                  
                  $('#jnc-popup' + id).html(pophtml);
                  $('#jnc-popup' + id).popup('open');
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                  // alert(jqXHR+','+ textStatus+','+ errorThrown);8-
                  //alert('Network error has occurred please check your network connection and try again!');
                  }
                  });
           }
           
           function showReallocateView(sign_type, signid) {
           var userid = window.localStorage.getItem("userid");
           var username = window.localStorage.getItem("username");
           
           $.ajax({
                  type: 'post',
                  url: remoteUrl + 'getrunuser',
                  data: {
                  'userid': userid
                  },
                  cache: false,
                  timeout: 60000,
                  beforeSend: function () {
                  $.mobile.loading('show');
                  },
                  complete: function () {
                  $.mobile.loading('hide');
                  },
                  success: function (result) {
                  result = JSON.parse(result);
                  $("#message").val("");
                  $("#installers-button span").html('Select Installer');
                  $('#installers').html('');
                  $('#installers').append($('<option>', {
                                            value: "",
                                            text: "Select Installer"
                                            }));
                  $.each(result, function (i, item) {
                         $('#installers').append($('<option>', {
                                                   value: item.userid,
                                                   text: item.username
                                                   }));
                         });
                  $('#reallocate-signid').val(signid);
                  $('#reallocate-userid').val(userid);
                  $('#reallocate-sign_type').val(sign_type);
                  $('#reallocatejob').popup('open');
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                  //  alert(jqXHR+','+ textStatus+','+ errorThrown); 9-
                  //alert('Network error has occurred please check your network connection and try again!');
                  }
                  });
           }
           
           $(document).on('click', '#btn-reallocate', function (event) {
                          event.preventDefault();
                          
                          if ($('#installers').val() == "") {
                          alert("Please select any one installer to re-allocate");
                          return;
                          }
                          
                          if ($('#frm-reallocate').validate({
                                                            rules: {
                                                            message: {
                                                            required: true
                                                            },
                                                            }
                                                            }).form()) {
                          
                          //alert(JSON.stringify({ olduserid: $('#reallocate-userid').val(), userid: $('#installers').val(), signid: $('#reallocate-signid').val(), sign_type: $('#reallocate-sign_type').val(), reason: $('#message').val() }));
                          
                          $.ajax({
                                 type: 'post',
                                 url: remoteUrl + 'changerunuser',
                                 data: {
                                 olduserid: $('#reallocate-userid').val(),
                                 userid: $('#installers').val(),
                                 signid: $('#reallocate-signid').val(),
                                 sign_type: $('#reallocate-sign_type').val(),
                                 reason: $('#message').val()
                                 },
                                 cache: false,
                                 timeout: 60000,
                                 beforeSend: function () {
                                 $.mobile.loading('show');
                                 },
                                 complete: function () {
                                 $.mobile.loading('hide');
                                 },
                                 success: function (result) {
                                 $('#reallocatejob').popup('close');
                                 
                                 result = JSON.parse(result);
                                 var signid = result.signid;
                                 var sign_type = result.sign_type;
                                 if (signid && sign_type) {
                                 jobview(sign_type, signid);
                                 } else {
                                 $(':mobile-pagecontainer').pagecontainer('change', "#home-page");
                                 homepageAjax(8);
                                 }
                                 },
                                 error: function (jqXHR, textStatus, errorThrown) {
                                 // alert(JSON.stringify(jqXHR));12-
                                 //alert('Network error has occurred please try again!');
                                 }
                                 });
                          
                          } else {
                          return false;
                          }
                          
                          });
           
           
           function onResume() {
           var userid = window.localStorage.getItem("userid");
           var username = window.localStorage.getItem("username");
           
           
           if (userid && username) {
           $(':mobile-pagecontainer').pagecontainer('change', "#home-page");
           homepageAjax(9);
           } else {
           $(':mobile-pagecontainer').pagecontainer('change', "#login-page");
           }
           }
           
           var refreshPrevPage = false;
           function LoadSettingsPage() {
           var mapselected = window.localStorage.getItem("mapselected");
           if (mapselected == "waze") {
           $("#radio-choice-waze").prop('checked', true);
           }
           else if (mapselected == "google") {
           $("#radio-choice-google").prop('checked', true);
           }
           else if (mapselected == "apple") {
           $("#radio-choice-apple").prop('checked', true);
           }
           else {
           $("#radio-choice-tomtom").prop('checked', true);
           }
           $(':mobile-pagecontainer').pagecontainer('change', "#settings-page");
           $(':mobile-pagecontainer').pagecontainer({
                                                    change: function (e, u) {
                                                    if (u.toPage[0].id == "settings-page") {
                                                    refreshPrevPage = true;
                                                    //if ((item.agentid != 0 || item.agentid2 != 0) && item.WarnJobIsNext == 1 && showAlert)
                                                    //confirmWarningForNextJob(sign_type, signid, item.orderid);
                                                    }
                                                    else {
                                                    if (refreshPrevPage) {
                                                    if (u.toPage[0].id == "home-page" || u.toPage[0].id == "runsheet-page") {
                                                    homepageAjax(2);
                                                    }
                                                    else if (u.toPage[0].id == "jobview-page") {
                                                    getJobdetails(last_sign_type, last_signid);
                                                    }
                                                    refreshPrevPage = false;
                                                    }
                                                    }
                                                    }
                                                    });
           }
           
           
           function onSuccessURL() {
           
           }
           
           function onFailureURL() {
           
           }
           
           
           
           function GetRunsheetLatestVersion() {
           
           $('#jnc-popup1').popup('close');
           
           //  var ref = window.open('https://digitalcentral.com.au/app?ver', '_blank', 'location=yes');
           OpenUrlExt.open('https://digitalcentral.com.au/app?version=CurrentVersionNumberFromApp', onSuccessURL, onFailureURL);
           
           }
           
           function GetLatestVersion() {
           
           $('#jnc-popup1').popup('close');
           
           //  var ref = window.open('https://digitalcentral.com.au/app?ver', '_blank', 'location=yes');
           //var ref = window.open('https://digitalcentral.com.au/app?version=CurrentVersionNumberFromApp', '_system');
           OpenUrlExt.open('https://digitalcentral.com.au/app?version=CurrentVersionNumberFromApp', onSuccessURL, onFailureURL);
           }
           
           function mapselectionchange() {
           window.localStorage.setItem("mapselected", $("input[name='radio-choice-w-6']:checked").val());
           }
           
           function logout() {
           var userid = window.localStorage.getItem("userid");
           
           
           $.ajax({
                  type: 'post',
                  url: remoteUrl + 'logout',
                  data: {
                  'userid': userid
                  },
                  cache: false,
                  beforeSend: function () {
                  $.mobile.loading('show');
                  },
                  complete: function () {
                  $.mobile.loading('hide');
                  },
                  success: function (result) {
                  window.localStorage.clear();
                  $('#uname').val('');
                  $('#pwd').val('');
                  $('#version').html(version);
                  $('.username').html('Logged in as: ' + userid);
                  $(':mobile-pagecontainer').pagecontainer('change', "#login-page");
                  $.toast().reset('all');
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                  // alert(jqXHR+','+ textStatus+','+ errorThrown);10-
                  //alert('Network error has occurred please check your network connection and try again!');
                  }
                  });
           
           
           $.ajax({
                  type: 'GET',
                  url: chatUrl + 'logout',
                  data: "",
                  headers: {
                  "Content-Type": "application/x-www-form-urlencoded",
                  "X-Auth-Token": window.localStorage.getItem("chat_authToken"),
                  "X-User-Id": window.localStorage.getItem("chat_userId")
                  },
                  
                  beforeSend: function () {
                  //$.mobile.loading('show');
                  },
                  complete: function () {
                  //$.mobile.loading('hide');
                  },
                  success: function (result) {
                  console.log("Logout >>>> " + JSON.stringify(result));
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                  }
                  });
           
           }
           
           
           //==============================================================================================//
           
           //============================== Geolocation ====================================//
           var utimer = 20000;
           var lastlat = 0,
           lastlong = 0;
           var lastLocationChangedTime = Date.now();
           var lastUpdatedTime = new Date();
           var watchID1 = null;
           
           function onLocationSuccess(position) {
           //alert('success');
           var lat = position.coords.latitude;
           var lon = position.coords.longitude;
           var userid = window.localStorage.getItem("userid");
           //alert(utimer);
           if (userid && utimer > 50000) {
           
           if (lastlat != lat || lastlong != lon) {
           lastLocationChangedTime = new Date();
           }
           
           lastlat = lat;
           lastlong = lon;
           //console.log(lastlat);
           //alert(lastLocationChangedTime);
           if (((new Date() - lastLocationChangedTime) / 60000) < 30) { // checked last location changed before than 60 secs then don't update
           //alert(lastUpdatedTime);
           if (((new Date() - lastUpdatedTime) / 1000) >= 29) {
           //console.log("updateUserLocation");
           //alert("update location");
           $.ajax({
                  type: 'post',
                  url: remoteUrl + 'updateUserLocation',
                  data: {
                  'lat': lat,
                  'lon': lon,
                  'userid': userid
                  },
                  cache: false,
                  beforeSend: function () {
                  //$.mobile.loading('show');
                  },
                  complete: function () {
                  //$.mobile.loading('hide');
                  },
                  success: function (result) {
                  lastUpdatedTime = new Date();
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                  //alert('Network error has occurred please check your network connection and try again!');
                  }
                  });
           }
           }
           }
           
           }
           
           // onError Callback receives a PositionError object
           //
           function onLocationError(error) {
           console.log('code: ' + error.code + '\n' + 'message: ' + error.message + '\n');
           // alert(error.code);
           
           //if (error.code != 2)
           //    alert(error.message);
           }
           
           
           
           
           function updateLocation() {
           
           var userid = window.localStorage.getItem("userid");
           
           
           if ((new Date()).getHours() >= 6 && (new Date()).getHours() < 18 && userid != null) // checked hour between 6 AM to 6 PM to gather GPS info
           {
           //    alert(userid);           //console.log("location");
           var watchID = navigator.geolocation.getCurrentPosition(onLocationSuccess, onLocationError, { timeout: 10000, enableHighAccuracy: true });
           if (watchID1 == null) {
           updateLocation1();
           }
           } else {
           if (watchID1 != null) {
           navigator.geolocation.clearWatch(watchID1);
           watchID = undefined;
           }
           }
           }
           
           function updateLocation1() {
           
           watchID1 = navigator.geolocation.watchPosition(onLocationSuccess, onLocationError, { timeout: 10000, enableHighAccuracy: true });
           }
           
           var updLocation = setInterval(function () {
                                         updateLocation();
                                         utimer = utimer + 30000
                                         }, 30000);
           
           var updLocation1 = setInterval(function () {
                                          updateLocation1();
                                          clearTimeout(updLocation1);
                                          }, 1000);
           
           function showReallocatedJob(id) {
           var userid = window.localStorage.getItem("userid");
           var username = window.localStorage.getItem("username");
           // alert('reallocat');
           $.ajax({
                  type: 'post',
                  url: remoteUrl + 'getreason',
                  data: {
                  'userid': userid
                  },
                  cache: false,
                  timeout: 60000,
                  beforeSend: function () {
                  $.mobile.loading('show');
                  },
                  complete: function () {
                  $.mobile.loading('hide');
                  },
                  success: function (result) {
                  //alert(result);
                  result = JSON.parse(result);
                  if (result.length > 0) {
                  var pophtml = '<div style="padding:10px 50px;"><div><h1 style="padding: 10px 0px;">Job Re-allocate</h1></div> <div><p style="display: inline-block;width:100%;margin:10px;"><span style="float:left;">' + result[0].username + ' that re_allocated the job</span> <br /> <span>Message: ' + result[0].reallocatejob_reason + '</span> </p> </div> <p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="getJobdetails(' + sign_type + ',' + signid + ')">View Job</a><a id="btn-cancel" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-delete">Dismiss</a></p> </div>';
                  
                  $('#jnc-popup' + id).html(pophtml);
                  $('#jnc-popup' + id).popup('open');
                  }
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                  //alert('Network error has occurred please check your network connection and try again!');
                  }
                  });
           }
           
           
           function checkInterntConnection() {
           
           try {
           
           var networkState = navigator.connection.type;
           
           if (networkState === Connection.UNKNOWN || networkState === Connection.NONE || networkState === undefined) {
           
           var pophtml = '';
           
           pophtml = '<div style="padding:20px 10px 20px 20px ;"> <div style="padding:20px 0px 20px 0px ;"> <strong> Network Error </strong> </div> <lable>No internet connection can be made, please check your internet connection and try again. </lablel><p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="closeNetworkStatusPopup()">Ok</a></p></div>';
           
           loadPopupBasedOnPage(pophtml);
           
           } else {
           
           checkConnectionCalls = checkConnectionCalls + 1;
           
           if (checkConnectionCalls > 9) {
           
           $.ajax({
                  type: 'GET',
                  url: remoteUrl + 'checkConnection',
                  beforeSend: function () {
                  $.mobile.loading('show');
                  },
                  complete: function () {
                  $.mobile.loading('hide');
                  },
                  success: function (result) {
                  
                  checkConnectionCalls = 0;
                  
                  console.log(result);
                  
                  var pophtml = '';
                  if (result == 0 || result == "0") {
                  pophtml = '<div style="padding:20px 10px 20px 20px ;"> <div style="padding:20px 0px 20px 0px ;"> <strong>DC Server OK </strong> </div> <lable>Digital Central Installer APP server functioning normally, there may be an issue with this job, please try again. </lablel><p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="closeNetworkStatusPopup()">Ok</a></p></div>';
                  } else if (result == 1 || result == "1") {
                  pophtml = '<div style="padding:20px 10px 20px 20px ;"> <div style="padding:20px 0px 20px 0px ;"> <strong>Database Error</strong> </div> <lable>The Installer APP database is not working properly, please try again later.</lablel><p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="closeNetworkStatusPopup()">Ok</a></p></div>';
                  }
                  
                  loadPopupBasedOnPage(pophtml);
                  
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                  
                  checkConnectionCalls = 0;
                  
                  var pophtml = '<div style="padding:20px 10px 20px 20px ;"> <div style="padding:20px 0px 20px 0px ;"> <strong>Server Down</strong> </div> <lable>The Installer APP server is currently offline, please try again later.  </lablel><p><a class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check" onclick="closeNetworkStatusPopup()">Ok</a></p></div>';
                  
                  loadPopupBasedOnPage(pophtml);
                  }
                  });
           }
           }
           } catch (e) {
           
           }
           }
           
           function closeNetworkStatusPopup() {
           
           var currentPageId = $(':mobile-pagecontainer').pagecontainer('getActivePage').attr('id');
           if (currentPageId == "home-page") {
           $('#jnc-popup1').popup('close');
           } else if (currentPageId == "runsheet-page") {
           $('#jnc-popup2').popup('close');
           } else if (currentPageId == "jobview-page") {
           $('#jnc-popup3').popup('close');
           } else if (currentPageId == "jobcompleted-page") {
           $('#jnc-popup5').popup('close');
           } else if (currentPageId == "signlookup-page") {
           $('#jnc-popup6').popup('close');
           } else if (currentPageId == "signdetails-page") {
           $('#jnc-popup7').popup('close');
           } else if (currentPageId == "settings-page") {
           $('#jnc-popup8').popup('close');
           } else if (currentPageId == "login-page") {
           $('#jnc-popup9').popup('close');
           }
           }
           
           function loadPopupBasedOnPage(pophtml) {
           var currentPageId = $(':mobile-pagecontainer').pagecontainer('getActivePage').attr('id');
           if (currentPageId == "home-page") {
           $('#jnc-popup1').html(pophtml);
           $('#jnc-popup1').popup('open');
           } else if (currentPageId == "runsheet-page") {
           $('#jnc-popup2').html(pophtml);
           $('#jnc-popup2').popup('open');
           } else if (currentPageId == "jobview-page") {
           $('#jnc-popup3').html(pophtml);
           $('#jnc-popup3').popup('open');
           } else if (currentPageId == "jobcompleted-page") {
           $('#jnc-popup5').html(pophtml);
           $('#jnc-popup5').popup('open');
           } else if (currentPageId == "signlookup-page") {
           $('#jnc-popup6').html(pophtml);
           $('#jnc-popup6').popup('open');
           } else if (currentPageId == "signdetails-page") {
           $('#jnc-popup7').html(pophtml);
           $('#jnc-popup7').popup('open');
           } else if (currentPageId == "settings-page") {
           $('#jnc-popup8').html(pophtml);
           $('#jnc-popup8').popup('open');
           } else if (currentPageId == "login-page") {
           $('#jnc-popup9').html(pophtml);
           $('#jnc-popup9').popup('open');
           }
           }
           
           //setInterval(function () {
           //var currentPageId = $(':mobile-pagecontainer').pagecontainer('getActivePage').attr('id');
           //if (currentPageId == "home-page") {
           //showReallocatedJob(1);
           //}
           //else if (currentPageId == "runsheet-page") {
           //showReallocatedJob(2);
           //}
           //}, 10000);
           
